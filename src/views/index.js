import React, { Suspense } from 'react';
import { Route, withRouter, Switch, Redirect } from 'react-router-dom';
import AppLayout from 'layout/AppLayout';
import { getGroupState } from 'redux/group/selectors';
import { useSelector } from 'react-redux';

const Dashboards = React.lazy(() => import('./dashboards'));
const Feeds = React.lazy(() => import('./feeds'));
const Taxonomies = React.lazy(() => import('./taxonomies'));
const Newsletter = React.lazy(() => import('./newsletter'));
const Subscribers = React.lazy(() => import('./subscribers'));
const Designer = React.lazy(() => import('./designer'));
const Widget = React.lazy(() => import('./widget'));
const ViewError = React.lazy(() => import('./error'));

const Views = () => {
  const { loading, groups, isAdmin } = useSelector(getGroupState);
  const hasGroups = groups && groups.length > 0;
  const notAccess = !isAdmin && !hasGroups;

  return (
    <AppLayout showSidebar={!notAccess}>
      <div className="dashboard-wrapper">
        {loading ? (
          <div className="loading" />
        ) : notAccess ? (
          <ViewError title="No Access." showBackHome />
        ) : (
          <Suspense fallback={<div className="loading" />}>
            <Switch>
              <Redirect exact from="/" to="/dashboards" />
              <Route path="/dashboards" render={(props) => <Dashboards {...props} />} />
              <Route path="/feeds" render={(props) => <Feeds {...props} />} />
              <Route path="/taxonomies" render={(props) => <Taxonomies {...props} />} />
              <Route path="/newsletter" render={(props) => <Newsletter {...props} />} />
              <Route path="/subscribers" render={(props) => <Subscribers {...props} />} />
              <Route path="/designer" render={(props) => <Designer {...props} />} />
              <Route path="/widget" render={(props) => <Widget {...props} />} />
              <Route path="/error" exact render={(props) => <ViewError {...props} title="Sorry, something went wrong." />} />
              <Redirect to="/error" />
            </Switch>
          </Suspense>
        )}
      </div>
    </AppLayout>
  );
};

export default withRouter(Views);
