import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import ListPageHeading from 'containers/pages/ListPageHeading';
import useMousetrap from 'hooks/use-mousetrap';
import AddNewSubscriberModal from 'containers/pages/AddNewSubscriberModal';
import { Row } from 'reactstrap';
import SubscribersView from 'containers/pages/SubscribersView';
import Pagination from 'containers/pages/Pagination';
import { search_match, compare } from 'helpers/Utils';
import useNewsletters from 'hooks/use-newsletters';
import useSubscribers from 'hooks/use-subscribers';
import { useDispatch } from 'react-redux';
import { deleteSubscriber } from 'redux/actions';

const orderOptions = [
  { column: 'date', label: 'Date' },
  { column: 'title', label: 'Title' },
  { column: 'email', label: 'Email' }
];
const pageSizes = [4, 8, 12, 20];

const SubscribersList = ({ match }) => {
  const dispatch = useDispatch();
  const { subscribers } = useSubscribers();
  const { newsletters } = useNewsletters();
  const [currentPage, setCurrentPage] = useState(1);
  const [selectedPageSize, setSelectedPageSize] = useState(8);
  const [selectedOrderOption, setSelectedOrderOption] = useState({ column: 'date', label: 'Date' });

  const [modalOpen, setModalOpen] = useState(false);
  const [totalItemCount, setTotalItemCount] = useState(0);
  const [totalPage, setTotalPage] = useState(1);
  const [search, setSearch] = useState('');
  const [selectedItems, setSelectedItems] = useState([]);
  const [editedItem, setEditedItem] = useState(null);
  const [items, setItems] = useState([]);

  useEffect(() => {
    setCurrentPage(1);
  }, [selectedPageSize, selectedOrderOption]);

  useEffect(() => {
    if (subscribers) {
      setTotalPage(Math.ceil(subscribers.length / selectedPageSize));
      setItems(subscribers);
      setSelectedItems([]);
      setTotalItemCount(subscribers.length);
    }
  }, [subscribers, selectedPageSize, currentPage, selectedOrderOption, search]);

  const handleChangeSelectAll = (isToggle) => {
    if (selectedItems.length >= items.length) {
      if (isToggle) {
        setSelectedItems([]);
      }
    } else {
      setSelectedItems(items.map((x) => x.id));
    }
    document.activeElement.blur();
    return false;
  };

  useMousetrap(['ctrl+a', 'command+a'], () => {
    handleChangeSelectAll(false);
  });

  useMousetrap(['ctrl+d', 'command+d'], () => {
    setSelectedItems([]);
    return false;
  });

  const onEdit = (item) => {
    setEditedItem(item);
    setModalOpen(true);
  };
  const onDelete = (id) => dispatch(deleteSubscriber(id));

  const startIndex = (currentPage - 1) * selectedPageSize;
  const endIndex = currentPage * selectedPageSize;

  const filteredItems = items.slice(startIndex, endIndex).sort((a, b) => {
    const { column } = selectedOrderOption;
    let key = 'title';
    let isAsc = false;
    if (column === 'date') key = 'created';
    if (column === 'email') {
      key = 'field_email';
      isAsc = true;
    }
    return compare(a.attributes[key], b.attributes[key], isAsc);
  });

  const finalItems = search_match(search, filteredItems, ['attributes', 'title'], true);

  return (
    <div className="disable-text-selection">
      <ListPageHeading
        heading="menu.subscribers-list"
        changeOrderBy={(column) => {
          setSelectedOrderOption(orderOptions.find((x) => x.column === column));
        }}
        changePageSize={setSelectedPageSize}
        selectedPageSize={selectedPageSize}
        totalItemCount={totalItemCount}
        selectedOrderOption={selectedOrderOption}
        match={match}
        startIndex={startIndex}
        endIndex={endIndex}
        onSearchKey={(e) => setSearch(e.target.value.toLowerCase())}
        orderOptions={orderOptions}
        pageSizes={pageSizes}
        toggleModal={() => setModalOpen(!modalOpen)}
      />
      <AddNewSubscriberModal
        newsletters={newsletters}
        modalOpen={modalOpen}
        toggleModal={() => {
          setEditedItem(null);
          setModalOpen(!modalOpen);
        }}
        edited={editedItem}
      />
      <Row>
        {finalItems.map((item) => (
          <SubscribersView key={item.id} item={item} newsletters={newsletters} onEdit={onEdit} onDelete={onDelete} />
        ))}
        <Pagination currentPage={currentPage} totalPage={totalPage} onChangePage={setCurrentPage} />
      </Row>
    </div>
  );
};
SubscribersList.propTypes = { match: PropTypes.object };
export default SubscribersList;
