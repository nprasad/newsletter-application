import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route, Switch } from 'react-router-dom';

const FeedList = React.lazy(() => import('./feed-list'));

const PagesProduct = ({ match }) => (
  <Suspense fallback={<div className="loading" />}>
    <Switch>
      <Redirect exact from={`${match.url}/`} to={`${match.url}/list`} />
      <Route path={`${match.url}/list`} render={(props) => <FeedList {...props} />} />
      <Redirect to="/error" />
    </Switch>
  </Suspense>
);

PagesProduct.propTypes = {
  match: PropTypes.object
};
export default PagesProduct;
