import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import ListPageHeading from 'containers/pages/ListPageHeading';
import AddNewFeed from 'containers/pages/AddNewFeed';
import ListPageListing from 'containers/pages/ListPageListing';
import useMousetrap from 'hooks/use-mousetrap';
import { compare, search_match } from 'helpers/Utils';
import useFeeds from 'hooks/use-feeds';
import ConfirmationModal from 'components/common/ConfirmationModal';

const getIndex = (value, arr, prop) => {
  for (let i = 0; i < arr.length; i += 1) {
    if (arr[i][prop] === value) {
      return i;
    }
  }
  return -1;
};

const orderOptions = [
  { column: 'date', label: 'Date' },
  { column: 'category', label: 'Category' },
  { column: 'type', label: 'type (xml, json, rss)' }
];
const pageSizes = [4, 8, 12, 20];

const FeedList = ({ match }) => {
  const { feeds, deleteFeed } = useFeeds();
  const [currentPage, setCurrentPage] = useState(1);
  const [selectedPageSize, setSelectedPageSize] = useState(8);
  const [selectedOrderOption, setSelectedOrderOption] = useState({
    column: 'date',
    label: 'Date'
  });
  const [modalOpen, setModalOpen] = useState(false);
  const [totalItemCount, setTotalItemCount] = useState(0);
  const [totalPage, setTotalPage] = useState(1);
  const [search, setSearch] = useState('');
  const [SearchAdminKey, setSearchAdminKey] = useState('');
  const [selectedItems, setSelectedItems] = useState([]);
  const [items, setItems] = useState([]);
  const [editedItem, setEditedItem] = useState(null);
  const [lastChecked, setLastChecked] = useState(null);
  const [showConfirmation, setShowConfirmation] = useState(false);

  useEffect(() => {
    if (feeds) {
      setTotalPage(Math.ceil(feeds.length / selectedPageSize));
      setItems(feeds);
      setSelectedItems([]);
      setTotalItemCount(feeds.length);
    }
  }, [feeds, selectedPageSize, currentPage, selectedOrderOption, search]);

  const onCheckItem = (event, id) => {
    if (event.target.tagName === 'A' || (event.target.parentElement && event.target.parentElement.tagName === 'A')) {
      return true;
    }

    setLastChecked(id);

    let selectedList = [...selectedItems];
    if (selectedList.includes(id)) {
      selectedList = selectedList.filter((x) => x !== id);
    } else {
      selectedList.push(id);
    }
    setSelectedItems(selectedList);

    if (event.shiftKey) {
      let newItems = [...items];
      const start = getIndex(id, newItems, 'id');
      const end = getIndex(lastChecked, newItems, 'id');
      newItems = newItems.slice(Math.min(start, end), Math.max(start, end) + 1);
      selectedItems.push(
        ...newItems.map((item) => {
          return item.id;
        })
      );
      selectedList = Array.from(new Set(selectedItems));
      setSelectedItems(selectedList);
    }
    document.activeElement.blur();
    return false;
  };

  const handleChangeSelectAll = (isToggle) => {
    if (selectedItems.length >= items.length) {
      if (isToggle) {
        setSelectedItems([]);
      }
    } else {
      setSelectedItems(items.map((x) => x.id));
    }
    document.activeElement.blur();
    return false;
  };

  const handleDeleteFeeds = () => {
    selectedItems.forEach((item) => deleteFeed(item));
    setShowConfirmation(false);
  };

  const onContextMenuClick = (e, data) => {
    // params : (e,data,target)
    if (data.action === 'delete') {
      setShowConfirmation(true);
    }
    if (data.action === 'edit') {
      const editedItem = items.find((item) => item.id === lastChecked);
      editedItem && setEditedItem(editedItem);
      setModalOpen(true);
    }
  };

  const onContextMenu = (e, data) => {
    const clickedProductId = data.data;
    if (!selectedItems.includes(clickedProductId)) {
      setSelectedItems([clickedProductId]);
    }
    setLastChecked(clickedProductId);
    return true;
  };

  useMousetrap(['ctrl+a', 'command+a'], () => {
    handleChangeSelectAll(false);
  });

  useMousetrap(['ctrl+d', 'command+d'], () => {
    setSelectedItems([]);
    return false;
  });

  const startIndex = (currentPage - 1) * selectedPageSize;
  const endIndex = currentPage * selectedPageSize;

  const withFilter = items.slice(startIndex, endIndex).sort((a, b) => {
    const { column } = selectedOrderOption;
    let key = 'title';
    if (column === 'date') key = 'created';
    if (column === 'category') key = 'category';
    if (column === 'type') key = 'type';
    return compare(a.attributes[key], b.attributes[key]);
  });

  const withSearch = search_match(search, withFilter, ['attributes', 'title'], true);
  const result = search_match(SearchAdminKey, withSearch, ['attributes', 'field_admin_group'], true);

  return (
    <>
      <div className="disable-text-selection">
        <ListPageHeading
          heading="menu.feed-list"
          changeOrderBy={(column) => {
            setSelectedOrderOption(orderOptions.find((x) => x.column === column));
          }}
          changePageSize={setSelectedPageSize}
          selectedPageSize={selectedPageSize}
          totalItemCount={totalItemCount}
          selectedOrderOption={selectedOrderOption}
          match={match}
          startIndex={startIndex}
          endIndex={endIndex}
          onSearchKey={(e) => setSearch(e.target.value.toLowerCase())}
          onSearchAdminKey={(e) => setSearchAdminKey(e.target.value.toLowerCase())}
          orderOptions={orderOptions}
          pageSizes={pageSizes}
          toggleModal={() => {
            setLastChecked(null);
            setModalOpen(!modalOpen);
          }}
        />
        {modalOpen && (
          <AddNewFeed
            modalOpen={modalOpen}
            toggleModal={() => {
              setModalOpen(!modalOpen);
              setEditedItem(null);
            }}
            edited={editedItem}
          />
        )}
        <ListPageListing
          items={result}
          selectedItems={selectedItems}
          onCheckItem={onCheckItem}
          currentPage={currentPage}
          totalPage={totalPage}
          onContextMenuClick={onContextMenuClick}
          onContextMenu={onContextMenu}
          onChangePage={setCurrentPage}
        />
      </div>
      <ConfirmationModal
        isOpen={showConfirmation}
        text="Are you sure that you want to delete this content?"
        onCancel={() => setShowConfirmation(false)}
        onConfirm={handleDeleteFeeds}
      />
    </>
  );
};

FeedList.propTypes = { match: PropTypes.object };

export default FeedList;
