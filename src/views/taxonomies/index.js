import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route, Switch } from 'react-router-dom';

const Categories = React.lazy(() => import('./categories'));
const Topics = React.lazy(() => import('./topics'));

const PagesProduct = ({ match }) => (
  <Suspense fallback={<div className="loading" />}>
    <Switch>
      <Redirect exact from={`${match.url}/`} to={`${match.url}/categories`} />
      <Route path={`${match.url}/categories`} render={(props) => <Categories {...props} />} />
      <Route path={`${match.url}/topics`} render={(props) => <Topics {...props} />} />
      <Redirect to="/error" />
    </Switch>
  </Suspense>
);

PagesProduct.propTypes = {
  match: PropTypes.object
};
export default PagesProduct;
