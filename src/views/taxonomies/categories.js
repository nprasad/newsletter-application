import React, { useState, useEffect } from 'react';
import ListPageHeading from 'containers/pages/ListPageHeading';
import useMousetrap from 'hooks/use-mousetrap';
import { compare, search_match } from 'helpers/Utils';
import AddNewCategory from 'containers/pages/AddNewCategory';
import ContextMenuContainer from 'containers/pages/ContextMenuContainer';
import { Row } from 'reactstrap';
import CategoryListView from 'containers/pages/CategoryListView';
import useTaxonomies from 'hooks/use-taxonomies';
import Pagination from 'containers/pages/Pagination';
import ConfirmationModal from 'components/common/ConfirmationModal';

const getIndex = (value, arr, prop) => {
  for (let i = 0; i < arr.length; i += 1) {
    if (arr[i][prop] === value) {
      return i;
    }
  }
  return -1;
};

const orderOptions = [
  { column: 'date', label: 'Date' },
  { column: 'name', label: 'Name' }
];
const pageSizes = [4, 8, 12, 20];

const Categories = ({ match }) => {
  const { categories, deleteTaxonomy } = useTaxonomies();
  const [currentPage, setCurrentPage] = useState(1);
  const [selectedPageSize, setSelectedPageSize] = useState(8);
  const [selectedOrderOption, setSelectedOrderOption] = useState({
    column: 'date',
    label: 'Date'
  });

  const [modalOpen, setModalOpen] = useState(false);
  const [totalItemCount, setTotalItemCount] = useState(0);
  const [totalPage, setTotalPage] = useState(1);
  const [search, setSearch] = useState('');
  const [selectedItems, setSelectedItems] = useState([]);
  const [items, setItems] = useState([]);
  const [editedItem, setEditedItems] = useState(null);
  const [lastChecked, setLastChecked] = useState(null);
  const [showConfirmation, setShowConfirmation] = useState(false);

  useEffect(() => {
    setCurrentPage(1);
  }, [selectedPageSize, selectedOrderOption]);

  useEffect(() => {
    if (categories) {
      setTotalPage(Math.ceil(categories.length / selectedPageSize));
      setItems(categories);
      setSelectedItems([]);
      setTotalItemCount(categories.length);
    }
  }, [categories, selectedPageSize, currentPage, selectedOrderOption, search]);

  const onCheckItem = (event, id) => {
    if (event.target.tagName === 'A' || (event.target.parentElement && event.target.parentElement.tagName === 'A')) {
      return true;
    }

    setLastChecked(id);

    let selectedList = [...selectedItems];
    if (selectedList.includes(id)) {
      selectedList = selectedList.filter((x) => x !== id);
    } else {
      selectedList.push(id);
    }
    setSelectedItems(selectedList);

    if (event.shiftKey) {
      let newItems = [...items];
      const start = getIndex(id, newItems, 'id');
      const end = getIndex(lastChecked, newItems, 'id');
      newItems = newItems.slice(Math.min(start, end), Math.max(start, end) + 1);
      selectedItems.push(
        ...newItems.map((item) => {
          return item.id;
        })
      );
      selectedList = Array.from(new Set(selectedItems));
      setSelectedItems(selectedList);
    }
    document.activeElement.blur();
    return false;
  };

  const handleChangeSelectAll = (isToggle) => {
    if (selectedItems.length >= items.length) {
      if (isToggle) {
        setSelectedItems([]);
      }
    } else {
      setSelectedItems(items.map((x) => x.id));
    }
    document.activeElement.blur();
    return false;
  };

  const handleDeleteCategories = () => {
    selectedItems.forEach((item) => deleteTaxonomy({ taxonomy: 'categories', id: item }));
    setShowConfirmation(false);
  };

  const onContextMenuClick = (e, data) => {
    // params : (e,data,target)
    console.log('onContextMenuClick - selected items', selectedItems);
    console.log('onContextMenuClick - action : ', data.action);

    if (data.action === 'delete') {
      setShowConfirmation(true);
    }
    if (data.action === 'edit') {
      const editedItem = items.find((item) => item.id === lastChecked);
      editedItem && setEditedItems(editedItem);
      setModalOpen(true);
    }
  };

  const onContextMenu = (e, data) => {
    const clickedProductId = data.data;
    if (!selectedItems.includes(clickedProductId)) {
      setSelectedItems([clickedProductId]);
    }
    setLastChecked(clickedProductId);
    return true;
  };

  useMousetrap(['ctrl+a', 'command+a'], () => {
    handleChangeSelectAll(false);
  });

  useMousetrap(['ctrl+d', 'command+d'], () => {
    setSelectedItems([]);
    return false;
  });

  const startIndex = (currentPage - 1) * selectedPageSize;
  const endIndex = currentPage * selectedPageSize;

  const filteredItems = items.slice(startIndex, endIndex).sort((a, b) => {
    const { column } = selectedOrderOption;
    let key = 'date';
    let isAsc = true;
    if (column === 'date') {
      key = 'revision_created';
      isAsc = false;
    }
    if (column === 'name') key = 'name';
    return compare(a.attributes[key], b.attributes[key], isAsc);
  });

  const finalItems = search_match(search, filteredItems, ['attributes', 'name'], true);

  return (
    <>
      <div className="disable-text-selection">
        <ListPageHeading
          heading="menu.categories"
          changeOrderBy={(column) => {
            setSelectedOrderOption(orderOptions.find((x) => x.column === column));
          }}
          changePageSize={setSelectedPageSize}
          selectedPageSize={selectedPageSize}
          totalItemCount={totalItemCount}
          selectedOrderOption={selectedOrderOption}
          match={match}
          startIndex={startIndex}
          endIndex={endIndex}
          onSearchKey={(e) => setSearch(e.target.value.toLowerCase())}
          orderOptions={orderOptions}
          pageSizes={pageSizes}
          toggleModal={() => {
            setLastChecked(null);
            setModalOpen(!modalOpen);
          }}
        />
        <AddNewCategory
          modalOpen={modalOpen}
          toggleModal={() => {
            setModalOpen(!modalOpen);
            setEditedItems(null);
          }}
          edited={editedItem}
        />
        <Row>
          {finalItems.map((item) => (
            <CategoryListView
              key={item.id}
              item={item}
              isSelect={selectedItems.includes(item.id)}
              onCheckItem={onCheckItem}
              collect={(props) => ({ data: props.data })}
            />
          ))}
          <Pagination currentPage={currentPage} totalPage={totalPage} onChangePage={(i) => setCurrentPage(i)} />
          <ContextMenuContainer onContextMenuClick={onContextMenuClick} onContextMenu={onContextMenu} />
        </Row>
      </div>
      <ConfirmationModal
        isOpen={showConfirmation}
        text="Are you sure that you want to delete this content?"
        onCancel={() => setShowConfirmation(false)}
        onConfirm={handleDeleteCategories}
      />
    </>
  );
};

export default Categories;
