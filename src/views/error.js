import React from 'react';
const Error = ({ title, showBackHome }) => {
  return (
    <>
      <h2>{title}</h2>
      {showBackHome && <a href="http://home.cern/">Back to home.cern </a>}
    </>
  );
};

export default Error;
