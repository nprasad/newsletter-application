/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import Designer from 'components/designer/Designer';
import { useDispatch, useSelector } from 'react-redux';
import { Card, CardBody, CardTitle, Label } from 'reactstrap';
import {
  resetNewsletter,
  selectInstance,
  onSetDesignerState,
  selectNewsletter,
  fetchInstances,
  fetchInstance,
  fetchNewsletter,
  resetDesigner,
  resetArticles
} from 'redux/actions';
import Select from 'react-select';
import CustomSelectInput from 'components/common/CustomSelectInput';
import useNewsletters from 'hooks/use-newsletters';

const EmailDesigner = ({ match, history }) => {
  const dispatch = useDispatch();
  const { newsletter, newsletters } = useNewsletters();
  const { instance, instances } = useSelector((state) => state.newsletter);
  const { issueId } = match.params;

  const onSelectNewsletter = ({ value }) => dispatch(selectNewsletter(value));
  const onSelectInstance = ({ value }) => {
    if (value) {
      dispatch(selectInstance(value));
      history.replace(`/designer/${value.id}`);
    }
  };

  useEffect(() => {
    if (issueId && !instance) dispatch(fetchInstance(issueId));
    if (newsletter && !issueId) {
      const query = `?filter[field_newsletter.id]=${newsletter.id}`;
      dispatch(fetchInstances(query));
    }
    if (instance && !newsletter) {
      const newsletterId = instance.relationships.field_newsletter.data.id || undefined;
      if (newsletterId) dispatch(fetchNewsletter(newsletterId));
    }
  }, [newsletter, issueId, instance]);

  useEffect(() => {
    const designerState = instance && instance.attributes.field_designer_object;
    if (designerState) dispatch(onSetDesignerState(JSON.parse(designerState)));
  }, [instance]);

  useEffect(() => {
    dispatch(resetArticles());
    return () => {
      dispatch(resetArticles());
      dispatch(resetNewsletter());
      dispatch(resetDesigner());
    };
  }, []);

  const issueOptions = instances ? instances.map((value) => ({ label: value.attributes.title, value, key: value.id })) : [];
  const newsletterOptions = newsletters ? newsletters.map((value) => ({ label: value.attributes.title, value, key: value.id })) : [];

  return instance ? (
    <Designer />
  ) : (
    <Card className="h-100">
      <CardBody style={{ padding: 100, width: 600, maxWidth: 600, margin: 'auto' }}>
        <CardTitle className="text-center">Select Newsletter Issue</CardTitle>
        <Label className="mt-4">Newsletter</Label>
        <Select
          components={{ Input: CustomSelectInput }}
          className="react-select w-100"
          classNamePrefix="react-select"
          name="newsletter"
          options={newsletterOptions}
          onChange={onSelectNewsletter}
        />
        {newsletter && (
          <>
            <Label className="mt-4">Issue</Label>
            <Select
              components={{ Input: CustomSelectInput }}
              className="react-select w-100"
              classNamePrefix="react-select"
              name="issue"
              options={issueOptions}
              onChange={onSelectInstance}
            />
          </>
        )}
      </CardBody>
    </Card>
  );
};

EmailDesigner.propTypes = {
  match: PropTypes.object,
  history: PropTypes.object,
  location: PropTypes.object
};

export default EmailDesigner;
