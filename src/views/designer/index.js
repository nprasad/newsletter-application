import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route, Switch } from 'react-router-dom';
import PdfTest from 'components/pdf/PdfTest';

const EmailDesigner = React.lazy(() => import('./email-designer'));

const Designer = ({ match }) => (
  <Suspense fallback={<div className="loading" />}>
    <Switch>
      <Route path={`${match.url}/test-pdf`} render={(props) => <PdfTest {...props} />} />
      <Route path={`${match.url}/:issueId`} render={(props) => <EmailDesigner {...props} />} />
      <Route path={`${match.url}/`} render={(props) => <EmailDesigner {...props} />} />
      <Redirect to="/error" />
    </Switch>
  </Suspense>
);

Designer.propTypes = {
  match: PropTypes.object
};
export default Designer;
