import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Row, Button, Collapse, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { archiveNewsletter, deleteNewsletter, selectNewsletter } from 'redux/actions';
import Breadcrumb from 'containers/navs/Breadcrumb';
import { Separator, Colxx } from 'components/common/CustomBootstrap';
import IntlMessages from 'helpers/IntlMessages';
import Pagination from 'containers/pages/Pagination';
import { compare, search_match } from 'helpers/Utils';
import useNewsletters from 'hooks/use-newsletters';
import NewsletterDetails from 'containers/pages/NewsletterDetails';
import ConfirmationModal from 'components/common/ConfirmationModal';
import { getGroupState } from '../../redux/group/selectors';

const orderOptions = [
  { column: 'date', label: 'Date' },
  { column: 'title', label: 'Title' }
];
const viewOptions = [
  { column: 'all', label: 'All' },
  { column: 'archive', label: 'Archive' },
  { column: 'unarchive', label: 'Un Archive' }
];

const NewsletterList = ({ match, history }) => {
  const dispatch = useDispatch();
  const { newsletters } = useNewsletters();
  const [search, setSearch] = useState('');
  const [displayOptionsIsOpen, setDisplayOptionsIsOpen] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPage, setTotalPage] = useState(1);
  const [selectedPageSize] = useState(10);
  const [selectedOrderOption, setSelectedOrderOption] = useState({ column: 'date', label: 'Date' });
  const [selectedViewOption, setSelectedViewOption] = useState(viewOptions[0]);
  const [showConfirmation, setShowConfirmation] = useState(false);
  const [deleteId, setDeleteId] = useState(null);

  const onChangeOrderBy = (column) => {
    setSelectedOrderOption(orderOptions.find((x) => x.column === column));
  };

  const onChangeViewBy = (column) => {
    setSelectedViewOption(viewOptions.find((x) => x.column === column));
  };

  const onSearchKey = (e) => setSearch(e.target.value.toLowerCase());
  const onSelectNewsletter = (data) => dispatch(selectNewsletter(data));
  const filteredNewsletters = newsletters
    ? selectedViewOption.column === 'all'
      ? newsletters
      : newsletters.filter((n) => (selectedViewOption.column !== 'archive' ? n.attributes.field_archive : !n.attributes.field_archive))
    : [];

  useEffect(() => {
    if (filteredNewsletters) {
      setTotalPage(Math.ceil(filteredNewsletters.length / selectedPageSize));
    }
  }, [filteredNewsletters, selectedPageSize, currentPage, selectedOrderOption, selectedViewOption, search]);

  const startIndex = (currentPage - 1) * selectedPageSize;
  const endIndex = currentPage * selectedPageSize;

  const onDeleteNewsletter = (id) => dispatch(deleteNewsletter(id));
  const onArchiveNewsletter = (id, value) => dispatch(archiveNewsletter(id, value));

  const finalNewsletters = search_match(search, filteredNewsletters, ['attributes', 'title'], true).sort((a, b) => {
    const { column } = selectedOrderOption;
    let key = 'title';
    let isDate = false;
    if (column === 'date') {
      key = 'created';
      isDate = true;
    }
    return compare(a.attributes[key], b.attributes[key], false, isDate);
  });

  return (
    <>
      <Row>
        <Colxx xxs="12">
          <Breadcrumb heading="menu.blog-list" match={match} />
          <div className="text-zero top-right-button-container">
            <Button color="primary" size="sm" className="top-right-button" onClick={() => history.push('/newsletter/add')}>
              <IntlMessages id="pages.add-new" />
            </Button>
          </div>
          <div className="mb-2">
            <Button
              color="empty"
              className="pt-0 pl-0 d-inline-block d-md-none"
              onClick={() => setDisplayOptionsIsOpen(!displayOptionsIsOpen)}
            >
              <IntlMessages id="pages.display-options" /> <i className="simple-icon-arrow-down align-middle" />
            </Button>
            <Collapse isOpen={displayOptionsIsOpen} className="d-md-block" id="displayOptions">
              <div className="d-block d-md-inline-block pt-1">
                <UncontrolledDropdown className="mr-1 float-md-left btn-group mb-1">
                  <DropdownToggle caret color="outline-dark" size="xs">
                    <IntlMessages id="pages.orderby" />
                    {selectedOrderOption.label}
                  </DropdownToggle>
                  <DropdownMenu>
                    {orderOptions.map((order, index) => {
                      return (
                        <DropdownItem key={index} onClick={() => onChangeOrderBy(order.column)}>
                          {order.label}
                        </DropdownItem>
                      );
                    })}
                  </DropdownMenu>
                </UncontrolledDropdown>

                <UncontrolledDropdown className="mr-1 float-md-left btn-group mb-1">
                  <DropdownToggle caret color="outline-dark" size="xs">
                    Show {selectedViewOption.label}
                  </DropdownToggle>
                  <DropdownMenu>
                    {viewOptions.map((order, index) => {
                      return (
                        <DropdownItem key={index} onClick={() => onChangeViewBy(order.column)}>
                          {order.label}
                        </DropdownItem>
                      );
                    })}
                  </DropdownMenu>
                </UncontrolledDropdown>

                <div className="search-sm d-inline-block float-md-left mr-1 mb-1 align-top">
                  <input type="text" name="keyword" id="search" placeholder="Search" onChange={onSearchKey} />
                </div>
              </div>
            </Collapse>
          </div>
          <Separator className="mb-5" />
        </Colxx>
        {finalNewsletters &&
          finalNewsletters.slice(startIndex, endIndex).map((element, index) => {
            const {
              id,
              attributes: { field_archive }
            } = element;

            return (
              <Colxx xxs="12" lg="6" className="mb-5" key={`node_${index}_${id}`}>
                <NewsletterDetails newsletter={element}>
                  <div className="d-flex justify-content-between">
                    <div className="ml-4">
                      <div className="mb-2 btn-group">
                        <Button color="primary" size="xs" onClick={() => history.push(`/newsletter/add/${id}`)}>
                          Edit
                        </Button>
                        <Button color="info" size="xs" onClick={() => onArchiveNewsletter(id, !field_archive)}>
                          {`${field_archive ? 'Un ' : ''}Archive`}
                        </Button>
                        <Button
                          color="danger"
                          size="xs"
                          onClick={() => {
                            setShowConfirmation(true);
                            setDeleteId(id);
                          }}
                        >
                          Delete
                        </Button>
                      </div>
                    </div>
                    <NavLink to={`details/${id}`} onClick={() => onSelectNewsletter(element)}>
                      <Button color="link" size="lg">
                        See More
                      </Button>
                    </NavLink>
                  </div>
                </NewsletterDetails>
              </Colxx>
            );
          })}
      </Row>
      <ConfirmationModal
        isOpen={showConfirmation}
        text="Are you sure that you want to delete this content?"
        onCancel={() => setShowConfirmation(false)}
        onConfirm={() => {
          if (deleteId) onDeleteNewsletter(deleteId);
          setShowConfirmation(false);
        }}
      />
      <Pagination currentPage={currentPage} totalPage={totalPage} onChangePage={setCurrentPage} />
    </>
  );
};

NewsletterList.propTypes = { match: PropTypes.object, history: PropTypes.object };

export default NewsletterList;
