/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  Row,
  Card,
  CardBody,
  Badge,
  Pagination,
  PaginationItem,
  PaginationLink,
  Button,
  Collapse,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';
import LinesEllipsis from 'react-lines-ellipsis';
import responsiveHOC from 'react-lines-ellipsis/lib/responsiveHOC';
import { NavLink } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { fetchArticles } from 'redux/actions';
import Breadcrumb from 'containers/navs/Breadcrumb';
import { Separator, Colxx } from 'components/common/CustomBootstrap';
import IntlMessages from 'helpers/IntlMessages';

const ResponsiveEllipsis = responsiveHOC()(LinesEllipsis);
const orderOptions = [
  { column: 'date', label: 'Date' },
  { column: 'category', label: 'Category' }
];

const ArticleList = ({ match }) => {
  const dispatch = useDispatch();
  const { articles } = useSelector((state) => state.article);
  const [displayOptionsIsOpen, setDisplayOptionsIsOpen] = useState(false);
  const [selectedOrderOption, setSelectedOrderOption] = useState({
    column: 'date',
    label: 'Date'
  });

  const onChangeOrderBy = (column) => {
    setSelectedOrderOption(orderOptions.find((x) => x.column === column));
  };

  const onSearchKey = (e) => {
    console.log(e);
  };

  useEffect(() => {
    if (!articles.length) {
      dispatch(fetchArticles());
    }
  }, [dispatch]);

  return (
    <>
      <Row>
        <Colxx xxs="12">
          <Breadcrumb heading="menu.blog-list" match={match} />
          <div className="mb-2">
            <Button
              color="empty"
              className="pt-0 pl-0 d-inline-block d-md-none"
              onClick={() => setDisplayOptionsIsOpen(!displayOptionsIsOpen)}
            >
              <IntlMessages id="pages.display-options" /> <i className="simple-icon-arrow-down align-middle" />
            </Button>
            <Collapse isOpen={displayOptionsIsOpen} className="d-md-block" id="displayOptions">
              <div className="d-block d-md-inline-block pt-1">
                <UncontrolledDropdown className="mr-1 float-md-left btn-group mb-1">
                  <DropdownToggle caret color="outline-dark" size="xs">
                    <IntlMessages id="pages.orderby" />
                    {selectedOrderOption.label}
                  </DropdownToggle>
                  <DropdownMenu>
                    {orderOptions.map((order, index) => {
                      return (
                        <DropdownItem key={index} onClick={() => onChangeOrderBy(order.column)}>
                          {order.label}
                        </DropdownItem>
                      );
                    })}
                  </DropdownMenu>
                </UncontrolledDropdown>
                <div className="search-sm d-inline-block float-md-left mr-1 mb-1 align-top">
                  <input type="text" name="keyword" id="search" placeholder="Search" onKeyPress={(e) => onSearchKey(e)} />
                </div>
              </div>
            </Collapse>
          </div>
          <Separator className="mb-5" />
        </Colxx>
        {articles &&
          articles.map(({ attributes }, index) => {
            const strap = attributes.field_strap ? attributes.field_strap.value : '';
            return (
              <Colxx xxs="12" lg="6" className="mb-5" key={`node_${index}`}>
                <Card className="flex-row listing-card-container h-100">
                  <div className="w-40 position-relative">
                    {attributes.field_image_thumb && (
                      <NavLink to="detail">
                        <img className="card-img-left" src={attributes.field_image_thumb} alt="thumb" />
                      </NavLink>
                    )}
                  </div>
                  <div className="w-60 d-flex align-items-center">
                    <CardBody>
                      <NavLink to="detail">
                        <ResponsiveEllipsis
                          className="mb-3 listing-heading"
                          text={attributes.title}
                          maxLine="2"
                          trimRight
                          basedOn="words"
                          component="h5"
                        />
                      </NavLink>
                      <p className="mb-1 text-muted text-small">{moment(attributes.created).format('D MMM, YYYY')}</p>
                      <ResponsiveEllipsis
                        className="listing-desc text-muted"
                        text={strap}
                        maxLine="3"
                        trimRight
                        basedOn="words"
                        component="p"
                      />
                      {attributes.category && (
                        <Badge color="primary" pill>
                          {attributes.category}
                        </Badge>
                      )}
                    </CardBody>
                  </div>
                </Card>
              </Colxx>
            );
          })}
      </Row>
      <Row>
        <Colxx xxs="12">
          <Pagination listClassName="justify-content-center">
            <PaginationItem>
              <PaginationLink className="prev" href="#">
                <i className="simple-icon-arrow-left" />
              </PaginationLink>
            </PaginationItem>
            <PaginationItem active>
              <PaginationLink href="#">1</PaginationLink>
            </PaginationItem>
            <PaginationItem>
              <PaginationLink href="#">2</PaginationLink>
            </PaginationItem>
            <PaginationItem>
              <PaginationLink href="#">3</PaginationLink>
            </PaginationItem>
            <PaginationItem>
              <PaginationLink className="next" href="#">
                <i className="simple-icon-arrow-right" />
              </PaginationLink>
            </PaginationItem>
          </Pagination>
        </Colxx>
      </Row>
    </>
  );
};

ArticleList.propTypes = { match: PropTypes.object };

export default ArticleList;
