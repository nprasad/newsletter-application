/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { v4 as uuidv4 } from 'uuid';
import { Row, Card, CardBody, CardTitle, Label, Input, Button, Collapse } from 'reactstrap';
import Breadcrumb from 'containers/navs/Breadcrumb';
import ReactDOMServer from 'react-dom/server';
import { Separator, Colxx } from 'components/common/CustomBootstrap';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { useDispatch, useSelector } from 'react-redux';
import {
  deleteInstance,
  editInstance,
  fetchArticles,
  fetchInstance,
  fetchInstances,
  fetchNewsletterInstances,
  postInstance,
  resetNewsletter,
  setLoading
} from 'redux/actions';
import Select from 'react-select';
import CustomSelectInput from 'components/common/CustomSelectInput';
import { useHistory } from 'react-router-dom';
import useNewsletters from 'hooks/use-newsletters';
import { calculateIssue, decodeHtml, getFontsFamily } from 'helpers/Utils';
import ConfirmationModal from 'components/common/ConfirmationModal';
import { ChromePicker } from 'react-color';
import PdfContent from 'components/pdf/PdfContent';
import Api from 'helpers/Api';
import { NotificationManager } from 'components/common/react-notifications';

const createPdf = async (article, sidebarArticle, articles, articlesSection, options, lang) => {
  const { pdfTemplate, ...restOptions } = options;
  const isMultiColumn = pdfTemplate === 'mcol';
  const html = ReactDOMServer.renderToStaticMarkup(
    <PdfContent
      article={article}
      articles={articles}
      articlesSection={articlesSection}
      sidebarArticle={sidebarArticle}
      options={restOptions}
      isMultiColumn={isMultiColumn}
      lang={lang}
    />
  );
  const decodedHtml = decodeHtml(html);

  const url = process.env.PUBLIC_URL + '/api/create-pdf';

  //const url = 'https://newsletter-ui.web.cern.ch/api/create-pdf'; // Keep for development
  //const newWin = window.open(); // Debug
  //newWin.document.write(html);

  const response = await fetch(url, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ html: decodedHtml })
  });
  if (response.ok) {
    const buffer = await response.arrayBuffer();
    // await uploadPdf(buffer);
    const blob = new Blob([buffer], { type: 'application/pdf' });
    const blobURL = URL.createObjectURL(blob);
    window.open(blobURL);
    return buffer;
  }
};

const INITIAL_FORM = {
  title: '',
  newsletter: '',
  schedule: new Date(),
  cookiesPolicy: '',
  privacy: '',
  HTML: '',
  GTracking: '',
  MTracking: '',
  articleIDs: '',
  newsletterFormat: 'both',
  pdfTemplate: 'mcol',
  originatorEmail: ''
};

const AddNewIssue = ({ match }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { newsletters } = useNewsletters();
  const {
    instance,
    instances,
    newsletterInstances,
    newsletter
  } = useSelector((state) => state.newsletter);
  const lang = newsletter?.attributes?.title.includes('(French)') ? 'fr' : 'en';
  const { articles } = useSelector((state) => state.article);
  const { newsletterId, issueId } = match.params;
  const [form, setForm] = useState(() => ({ ...INITIAL_FORM, newsletter: newsletterId }));
  const [template, setTemplate] = useState({ newsletter: null, issue: null, clone: null });
  const [showConfirmation, setShowConfirmation] = useState(false);
  const [pdfTitle, setPdfTitle] = useState('');
  const [pdfIssueTitle, setPdfIssueTitle] = useState('');
  const [pdfFeaturedArticle, setPdfFeaturedArticle] = useState(null);
  const [pdfArticles, setPdfArticles] = useState([]);
  const [initialPdfArticles, setInitialPdfArticles] = useState([]);
  const [pdfArticlesSections, setPdfArticlesSections] = useState([]);
  const [togglePdfStyles, setTogglePdfStyles] = useState(false);
  const [pdfStyles, setPdfStyles] = useState({ color: '#0055a8', fontFamily: 'sans-serif' });
  const [pdfBuffer, setPdfBuffer] = useState(null);
  const [fonts, setFonts] = useState([]);

  const handleChange = (event) => {
    const { name, value } = event.target;
    setForm({ ...form, [name]: value });
  };
  const onChangeShedule = (value) => {
    setForm({ ...form, schedule: value });
  };
  const handleChangeNewsletter = ({ value }) => setForm({ ...form, newsletter: value });
  const handleChangeNewsletterFormat = ({ value }) => setForm({ ...form, newsletterFormat: value });
  const handleChangePdfTemplate = ({ value }) => setForm({ ...form, pdfTemplate: value });

  const onSave = () => {
    const data = {
      type: 'node--instance',
      attributes: {
        title: form.title,
        body: form.HTML,
        field_cookies_policy: form.cookiesPolicy,
        field_privacy: form.privacy,
        field_schedule: moment(form.schedule).format(),
        field_gtracking: form.GTracking,
        field_mtracking: form.MTracking,
        field_newsletterformat: form.newsletterFormat,
        field_articleids: form.articleIDs,
        field_pdf_template: form.pdfTemplate,
        field_email: form.originatorEmail,
        field_designer_object: template.clone ? JSON.stringify(template.clone) : undefined
      },
      relationships: {
        field_newsletter: { data: form.newsletter ? { id: form.newsletter, type: 'node--newsletter' } : null }
      }
    };
    return instance ? dispatch(editInstance({ data: { ...data, id: instance.id }, id: instance.id })) : dispatch(postInstance(data));
  };

  const getPdfSidebarArticle = () => {
    const designerState = instance && instance.attributes.field_designer_object;
    const { components } = JSON.parse(designerState);
    if (components) {
      const key = Object.keys(components).find((key) => key.startsWith('emptyArticleSidebar'));
      const sidebarArticle = components[key];
      if (sidebarArticle && sidebarArticle.props) {
        const {
          props: { article, content, title }
        } = sidebarArticle;
        return { article, content, title };
      }
    }
    return null;
  };

  const onCreatePdf = async () => {
    const pdfOptions = {
      styles: pdfStyles,
      pdfTemplate: form.pdfTemplate,
      header: { title: pdfTitle, info: [pdfIssueTitle || form.title, 'http://home.cern/cern-people'] },
      firstPage: {}
    };

    const _articles = articles && articles.filter(({ attributes: { drupal_internal__nid } }) => pdfArticles.includes(drupal_internal__nid));
    const featuredArticle = articles.find(({ attributes: { drupal_internal__nid } }) => drupal_internal__nid === pdfFeaturedArticle);
    const articlesSection = pdfArticlesSections.map((s) => ({
      ...s,
      articles: articles ? articles.filter(({ attributes: { drupal_internal__nid } }) => s.articles.includes(drupal_internal__nid)) : []
    }));
    const sidebarArticle = getPdfSidebarArticle();
    dispatch(setLoading('newsletter', true));
    const buffer = await createPdf(
      featuredArticle,
      sidebarArticle,
      _articles,
      articlesSection,
      pdfOptions,
      lang
    );
    dispatch(setLoading('newsletter', false));
    setPdfBuffer(buffer);
  };

  const onUploadPdf = async () => {
    const response = await Api.upload(`node/instance/${instance.id}/field_field_pdf`, Buffer.from(pdfBuffer, 'binary'));
    if (response.status === 200) {
      NotificationManager.success(true, `PDF Saved!`, 3000, null, null, '');
    }
  };

  const toggleConfirmation = () => setShowConfirmation(!showConfirmation);

  const deleteIssue = () => {
    dispatch(deleteInstance(instance.id));
    toggleConfirmation();
    history.push(`/`);
  };

  const goToDesigner = () => history.push(`/designer/${instance.id}`);

  const cloneIssue = () => {
    const designerState = template.issue && template.issue.attributes.field_designer_object;
    const designerStateObj = JSON.parse(designerState);
    const { components, droppables } = designerStateObj;
    const newComponents = Object.keys(components).reduce(
      (acc, curr) =>
        !['articlesWidget', 'article', 'emptyArticle', 'emptyArticleSidebar', 'titleWidget'].some((c) => curr.split('-')[0] === c)
          ? { ...acc, [curr]: components[curr] }
          : acc,
      {}
    );
    const clone = {
      ...designerStateObj,
      components: newComponents,
      droppables: { ...droppables, article: { id: 'article', title: 'article', componentIds: [] } },
      html: ''
    };
    setTemplate({ ...template, clone });
  };

  const addNewPdfArticleSection = () => {
    const newSection = { id: uuidv4(), title: '', articles: [] };
    setPdfArticlesSections([...pdfArticlesSections, newSection]);
  };

  const onChangeSectionTitle = (id, title) => {
    const curr = pdfArticlesSections.find((s) => s.id === id);
    if (curr) {
      curr.title = title;
      const filteredOut = pdfArticlesSections.filter((s) => s.id !== id);
      setPdfArticlesSections([...filteredOut, curr]);
    }
  };

  const onChangeSectionArticles = (id, articles) => {
    const curr = pdfArticlesSections.find((s) => s.id === id);
    if (curr) {
      curr.articles = articles;
      const filteredOut = pdfArticlesSections.filter((s) => s.id !== id);
      setPdfArticlesSections([...filteredOut, curr]);
      const filteredPdfArticles = pdfArticles.filter((a) => !articles.includes(a));
      setPdfArticles(filteredPdfArticles);
    }
  };

  useEffect(() => {
    if (issueId) dispatch(fetchInstance(issueId));
    return () => dispatch(resetNewsletter());
  }, [issueId]);

  useEffect(() => {
    if (form.newsletter && newsletters) {
      dispatch(fetchNewsletterInstances(form.newsletter));
      const _newsletter = newsletters.find((n) => n.id === form.newsletter);
      const newsletterNID = _newsletter.attributes.drupal_internal__nid;
      const query = newsletterNID ? `?filter[field_source]=${newsletterNID}` : '';
      dispatch(fetchArticles(query));
    }
  }, [form.newsletter, newsletters]);

  useEffect(() => {
    if (!instance) {
      const latestIssue =
        newsletterInstances &&
        newsletterInstances.length &&
        newsletterInstances.reduce((a, b) => (a.attributes.field_schedule > b.attributes.field_schedule ? a : b));
      if (latestIssue) {
        const {
          attributes: { title }
        } = latestIssue;
        setForm({ ...form, title: calculateIssue(title) });
      }
    }
  }, [newsletterInstances]);

  useEffect(() => {
    if (instance) {
      const {
        attributes: {
          title,
          body,
          field_cookies_policy,
          field_privacy,
          field_schedule,
          field_gtracking,
          field_mtracking,
          field_newsletterformat,
          field_pdf_template,
          field_articleids,
          field_email
        },
        relationships: { field_newsletter, field_field_pdf }
      } = instance;

      setForm({
        ...form,
        title: title,
        newsletter: field_newsletter && field_newsletter.data ? field_newsletter.data.id : '',
        schedule: new Date(field_schedule),
        cookiesPolicy: field_cookies_policy ? field_cookies_policy.value : '',
        privacy: field_privacy ? field_privacy.value : '',
        HTML: body ? body.value : '',
        GTracking: field_gtracking || '',
        MTracking: field_mtracking || '',
        newsletterFormat: field_newsletterformat,
        pdfTemplate: field_pdf_template,
        articleIDs: field_articleids || '',
        originatorEmail: field_email || ''
      });

      if (field_articleids) {
        const articlesNID = field_articleids.split(',').map(Number);
        setPdfArticles(articlesNID);
        setInitialPdfArticles(articlesNID);
      }

      if (field_field_pdf.data === null && ['both', 'pdf'].includes(field_newsletterformat)) {
        NotificationManager.warning('You must Generate Pdf and Save Pdf', 'Not PDF Found', 5000);
      }
    }
  }, [instance]);

  useEffect(() => {
    if (template.newsletter) {
      const query = `?filter[field_newsletter.id]=${template.newsletter}`;
      dispatch(fetchInstances(query));
    }
  }, [template]);

  useEffect(() => {
    async function getFonts() {
      const _fonts = await getFontsFamily();
      setFonts(_fonts);
    }
    getFonts();
  }, []);

  const newsletterOptions = newsletters ? newsletters.map(({ attributes: { title }, id }) => ({ label: title, value: id, key: id })) : [];
  const pdfOptions = [
    { key: 'pdf-template-scol', value: 'scol', label: 'Single Column' },
    { key: 'pdf-template-mcol', value: 'mcol', label: 'Multi Column' }
  ];
  const newsletterFormatOptions = [
    { key: 'newsletter-format-both', value: 'both', label: 'Both' },
    { key: 'newsletter-format-html', value: 'html', label: 'Html' },
    { key: 'newsletter-format-pdf', value: 'pdf', label: 'Pdf' }
  ];
  const issueOptions = instances ? instances.map((value) => ({ label: value.attributes.title, value, key: value.id })) : [];
  const isPdf = Boolean(['both', 'pdf'].includes(form.newsletterFormat) && instance);
  const isValidPdf = Boolean(form.newsletter && pdfArticles.length > 0 && pdfFeaturedArticle && pdfTitle && pdfIssueTitle);
  const isValid = Boolean(form.title && form.newsletter);

  const articleOptions = articles
    ? articles.map(({ id, attributes: { title, drupal_internal__nid } }) => ({
        label: title,
        value: drupal_internal__nid,
        key: id
      }))
    : [];

  const articleSectionOptions = articleOptions.filter((item) => initialPdfArticles.includes(item.value));

  const fontOptions = fonts.map((font) => ({
    label: font,
    value: font,
    key: font
  }));

  return (
    <>
      <Row>
        <Colxx xxs="12">
          <Breadcrumb heading="menu.newsletter-add-issue" match={match} />
          <Separator className="mb-5" />
        </Colxx>
      </Row>

      <Row>
        <Colxx xxs="12" md="12" xl="8" className="col-left">
          <Card className="h-100">
            <CardBody>
              <Label className="mt-4">Newsletter</Label>
              <Select
                  components={{ Input: CustomSelectInput }}
                  className="react-select"
                  classNamePrefix="react-select"
                  options={newsletterOptions}
                  value={newsletterOptions.find((item) => form.newsletter === item.value)}
                  onChange={handleChangeNewsletter}
                  isDisabled={instance}
              />
              <Label className="mt-4">Issue Title</Label>
              <Input name="title" value={form.title} onChange={handleChange} placeholder="Issue No. 01-02/2020" required />
              <Label className="mt-4">HTML</Label>
              <Input type="textarea" name="HTML" value={form.HTML} onChange={handleChange} />
              <Label className="mt-4">Cookies Policy</Label>
              <Input type="textarea" name="cookiesPolicy" value={form.cookiesPolicy} onChange={handleChange} />
              <Label className="mt-4">Privacy</Label>
              <Input type="textarea" name="privacy" value={form.privacy} onChange={handleChange} />
              <Label className="mt-4">GTracking</Label>
              <Input type="textarea" name="GTracking" value={form.GTracking} onChange={handleChange} />
              <Label className="mt-4">MTracking</Label>
              <Input type="textarea" name="MTracking" value={form.MTracking} onChange={handleChange} />
              <Label className="mt-4">ArticleIDs</Label>
              <Input type="textarea" name="articleIDs" value={form.articleIDs} onChange={handleChange} />
              <Label className="mt-4">Originator email</Label>
              <Input name="originatorEmail" value={form.originatorEmail} onChange={handleChange} />
              <Label className="mt-4">Newsletter Format</Label>
              <Select
                components={{ Input: CustomSelectInput }}
                className="react-select"
                classNamePrefix="react-select"
                options={newsletterFormatOptions}
                value={newsletterFormatOptions.find((item) => form.newsletterFormat === item.value)}
                onChange={handleChangeNewsletterFormat}
              />

              <Button className="mt-4" color="primary" onClick={onSave} disabled={!isValid}>
                {instance ? 'Save Edited' : 'Save'} Issue
              </Button>

              {instance && (
                <>
                  <Button className="ml-3 mt-4" color="danger" onClick={toggleConfirmation}>
                    Delete Issue
                  </Button>
                  <Button className="ml-3 mt-4" color="info" onClick={goToDesigner}>
                    Go To Designer
                  </Button>
                </>
              )}
            </CardBody>
          </Card>
        </Colxx>

        <Colxx xxs="12" md="12" xl="4" className="col-left">
          {isPdf && (
            <Card className="mb-4">
              <CardBody>
                <CardTitle>PDF Options</CardTitle>
                <Label className="mt-4">Pdf Title*</Label>
                <Input name="title" value={pdfTitle} onChange={(e) => setPdfTitle(e.target.value)} placeholder="For example: CERN Bulletin" required />
                <Label className="mt-4">Issue Title - Date*</Label>
                <Input
                  name="title"
                  value={pdfIssueTitle}
                  onChange={(e) => setPdfIssueTitle(e.target.value)}
                  placeholder="For example: Issue No. 10-11/2021 - Wednesday 10 March 2021"
                  required
                />
                <Label className="mt-4">Pdf Template*</Label>
                <Select
                  components={{ Input: CustomSelectInput }}
                  className="react-select"
                  classNamePrefix="react-select"
                  options={pdfOptions}
                  value={pdfOptions.find((item) => form.pdfTemplate === item.value)}
                  onChange={handleChangePdfTemplate}
                />

                <Label className="mt-4">Select Featured Article*</Label>
                <Select
                  components={{ Input: CustomSelectInput }}
                  className="react-select"
                  classNamePrefix="react-select"
                  options={articleOptions}
                  value={articleOptions.find((item) => pdfFeaturedArticle === item.value)}
                  onChange={({ value }) => setPdfFeaturedArticle(value)}
                />

                <Label className="mt-4">Select Articles</Label>
                <Select
                  isMulti
                  components={{ Input: CustomSelectInput }}
                  className="react-select"
                  options={articleOptions}
                  value={articleOptions.filter((item) => pdfArticles.includes(item.value))}
                  onChange={(values) => setPdfArticles(values ? values.map(({ value }) => value) : [])}
                />

                <Button className="pl-0 w-100 text-left" color="link" size="lg" onClick={addNewPdfArticleSection}>
                  Add New Articles Section
                </Button>

                {pdfArticlesSections.length > 0 &&
                  pdfArticlesSections.map((section) => (
                    <React.Fragment key={section.id}>
                      <Label className="mt-4">Section Title</Label>
                      <Input
                        value={section.title}
                        onChange={(e) => onChangeSectionTitle(section.id, e.target.value)}
                        placeholder="Announcements"
                      />
                      <Label className="mt-4">Select Articles</Label>
                      <Select
                        isMulti
                        components={{ Input: CustomSelectInput }}
                        className="react-select"
                        options={articleSectionOptions}
                        value={articleSectionOptions.filter((item) => section.articles.includes(item.value))}
                        onChange={(values) => onChangeSectionArticles(section.id, values ? values.map(({ value }) => value) : [])}
                      />
                    </React.Fragment>
                  ))}

                {!pdfBuffer ? (
                  <Button className="btn-block mt-4" color="primary" disabled={!isValidPdf} onClick={onCreatePdf}>
                    Generate PDF
                  </Button>
                ) : (
                  <div className="mt-4 btn-group btn-block">
                    <Button color="primary" onClick={onUploadPdf}>
                      Save PDF
                    </Button>
                    <Button color="info" onClick={onCreatePdf}>
                      Generate PDF
                    </Button>
                  </div>
                )}
                <Button
                  className="mt-4 btn btn-info"
                  color="info"
                  onClick={() => setTogglePdfStyles(!togglePdfStyles)}
                >
                  Set Styles
                </Button>
                <Collapse isOpen={togglePdfStyles} className="mt-4">
                  <Label>Primary Color</Label>
                  <ChromePicker color={pdfStyles.color} onChange={(color) => setPdfStyles({ ...pdfStyles, color: color.hex })} />
                  <Label className="mt-4">Font Family:</Label>
                  <Select
                    components={{ Input: CustomSelectInput }}
                    className="react-select"
                    name="font-family"
                    options={fontOptions}
                    value={fontOptions.find((item) => pdfStyles.fontFamily === item.value)}
                    onChange={({ value }) => setPdfStyles({ ...pdfStyles, fontFamily: value })}
                  />
                </Collapse>
              </CardBody>
            </Card>
          )}

          <Card>
            <CardBody>
              <CardTitle>Select Template</CardTitle>
              <Label className="mt-4">Newsletter</Label>
              <Select
                components={{ Input: CustomSelectInput }}
                className="react-select w-100"
                classNamePrefix="react-select"
                name="newsletter"
                options={newsletterOptions}
                onChange={({ value }) => setTemplate({ ...template, newsletter: value })}
              />
              {template.newsletter && (
                <>
                  <Label className="mt-4">Issue</Label>
                  <Select
                    components={{ Input: CustomSelectInput }}
                    className="react-select w-100"
                    classNamePrefix="react-select"
                    name="issue"
                    options={issueOptions}
                    onChange={({ value }) => setTemplate({ ...template, issue: value })}
                  />

                  {template.issue && (
                    <Button className="btn-block mt-4" color="primary" onClick={cloneIssue}>
                      Clone
                    </Button>
                  )}
                </>
              )}
            </CardBody>
          </Card>

          <Card className="mt-4">
            <CardBody>
              <CardTitle>Schedule</CardTitle>
              <DatePicker
                calendarClassName="embedded d-flex"
                inline
                showTimeSelect
                timeIntervals={10}
                selected={form.schedule}
                onChange={onChangeShedule}
              />
            </CardBody>
          </Card>
        </Colxx>
      </Row>
      <ConfirmationModal
        isOpen={showConfirmation}
        text="Are you sure that you want to delete this content?"
        onCancel={toggleConfirmation}
        onConfirm={deleteIssue}
      />
    </>
  );
};

AddNewIssue.propTypes = { match: PropTypes.object };

export default AddNewIssue;
