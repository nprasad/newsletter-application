/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Row } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import Breadcrumb from 'containers/navs/Breadcrumb';
import { Separator, Colxx } from 'components/common/CustomBootstrap';
import { deleteNewsletter, fetchNewsletter, fetchNewsletterInstances } from 'redux/actions';
import Issues from 'containers/dashboards/Issues';
import NewsletterDetails from 'containers/pages/NewsletterDetails';
import ConfirmationModal from 'components/common/ConfirmationModal';

const NewsletterDetail = ({ match, history }) => {
  const dispatch = useDispatch();
  const { newsletter, newsletterInstances } = useSelector((state) => state.newsletter);
  const [showConfirmation, setShowConfirmation] = useState(false);
  const newsletterId = match.params.id;

  useEffect(() => {
    if (!newsletter) {
      dispatch(fetchNewsletter(newsletterId));
    }

    if (!newsletterInstances) {
      dispatch(fetchNewsletterInstances(newsletterId));
    }
  }, [newsletter, newsletterInstances, dispatch]);

  const onDeleteNewsletter = () => {
    dispatch(deleteNewsletter(newsletterId));
    setShowConfirmation(false);
    history.push('/newsletter');
  };

  const toggleConfirmation = () => setShowConfirmation(!showConfirmation);

  return (
    <>
      <Row>
        <Colxx xxs="12">
          <Breadcrumb heading="menu.newsletter-detail" match={match} />
          <div className="text-zero top-right-button-container btn-group">
            <Button color="primary" size="sm" onClick={() => history.push(`/newsletter/add/${newsletterId}`)}>
              Edit Newsletter
            </Button>
            <Button color="danger" size="sm" onClick={toggleConfirmation}>
              Delete Newsletter
            </Button>
          </div>
        </Colxx>
        <Colxx xxs="12">
          <Separator className="mb-5" />
        </Colxx>
      </Row>

      {newsletter && newsletterInstances && (
        <Row>
          <Colxx xxs="8">
            <NewsletterDetails newsletter={newsletter} isFull />
          </Colxx>
          <Colxx xxs="4">
            <Issues className="mb-4" issues={newsletterInstances} newsletterId={newsletterId} height="100%" />
          </Colxx>
        </Row>
      )}

      <ConfirmationModal
        isOpen={showConfirmation}
        text="Are you sure that you want to delete this content?"
        onCancel={toggleConfirmation}
        onConfirm={onDeleteNewsletter}
      />
    </>
  );
};

NewsletterDetail.propTypes = { match: PropTypes.object, history: PropTypes.object };

export default NewsletterDetail;
