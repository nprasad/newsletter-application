import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route, Switch } from 'react-router-dom';

const NewsletterList = React.lazy(() => import('./newsletter-list'));
const ArticleList = React.lazy(() => import('./article-list'));
const NewsletterDetail = React.lazy(() => import('./newsletter-detail'));
const NewsletterAdd = React.lazy(() => import('./newsletter-add'));
const AddNewIssue = React.lazy(() => import('./add-new-issue'));

const PagesNewsletter = ({ match }) => (
  <Suspense fallback={<div className="loading" />}>
    <Switch>
      <Redirect exact from={`${match.url}/`} to={`${match.url}/list`} />
      <Route path={`${match.url}/list`} render={(props) => <NewsletterList {...props} />} />
      <Route path={`${match.url}/article-list`} render={(props) => <ArticleList {...props} />} />
      <Route path={`${match.url}/details/:id`} render={(props) => <NewsletterDetail {...props} />} />
      <Route path={`${match.url}/add/:id`} render={(props) => <NewsletterAdd {...props} />} />
      <Route path={`${match.url}/add`} render={(props) => <NewsletterAdd {...props} />} />
      <Route path={`${match.url}/:newsletterId/issue-add`} render={(props) => <AddNewIssue {...props} />} />
      <Route path={`${match.url}/issue-add/:issueId`} render={(props) => <AddNewIssue {...props} />} />
      <Route path={`${match.url}/issue-add`} render={(props) => <AddNewIssue {...props} />} />
      <Redirect to="/error" />
    </Switch>
  </Suspense>
);

PagesNewsletter.propTypes = {
  match: PropTypes.object
};
export default PagesNewsletter;
