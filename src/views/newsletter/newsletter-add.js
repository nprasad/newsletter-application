/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Row, Card, CardBody, CardTitle } from 'reactstrap';
import Breadcrumb from 'containers/navs/Breadcrumb';
import { Separator, Colxx } from 'components/common/CustomBootstrap';
import AddNewsletter from 'containers/pages/AddNewsletter';
import LatestNewsletters from 'containers/pages/LatestNewsletters';
import useNewsletters from 'hooks/use-newsletters';

const NewsletterAdd = ({ match }) => {
  const [selectedNewsletter, setSelectedNewsletter] = useState(null);
  const [mode, setMode] = useState('');
  const { newsletters } = useNewsletters();
  const newsletterId = match.params.id;

  const onClone = (newsletter) => {
    setSelectedNewsletter(newsletter);
    setMode('clone');
  };

  useEffect(() => {
    if (newsletters && newsletterId) {
      const selected = newsletters.find((n) => n.id === newsletterId);
      if (selected) {
        setSelectedNewsletter(selected);
        setMode('edit');
      }
    }
  }, [newsletters, newsletterId]);

  return (
    <>
      <Row>
        <Colxx xxs="12">
          <Breadcrumb heading="menu.newsletter-add" match={match} />
          <Separator className="mb-5" />
        </Colxx>
      </Row>

      <Row>
        <Colxx xxs="12" md="12" xl="7" className="col-left">
          <Card className="h-100">
            <CardBody>
              <AddNewsletter selectedNewsletter={selectedNewsletter} mode={mode} />
            </CardBody>
          </Card>
        </Colxx>

        <Colxx xxs="12" md="12" xl="5" className="col-left">
          <Card className="h-100">
            <CardBody>
              <CardTitle>Latest newsletters</CardTitle>
              <LatestNewsletters counter={8} newsletters={newsletters} onClone={onClone} />
            </CardBody>
          </Card>
        </Colxx>
      </Row>
    </>
  );
};

NewsletterAdd.propTypes = { match: PropTypes.object };

export default NewsletterAdd;
