import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route, Switch } from 'react-router-dom';

const AnalyticsDefault = React.lazy(() => import(/* webpackChunkName: "dashboard-analytics" */ './analytics'));

const Dashboards = ({ match }) => (
  <Suspense fallback={<div className="loading" />}>
    <Switch>
      <Redirect exact from={`${match.url}/`} to={`${match.url}/analytics`} />
      <Route path={`${match.url}/analytics`} render={(props) => <AnalyticsDefault {...props} />} />
      <Redirect to="/error" />
    </Switch>
  </Suspense>
);

Dashboards.propTypes = {
  match: PropTypes.object
};
export default Dashboards;
