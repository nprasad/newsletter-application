import React from 'react';
import PropTypes from 'prop-types';
import { useIntl } from 'react-intl';
import { Row } from 'reactstrap';
import { Colxx, Separator } from '../../components/common/CustomBootstrap';
import Breadcrumb from '../../containers/navs/Breadcrumb';
import GradientWithRadialProgressCard from 'components/cards/GradientWithRadialProgressCard';

import LatestArticles from 'containers/dashboards/LatestArticles';
import Subscribers from 'containers/dashboards/Subscribers';
import useNewsletters from 'hooks/use-newsletters';
import useSubscribers from 'hooks/use-subscribers';
import useNewslettersInstance from 'hooks/use-newsletters-instance';
import LatestIssues from 'containers/dashboards/LatestIssues';

const DashboardAnalytics = ({ match }) => {
  const { formatMessage: f } = useIntl();
  const { subscribers } = useSubscribers();
  const { newsletters, totalNewsletters } = useNewsletters();
  const { instances, sentInstances, nextSendingInstances } = useNewslettersInstance();
  const totalSubscribers = subscribers ? subscribers.length : 0;
  const sentInstancesLen = sentInstances ? sentInstances.length : 0;
  const nextSendingInstancesLen = newsletters ? nextSendingInstances.length : 0;
  return (
    <>
      <Row>
        <Colxx xxs="12">
          <Breadcrumb heading="menu.analytics" match={match} />
          <Separator className="mb-5" />
        </Colxx>
      </Row>
      <Row>
        <Colxx lg="3" md="6" className="mb-4">
          <GradientWithRadialProgressCard
            icon="iconsminds-clock"
            title={`${totalSubscribers} ${f({ id: 'dashboards.subscribers' })}`}
            detail={f({ id: 'dashboards.pending-for-publish' })}
          />
        </Colxx>
        <Colxx lg="3" md="6" className="mb-4">
          <GradientWithRadialProgressCard
            icon="iconsminds-male"
            title={`${totalNewsletters} ${f({ id: 'dashboards.newsletters' })}`}
            detail={f({ id: 'dashboards.on-approval-process' })}
          />
        </Colxx>
        <Colxx lg="3" md="6" className="mb-4">
          <GradientWithRadialProgressCard
            icon="iconsminds-bell"
            title={`${sentInstancesLen} ${f({ id: 'dashboards.sent-this-week' })}`}
            detail={f({ id: 'dashboards.waiting-for-notice' })}
          />
        </Colxx>
        <Colxx lg="3" md="6" className="mb-4">
          <GradientWithRadialProgressCard
            icon="iconsminds-bell"
            title={`${nextSendingInstancesLen} ${f({ id: 'dashboards.next-sending' })}`}
            detail={f({ id: 'dashboards.waiting-for-notice' })}
          />
        </Colxx>
      </Row>
      <Row>
        <Colxx sm="12" md="8" className="mb-4">
          <div className="mb-4">
            <LatestIssues issues={instances} />
          </div>
          <div>
            <LatestArticles />
          </div>
        </Colxx>
        <Colxx sm="12" md="4" className="mb-4">
          <Subscribers subscribers={subscribers} />
        </Colxx>
      </Row>
    </>
  );
};

DashboardAnalytics.propTypes = { match: PropTypes.object };
export default DashboardAnalytics;
