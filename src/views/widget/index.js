import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route, Switch } from 'react-router-dom';

const Widget = React.lazy(() => import('./widget'));

const Designer = () => (
  <Suspense fallback={<div className="loading" />}>
    <Switch>
      <Route path="/widget" render={(props) => <Widget {...props} />} />
      <Redirect to="/error" />
    </Switch>
  </Suspense>
);

Designer.propTypes = {
  match: PropTypes.object
};
export default Designer;
