import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Card, CardBody, CardTitle } from 'reactstrap';

const Widget = () => {
  const { locale } = useSelector((state) => state.settings);
  useEffect(() => {
    const script = document.createElement('script');
    const params = JSON.stringify({ name: 'nw1', renderTo: '.newsletter-widget', categoryId: '16', lang: locale });
    script.src = '/assets/js/widget.js';
    script.id = 'newsletter-widget-script';
    script.setAttribute('data-params', params);
    document.body.appendChild(script);

    return () => {
      document.body.removeChild(script);
    };
  }, [locale]);
  return (
    <Card className="h-100">
      <CardBody style={{ width: 600, maxWidth: 600, margin: 'auto' }}>
        <CardTitle className="text-center">Synchronous</CardTitle>
        <p>This method uses a simple script tag reference as shown below:</p>
        <pre>
          <code>
            {`<div id="widget">Loading...</div>
<script
  src="./widget.js"
  id="newsletter-widget-script"
  data-params="{'name': 'nw1', 'renderTo': '#widget', 'categoryId': '16', lang: ${locale} }"
></script>
`}
          </code>
        </pre>
        <CardTitle className="text-center mt-4">Asynchronous</CardTitle>
        <p>This code follows the pattern used by Google Analytics.</p>
        <pre>
          <code>
            {`<div id="root">Loading...</div>
<script>
  (function (w, d, s, o, f, js, fjs) {
    w["newsletter-widget"] = o;
    w[o] =
      w[o] ||
      function () {
        (w[o].q = w[o].q || []).push(arguments);
      };
    (js = d.createElement(s)), (fjs = d.getElementsByTagName(s)[0]);
    js.id = o;
    js.src = f;
    js.async = 1;
    fjs.parentNode.insertBefore(js, fjs);
  })(window, document, "script", "cnw1", "./widget.js");
  cnw1("init", { renderTo: "#root", categoryId: "16", lang: ${locale} });
</script>
`}
          </code>
        </pre>

        <CardTitle className="text-center mt-5">Example</CardTitle>

        <div className="newsletter-widget mt-5">Loading Widget...</div>
      </CardBody>
    </Card>
  );
};

export default Widget;
