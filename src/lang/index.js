import locale_en from './locales/en.json';
import locale_fr from './locales/fr.json';

const AppLocale = {
  en: locale_en,
  fr: locale_fr
};

export default AppLocale;
