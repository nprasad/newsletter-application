import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { NavLink } from 'react-router-dom';
import { useIntl } from 'react-intl';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { Card, CardBody, CardTitle } from 'reactstrap';

const Subscribers = ({ subscribers }) => {
  const { formatMessage: f } = useIntl();

  return (
    <Card style={{ height: '100%' }}>
      <CardBody>
        <CardTitle>{f({ id: 'dashboards.subscribers' })}</CardTitle>
        <div className="dashboard-list-with-user">
          <PerfectScrollbar options={{ suppressScrollX: true, wheelPropagation: false }}>
            {!!subscribers.length &&
              subscribers.map(({ attributes: { title, field_email, created } }, index) => {
                return (
                  <div key={index} className="d-flex flex-row mb-3 pb-3 border-bottom">
                    <div className="pl-3 pr-2">
                      <NavLink to="/pages/product/details">
                        <p className="font-weight-medium mb-0 ">
                          {title} ({field_email})
                        </p>
                        <p className="text-muted mb-0 text-small">{moment(created).format('D MMM, YYYY')}</p>
                      </NavLink>
                    </div>
                  </div>
                );
              })}
          </PerfectScrollbar>
        </div>
      </CardBody>
    </Card>
  );
};
Subscribers.propTypes = { subscribers: PropTypes.array };
Subscribers.defaultProps = { subscribers: [] };
export default Subscribers;
