import React, { useEffect } from 'react';
import moment from 'moment';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { Card, CardBody, CardTitle } from 'reactstrap';
import { fetchArticles } from 'redux/actions';

import IntlMessages from 'helpers/IntlMessages';
import { useDispatch, useSelector } from 'react-redux';

const LatestArticles = () => {
  const dispatch = useDispatch();
  const { articles } = useSelector((state) => state.article);
  useEffect(() => {
    if (!articles) dispatch(fetchArticles(undefined, { total: 55 }));
  }, [articles, dispatch]);

  return (
    <div>
      <Card>
        <CardBody>
          <CardTitle>
            <IntlMessages id="dashboards.latest-articles" />
          </CardTitle>
          <div className="dashboard-logs">
            <PerfectScrollbar options={{ suppressScrollX: true, wheelPropagation: false }}>
              <table className="table table-sm table-borderless">
                <tbody>
                  {articles &&
                    !!articles.length &&
                    articles
                      .sort((a, b) => Date.parse(b.attributes.created) - Date.parse(a.attributes.created))
                      .map(({ attributes }, index) => {
                        return (
                          <tr key={index}>
                            <td>
                              <span className="font-weight-medium">{attributes.title}</span>
                            </td>
                            <td className="text-right">
                              <span className="text-muted">{moment(attributes.created).format('D MMM, YYYY')}</span>
                            </td>
                          </tr>
                        );
                      })}
                </tbody>
              </table>
            </PerfectScrollbar>
          </div>
        </CardBody>
      </Card>
    </div>
  );
};
export default LatestArticles;
