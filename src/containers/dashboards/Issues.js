/* eslint-disable react/no-array-index-key */
import React from 'react';
import { NavLink } from 'react-router-dom';
import { useIntl } from 'react-intl';
import moment from 'moment';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { Button, Card, CardBody, CardTitle } from 'reactstrap';

const Issues = ({ issues, height, className, newsletterId }) => {
  const { formatMessage: f } = useIntl();

  return (
    <Card className={className} style={{ height }}>
      <CardBody>
        <CardTitle>
          <span>{f({ id: 'pages.issues' })}</span>
          {!!newsletterId && (
            <NavLink to={`/newsletter/${newsletterId}/issue-add`}>
              <Button color="link" size="xs">
                (Add Issue)
              </Button>
            </NavLink>
          )}
        </CardTitle>
        <div className="dashboard-list-with-user">
          <PerfectScrollbar options={{ suppressScrollX: true, wheelPropagation: false }}>
            {issues && issues.length ? (
              issues.sort((a, b) => new Date(
                  b.attributes.created).getTime() - new Date(a.attributes.created).getTime()
              ).map(({ id, attributes: { title, created } }) => (
                <div key={id} className="d-flex flex-row mb-3 pb-3 border-bottom">
                  <div className="pl-3 pr-2">
                    <NavLink to={`/newsletter/issue-add/${id}`}>
                      <p className="font-weight-medium mb-0 ">{title}</p>
                      <p className="text-muted mb-0 text-small">{moment(created).format('D MMM, YYYY')}</p>
                    </NavLink>
                  </div>
                </div>
              ))
            ) : (
              <p className="mb-2">No Issue Found.</p>
            )}
          </PerfectScrollbar>
        </div>
      </CardBody>
    </Card>
  );
};
export default Issues;
