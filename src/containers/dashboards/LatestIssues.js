/* eslint-disable react/display-name */
import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { useIntl } from 'react-intl';
import { useTable, usePagination, useSortBy } from 'react-table';
import { Card, CardBody, CardTitle, Badge } from 'reactstrap';
import DatatablePagination from '../../components/common/DatatablePagination';
import moment from 'moment';

function Table({ columns, data }) {
  const sortBy = useMemo(() =>[{ id: '1', desc: true }], []);
  const {
    getTableProps,
    getTableBodyProps,
    prepareRow,
    headerGroups,
    page,
    canPreviousPage,
    canNextPage,
    pageCount,
    gotoPage,
    setPageSize,
    state: { pageIndex, pageSize }
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0, pageSize: 6, sortBy }
    },
    useSortBy,
    usePagination
  );



  return (
    <>
      <table {...getTableProps()} className="r-table table">
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column, columnIndex) => (
                <th
                  key={`th_${columnIndex}`}
                  {...column.getHeaderProps(column.getSortByToggleProps())}
                  className={column.isSorted ? (column.isSortedDesc ? 'sorted-desc' : 'sorted-asc') : ''}
                >
                  {column.render('Header')}
                  <span />
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {page.map((row) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map((cell, cellIndex) => (
                  <td
                    key={`td_${cellIndex}`}
                    {...cell.getCellProps({
                      className: cell.column.cellClass
                    })}
                  >
                    {cell.render('Cell')}
                  </td>
                ))}
              </tr>
            );
          })}
        </tbody>
      </table>

      <DatatablePagination
        page={pageIndex}
        pages={pageCount}
        canPrevious={canPreviousPage}
        canNext={canNextPage}
        pageSizeOptions={[4, 10, 20, 30, 40, 50]}
        showPageSizeOptions={false}
        showPageJump={false}
        defaultPageSize={pageSize}
        onPageChange={(p) => gotoPage(p)}
        onPageSizeChange={(s) => setPageSize(s)}
        paginationMaxSize={pageCount}
      />
    </>
  );
}
Table.propTypes = {
  columns: PropTypes.array,
  data: PropTypes.array
};

const LatestIssues = ({ issues }) => {
  const { formatMessage: f } = useIntl();

  const cols = useMemo(
    () => [
      {
        id: '1',
        Header: 'Date',
        accessor: 'attributes.field_schedule',
        cellClass: 'text-muted w-25',
        Cell: (props) => <>{moment(props.value).format('D MMM, YYYY')}</>,
        sortType: 'basic'
      },
      {
        id: '2',
        Header: 'Title',
        accessor: 'attributes.title',
        cellClass: 'text-muted w-50',
        Cell: (props) => <>{props.value}</>,
        sortType: 'basic'
      },
      {
        id: '3',
        Header: 'Status',
        accessor: 'attributes.field_processed',
        cellClass: 'text-muted w-25 ',
        Cell: (props) => {
          return (
            <Badge color={props.value ? 'primary' : 'secondary'} pill>
              {props.value ? 'PROCESSED' : 'ON HOLD'}
            </Badge>
          );
        },
        sortType: 'basic'
      }
    ],
    []
  );

  return (
    <Card className="h-100">
      <CardBody>
        <CardTitle>{f({ id: 'dashboards.latest-issues' })}</CardTitle>
        {!!issues.length && <Table columns={cols} data={issues} />}
      </CardBody>
    </Card>
  );
};

LatestIssues.propTypes = {
  issues: PropTypes.array
};
LatestIssues.defaultProps = { issues: [] };
export default LatestIssues;
