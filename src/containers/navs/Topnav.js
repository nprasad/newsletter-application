import React, { useState } from 'react';
import { injectIntl } from 'react-intl';

import { UncontrolledDropdown, DropdownItem, DropdownToggle, DropdownMenu } from 'reactstrap';

import { NavLink } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import { setContainerClassnames, clickOnMobileMenu, changeLocale, setKeycloakReset } from 'redux/actions';
import { keycloakInstance, keycloakUserName } from 'redux/keycloak/selectors';

import { searchPath, localeOptions, isDarkSwitchActive } from 'constants/defaultValues';

import { MobileMenuIcon, MenuIcon } from 'components/svg';
import TopnavEasyAccess from './Topnav.EasyAccess';
import TopnavDarkSwitch from './Topnav.DarkSwitch';

import { getDirection, setDirection } from 'helpers/Utils';

const TopNav = ({ history, showMenu }) => {
  const keycloak = useSelector(keycloakInstance);
  const userName = useSelector(keycloakUserName);
  const { containerClassnames, menuClickCount, selectedMenuHasSubItems } = useSelector((state) => state.menu);
  const { locale } = useSelector((state) => state.settings);
  const { loading } = useSelector((state) => state.global);

  const dispatch = useDispatch();

  const [isInFullScreen, setIsInFullScreen] = useState(false);
  const [searchKeyword, setSearchKeyword] = useState('');

  const search = () => {
    history.push(`${searchPath}?key=${searchKeyword}`);
    setSearchKeyword('');
  };

  const handleChangeLocale = (_locale, direction) => {
    dispatch(changeLocale(_locale));

    const currentDirection = getDirection().direction;
    if (direction !== currentDirection) {
      setDirection(direction);
      setTimeout(() => {
        window.location.reload();
      }, 500);
    }
  };

  const isInFullScreenFn = () => {
    return (
      (document.fullscreenElement && document.fullscreenElement !== null) ||
      (document.webkitFullscreenElement && document.webkitFullscreenElement !== null) ||
      (document.mozFullScreenElement && document.mozFullScreenElement !== null) ||
      (document.msFullscreenElement && document.msFullscreenElement !== null)
    );
  };

  const handleDocumentClickSearch = (e) => {
    let isSearchClick = false;
    if (e.target && e.target.classList && (e.target.classList.contains('navbar') || e.target.classList.contains('simple-icon-magnifier'))) {
      isSearchClick = true;
      if (e.target.classList.contains('simple-icon-magnifier')) {
        search();
      }
    } else if (e.target.parentElement && e.target.parentElement.classList && e.target.parentElement.classList.contains('search')) {
      isSearchClick = true;
    }

    if (!isSearchClick) {
      const input = document.querySelector('.mobile-view');
      if (input && input.classList) input.classList.remove('mobile-view');
      removeEventsSearch();
      setSearchKeyword('');
    }
  };

  const removeEventsSearch = () => {
    document.removeEventListener('click', handleDocumentClickSearch, true);
  };

  const toggleFullScreen = () => {
    const isFS = isInFullScreenFn();

    const docElm = document.documentElement;
    if (!isFS) {
      if (docElm.requestFullscreen) {
        docElm.requestFullscreen();
      } else if (docElm.mozRequestFullScreen) {
        docElm.mozRequestFullScreen();
      } else if (docElm.webkitRequestFullScreen) {
        docElm.webkitRequestFullScreen();
      } else if (docElm.msRequestFullscreen) {
        docElm.msRequestFullscreen();
      }
    } else if (document.exitFullscreen) {
      document.exitFullscreen();
    } else if (document.webkitExitFullscreen) {
      document.webkitExitFullscreen();
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else if (document.msExitFullscreen) {
      document.msExitFullscreen();
    }
    setIsInFullScreen(!isFS);
  };

  const handleLogout = () => {
    keycloak && keycloak.logout();
    dispatch(setKeycloakReset());
  };

  const menuButtonClick = (e, _clickCount, _conClassnames) => {
    e.preventDefault();

    setTimeout(() => {
      const event = document.createEvent('HTMLEvents');
      event.initEvent('resize', false, false);
      window.dispatchEvent(event);
    }, 350);
    dispatch(setContainerClassnames(_clickCount + 1, _conClassnames, selectedMenuHasSubItems));
  };

  const mobileMenuButtonClick = (e, _containerClassnames) => {
    e.preventDefault();
    dispatch(clickOnMobileMenu(_containerClassnames));
  };

  const showLoader = loading && Object.values(loading).some(Boolean);
  return (
    <nav className="navbar fixed-top">
      {showMenu && (
        <div className="d-flex align-items-center navbar-left">
          <NavLink
            to="#"
            location={{}}
            className="menu-button d-none d-md-block"
            onClick={(e) => menuButtonClick(e, menuClickCount, containerClassnames)}
          >
            <MenuIcon />
          </NavLink>
          <NavLink
            to="#"
            location={{}}
            className="menu-button-mobile d-xs-block d-sm-block d-md-none"
            onClick={(e) => mobileMenuButtonClick(e, containerClassnames)}
          >
            <MobileMenuIcon />
          </NavLink>

          <div className="d-inline-block">
            <UncontrolledDropdown className="ml-2">
              <DropdownToggle caret color="light" size="sm" className="language-button">
                <span className="name">{locale.toUpperCase()}</span>
              </DropdownToggle>
              <DropdownMenu className="mt-3" right>
                {localeOptions.map((l) => {
                  return (
                    <DropdownItem onClick={() => handleChangeLocale(l.id, l.direction)} key={l.id}>
                      {l.name}
                    </DropdownItem>
                  );
                })}
              </DropdownMenu>
            </UncontrolledDropdown>
          </div>
        </div>
      )}
      <a className="navbar-logo" href="/">
        CERN Newsletters service
      </a>

      {showMenu && (
        <div className="navbar-right">
          {showLoader && (
            <>
              <div className="loading-inline"></div>
              <div className="loading-overlay"></div>
            </>
          )}
          {isDarkSwitchActive && <TopnavDarkSwitch />}
          <div className="header-icons d-inline-block align-middle">
            <TopnavEasyAccess />
            <button
              className="header-icon btn btn-empty d-none d-sm-inline-block"
              type="button"
              id="fullScreenButton"
              onClick={toggleFullScreen}
            >
              {isInFullScreen ? <i className="simple-icon-size-actual d-block" /> : <i className="simple-icon-size-fullscreen d-block" />}
            </button>
          </div>
          <div className="user d-inline-block">
            <UncontrolledDropdown className="dropdown-menu-right">
              <DropdownToggle className="p-0" color="empty">
                <span className="name mr-1">{userName}</span>
              </DropdownToggle>
              <DropdownMenu className="mt-3" right>
                <DropdownItem onClick={handleLogout}>Sign out</DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </div>
        </div>
      )}
    </nav>
  );
};

export default injectIntl(TopNav);
