import React from 'react';
import { UncontrolledDropdown, DropdownToggle, DropdownMenu } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import IntlMessages from '../../helpers/IntlMessages';

const TopnavEasyAccess = () => {
  return (
    <div className="position-relative d-none d-sm-inline-block">
      <UncontrolledDropdown className="dropdown-menu-right">
        <DropdownToggle className="header-icon" color="empty">
          <i className="simple-icon-grid" />
        </DropdownToggle>
        <DropdownMenu className="position-absolute mt-3" right id="iconMenuDropdown">
          <NavLink to="/dashboards" className="icon-menu-item">
            <i className="iconsminds-shop-4 d-block" /> <IntlMessages id="menu.dashboards" />
          </NavLink>
          <NavLink to="/feeds" className="icon-menu-item">
            <i className="simple-icon-book-open d-block" /> <IntlMessages id="menu.feeds" />
          </NavLink>
          <NavLink to="/taxonomies" className="icon-menu-item">
            <i className="iconsminds-newspaper d-block" /> <IntlMessages id="menu.taxonomies" />
          </NavLink>
          <NavLink to="/newsletter" className="icon-menu-item">
            <i className="iconsminds-library d-block" /> <IntlMessages id="menu.newsletter" />
          </NavLink>
          <NavLink to="/subscribers" className="icon-menu-item">
            <i className="iconsminds-network d-block" /> <IntlMessages id="menu.subscribers" />
          </NavLink>
          <NavLink to="/designer" className="icon-menu-item">
            <i className="iconsminds-mail-photo d-block" /> <IntlMessages id="menu.email-designer" />
          </NavLink>
        </DropdownMenu>
      </UncontrolledDropdown>
    </div>
  );
};

export default TopnavEasyAccess;
