import React from 'react';
import PropTypes from 'prop-types';
import { useIntl } from 'react-intl';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { NavLink } from 'react-router-dom';

const BreadcrumbItems = ({ match }) => {
  const { formatMessage: f } = useIntl();
  const path = match.path.substr(1);
  let paths = path.split('/');
  if (paths[paths.length - 1].indexOf(':') > -1) {
    paths = paths.filter((x) => x.indexOf(':') === -1);
  }

  const getMenuTitle = (sub) => {
    return f({ id: `menu.${sub}` });
  };

  const getUrl = (path, sub, index) => {
    if (index === 0) {
      return '';
    }
    return path.split(sub)[0] + sub;
  };

  return (
    <>
      <Breadcrumb className="pt-0 breadcrumb-container d-none d-sm-block d-lg-inline-block">
        {paths.map((sub, index) => {
          return (
            <BreadcrumbItem key={index} active={paths.length === index + 1}>
              {paths.length !== index + 1 ? <NavLink to={`/${getUrl(path, sub, index)}`}>{getMenuTitle(sub)}</NavLink> : getMenuTitle(sub)}
            </BreadcrumbItem>
          );
        })}
      </Breadcrumb>
    </>
  );
};

BreadcrumbItems.propTypes = {
  match: PropTypes.object
};

const BreadcrumbContainer = ({ heading, match }) => {
  const { formatMessage: f } = useIntl();
  return (
    <>
      {heading && <h1>{f({ id: heading })}</h1>}
      <BreadcrumbItems match={match} />
    </>
  );
};

BreadcrumbContainer.propTypes = {
  heading: PropTypes.string,
  match: PropTypes.object
};

export default BreadcrumbContainer;
