import React from 'react';
import PropTypes from 'prop-types';
import Image from 'components/emails/Image';
import Link from 'components/emails/Link';
import Table from 'components/emails/Table';

const styles = {
  wrapper: {
    backgroundColor: '#1EA176',
    padding: 10,
    marginBottom: 20,
    boxShadow: '0 0 5px rgba(0, 0, 0, 0.15)',
    textAlign: 'right'
  },
  socialLink: { textDecoration: 'none', color: 'black' },
  socialImg: {
    marginLeft: 10,
    marginRight: 10,
    maxWidth: 30,
    maxHeight: 30,
    minWidth: 10,
    minHeight: 10,
    width: 30,
    height: 30
  }
};

function SocialWidget({
  globalStyles: {
    global: { backgroundColor }
  },
  facebook,
  twitter,
  youtube,
  instagram,
  linkedin
}) {
  return (
    <Table width="100%" style={{ ...styles.wrapper, backgroundColor }}>
      <Table.Row>
        {/* <Table.Cell align="right" width="99%" style={{ textAlign: 'right', paddingTop: 10, paddingBottom: 10 }}></Table.Cell> */}
        {facebook && (
          <Table.Cell align="right" width="1%" style={{ textAlign: 'right', paddingTop: 10, paddingBottom: 10 }}>
            <Link href={facebook} style={styles.socialLink}>
              <Image src={process.env.PUBLIC_URL + '/assets/img/facebook.png'} alt="" style={styles.socialImg} />
            </Link>
          </Table.Cell>
        )}
        {twitter && (
          <Table.Cell align="right" width="1%" style={{ textAlign: 'right', paddingTop: 10, paddingBottom: 10 }}>
            <Link href={twitter} style={styles.socialLink}>
              <Image src={process.env.PUBLIC_URL + '/assets/img/twitter.png'} alt="" style={styles.socialImg} />
            </Link>
          </Table.Cell>
        )}
        {youtube && (
          <Table.Cell align="right" width="1%" style={{ textAlign: 'right', paddingTop: 10, paddingBottom: 10 }}>
            <Link href={youtube} style={styles.socialLink}>
              <Image src={process.env.PUBLIC_URL + '/assets/img/youtube.png'} alt="" style={styles.socialImg} />
            </Link>
          </Table.Cell>
        )}
        {instagram && (
          <Table.Cell align="right" width="1%" style={{ textAlign: 'right', paddingTop: 10, paddingBottom: 10 }}>
            <Link href={instagram} style={styles.socialLink}>
              <Image src={process.env.PUBLIC_URL + '/assets/img/instagram.png'} alt="" style={styles.socialImg} />
            </Link>
          </Table.Cell>
        )}
        {linkedin && (
          <Table.Cell align="right" width="1%" style={{ textAlign: 'right', paddingTop: 10, paddingBottom: 10 }}>
            <Link href={linkedin} style={styles.socialLink}>
              <Image src={process.env.PUBLIC_URL + '/assets/img/linkedin.png'} alt="" style={styles.socialImg} />
            </Link>
          </Table.Cell>
        )}
      </Table.Row>
    </Table>
  );
}

SocialWidget.propTypes = {
  globalStyles: PropTypes.object,
  facebook: PropTypes.string,
  twitter: PropTypes.string,
  youtube: PropTypes.string,
  instagram: PropTypes.string,
  linkedin: PropTypes.string
};
SocialWidget.defaultProps = {
  globalStyles: { global: { backgroundColor: '#198763', color: '#198763' } },
  facebook: 'https://www.facebook.com/cern',
  twitter: 'http://twitter.com/cern',
  youtube: 'https://www.youtube.com/cern',
  instagram: 'https://www.instagram.com/cern/',
  linkedin: 'https://www.linkedin.com/company/cern'
};
export default SocialWidget;
