import React from 'react';
import PropTypes from 'prop-types';
import EmailArticle from './EmailArticle';
import TitleWidget from './TitleWidget';

function EmailArticlesWidget({ title, articles, globalStyles, lang }) {
  return (
    <>
      <TitleWidget title={title} globalStyles={globalStyles} />
      {articles.map((article, index) => (
        <EmailArticle key={`article-${index}`} {...article} globalStyles={globalStyles} lang={lang}/>
      ))}
    </>
  );
}

EmailArticlesWidget.propTypes = {
  lang: PropTypes.string,
  title: PropTypes.string,
  articles: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      featuredImage: PropTypes.string,
      entryImage: PropTypes.string,
      excerpt: PropTypes.string,
      link: PropTypes.string,
      isFeatured: PropTypes.bool,
      withImage: PropTypes.bool
    })
  ),
  globalStyles: PropTypes.object
};
EmailArticlesWidget.defaultProps = {
  lang: 'en',
  title: 'Widget Title',
  articles: [
    {
      title: 'LS2 Report: The PS Booster restarts',
      featuredImage: 'https://cds.cern.ch/images/CERN-PHOTO-202006-091-4/file?size=large',
      entryImage: 'https://cds.cern.ch/images/CERN-HOMEWEB-PHO-2020-073-1/file?size=large',
      excerpt: 'It’s the end of Long Shutdown 2 for the PS Booster, the first accelerator to be recommissioned, alongside Linac 4',
      link:
        'https://home.cern/news/news/accelerators/ls2-report-ps-booster-restarts?utm_source=Bulletin&utm_medium=Email&utm_content=2020-07-08E&utm_campaign=BulletinEmail',
      isFeatured: false,
      withImage: false
    }
  ]
};

export default EmailArticlesWidget;
