import Image from 'components/emails/Image';
import Link from 'components/emails/Link';
import Span from 'components/emails/Span';
import Table from 'components/emails/Table';
import PropTypes from 'prop-types';
import React from 'react';
import TitleWidget from './TitleWidget';

const styles = {
  table: {
    width: '100%',
    marginTop: '20px',
    marginBottom: '20px'
  },
  description: { position: 'relative', marginBottom: '10px', color: 'dimgray' },
  entryImage: { objectFit: 'cover', objectPosition: 'center', height: '150px', width: '150px' }
};

function CDSWidget(props) {
  const { title, description, entries, droppableKey, globalStyles } = props;
  const isSidebar = droppableKey && droppableKey === 'sidebar';

  const createItemList = () => {
    let rows = {};
    let counter = 1;
    entries.forEach((image, index) => {
      rows[counter] = rows[counter] ? [...rows[counter]] : [];
      if (index % 6 === 0 && index !== 0) {
        counter++;
        rows[counter] = rows[counter] ? [...rows[counter]] : [];
        rows[counter].push(image);
      } else {
        rows[counter].push(image);
      }
    });
    return rows;
  };

  const rows = createItemList();

  const renderRows = () =>
    Object.keys(rows).map((row, idx) => (
      <Table.Row key={idx}>
        {rows[row].map(({ id, links, thumbnail }, index) => (
          <Table.Cell key={`${id}-${index}`}>
            <Link href={links.self}>
              <Image src={thumbnail ? thumbnail : links.small} alt="" style={styles.entryImage} />
            </Link>
          </Table.Cell>
        ))}
      </Table.Row>
    ));

  const renderSimple = () =>
    entries.map(({ id, links, thumbnail }, index) => (
      <Table.Row key={`${id}-${index}`}>
        <Table.Cell>
          <Link href={links.self}>
            <Image src={thumbnail ? thumbnail : links.small} alt="" style={{ ...styles.entryImage, width: '100%', height: 'auto' }} />
          </Link>
        </Table.Cell>
      </Table.Row>
    ));

  const content = isSidebar ? renderSimple() : renderRows();

  return (
    <>
      <TitleWidget title={title} globalStyles={globalStyles} />
      <Table width="100%">
        <Table.Row>
          <Table.Cell valign="top" width="100%">
            <Span style={styles.description}>{description}</Span>
          </Table.Cell>
        </Table.Row>
      </Table>
      <Table style={styles.table}>{content}</Table>
    </>
  );
}

CDSWidget.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  entries: PropTypes.array,
  droppableKey: PropTypes.string,
  globalStyles: PropTypes.object
};
CDSWidget.defaultProps = {
  title: '',
  description: '',
  entries: [],
  globalStyles: { global: { backgroundColor: '#198763', color: '#198763' } }
};

export default CDSWidget;
