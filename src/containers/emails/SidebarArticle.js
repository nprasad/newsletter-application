import React from 'react';
import PropTypes from 'prop-types';

import Table from 'components/emails/Table';
import Link from 'components/emails/Link';

const styles = {
  wrapper: {
    marginTop: 20,
    marginBottom: 20,
    boxShadow: '0 0 5px rgba(0, 0, 0, 0.15)',
    border: '1px solid #1EA176'
  },
  link: {
    textDecoration: 'none',
    color: 'white',
    padding: '5px',
    backgroundColor: '#198763',
    fontStyle: 'italic',
    position: 'absolute',
    bottom: 0,
    right: 0
  }
};
const translations = {
  en: { btnlabel: 'more'},
  fr: { btnlabel: 'plus' }
};
const contentPlaceholder = `<h4>Title</h4> <p>Paragraph</p> <p>Another Paragraph...</p>`;

function SidebarArticle({ title, content, article, globalStyles, lang }) {
  const {
    global: { backgroundColor },
    widgetTitle
  } = globalStyles;
  const locale = translations[lang];
  return (
    <>
      <Table width="100%" style={{ ...styles.wrapper, border: `1px solid ${backgroundColor}` }}>
        <Table.Row>
          <Table.Cell width="100%" style={{ textAlign: 'center', backgroundColor, borderBottom: `1px solid ${backgroundColor}` }}>
            <h1
              style={{
                fontSize: '20px',
                color: 'white',
                marginTop: '0px',
                marginBottom: '0px',
                marginLeft: '10px',
                marginRight: '10px',
                padding: '10px 0 10px 0',
                textTransform: 'uppercase',
                ...widgetTitle
              }}
            >
              {title}
            </h1>
          </Table.Cell>
        </Table.Row>

        <Table.Row>
          <Table.Cell style={{ padding: 10 }}>
            <div dangerouslySetInnerHTML={{ __html: content }}></div>

            <Table width="100%" style={{ position: 'relative', marginTop: 40 }}>
              <Table.Row>
                <Table.Cell align="right">
                  <Link href={article?.attributes?.field_url} style={{ ...styles.link, backgroundColor }}>
                    {locale.btnlabel} &gt;
                  </Link>
                </Table.Cell>
              </Table.Row>
            </Table>
          </Table.Cell>
        </Table.Row>
      </Table>
    </>
  );
}

SidebarArticle.propTypes = {
  lang: PropTypes.string,
  title: PropTypes.string,
  content: PropTypes.string,
  article: PropTypes.object,
  globalStyles: PropTypes.object
};
SidebarArticle.defaultProps = {
  lang: 'en',
  title: 'A WORD FROM...',
  content: contentPlaceholder,
  article: { attributes: { field_url: '' } }
};

export default SidebarArticle;
