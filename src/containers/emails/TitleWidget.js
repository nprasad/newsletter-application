import React from 'react';
import PropTypes from 'prop-types';
import Table from 'components/emails/Table';

const styles = {
  table: {
    marginBottom: 10,
    backgroundColor: '#1EA176',
    color: '#fff',
    boxShadow: '0 0 5px rgba(0, 0, 0, 0.15)'
  },
  title: { margin: 0, padding: 10 }
};

function TitleWidget(props) {
  const {
    title,
    globalStyles: { global, widgetTitle }
  } = props;
  return (
    <Table width="100%" style={{ ...styles.table, backgroundColor: global.backgroundColor }}>
      <Table.Row>
        <Table.Cell>
          <h2 style={{ ...styles.title, ...widgetTitle }}> {title} </h2>
        </Table.Cell>
      </Table.Row>
    </Table>
  );
}

TitleWidget.propTypes = { title: PropTypes.string, globalStyles: PropTypes.object };
TitleWidget.defaultProps = { title: '', globalStyles: { global: { backgroundColor: '#198763' } } };

export default TitleWidget;
