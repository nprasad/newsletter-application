import React from 'react';
import PropTypes from 'prop-types';
import Span from 'components/emails/Span';
import Image from 'components/emails/Image';
import Link from 'components/emails/Link';
import Table from 'components/emails/Table';
import { getStorage } from '../../helpers/Localstorage';
import { LOCALE_STORAGE_KEYS } from '../../constants/emails';
import { formatDate } from '../../helpers/Utils';

const styles = {
  table: {
    fontSize: '14px',
    lineHeight: 1,
    paddingLeft: '20px',
    paddingRight: '20px',
    overflow: 'hidden',
    height: '40px'
  },
  tableMenu: {
    fontSize: '14px',
    lineHeight: 1,
    paddingLeft: '5px',
    paddingRight: '5px',
    height: '40px',
    tableLayout: 'auto',
    borderCollapse: 'separate'
  },
  link: {
    textDecoration: 'none',
    color: '#ccc',
    marginTop: '6px',
    padding: '6px',
    MozBorderRadius: '5px',
    WebkitBorderRadius: '5px',
    borderRadius: '5px',
    display: 'inline-block',
    verticalAlign: 'middle'
  }
};
const translations = {
  en: { left_title: 'Accelerating science', right_title: 'Directory', printable: 'Printable', version: ' version' },
  fr: { left_title: 'Accélérateur de science', right_title: 'Liens utiles', printable: 'Version', version: ' imprimable' }
};

function EmailHeader(props) {
  const { bannerImage, date, issue, links, printableLink, cernLink } = props;
  const { header: headerStyles, headerTitle: titleStyles, headerLinks: linksStyles } = props.globalStyles;
  const lang = getStorage(LOCALE_STORAGE_KEYS.LANG) || 'en';
  const locale = translations[lang];
  return (
    <>
      <Table cellSpacing={20} width="100%" style={{ ...headerStyles, ...styles.table }}>
        <Table.Row>
          <Table.Cell align="left" valign="top" style={{ textAlign: 'left', paddingLeft: 15 }}>
            <Link href={cernLink} style={{ ...styles.link, ...linksStyles }}>
              <Span style={{ ...titleStyles }}>CERN</Span> {locale.left_title}
            </Link>
          </Table.Cell>
          <Table.Cell align="right" valign="top" style={{ paddingRight: 15 }}>
            <Link id="directory-link" href="http://directory.web.cern.ch/directory/" style={{ ...styles.link, ...linksStyles }}>
              {locale.right_title}
            </Link>
          </Table.Cell>
        </Table.Row>
      </Table>

      <Table width="100%" style={{ marginBottom: '-5px' }} cellSpacing={0} cellPadding={0}>
        <Table.Row>
          <Table.Cell>
            <Link href="http://home.cern/cern-people">
              <Image src={bannerImage} alt="" style={{ zIndex: -1, width: '100%' }} />
            </Link>
          </Table.Cell>
        </Table.Row>
      </Table>

      <Table id="table-menu" width="100%" style={{ ...styles.tableMenu, ...headerStyles }}>
        <Table.Row>
          <Table.Cell id="header-menu">
            <Table id="header-menu-table">
              <Table.Row>
                <Table.Cell></Table.Cell>
                {links.map(({ title, href }, index) => (
                  <Table.Cell
                    key={title + '-' + index}
                    style={{ marginTop: '5px', width: 'auto', display: 'inline-block', whiteSpace: 'nowrap' }}
                  >
                    <Link href={href} style={{ ...styles.link, ...linksStyles }}>
                      {title}
                    </Link>
                  </Table.Cell>
                ))}
              </Table.Row>
            </Table>
          </Table.Cell>
          <Table.Cell width="1%"></Table.Cell>
          <Table.Cell align="right">
            <Table id="printable-table" width="100%">
              <Table.Row>
                <Table.Cell style={{ display: 'inline-block', whiteSpace: 'nowrap', width: 'auto' }}>
                  <Span style={{ textDecoration: 'none', ...titleStyles }}>
                    <Link href={printableLink} style={{ textDecoration: 'none', ...linksStyles }}>
                      {issue}
                      <br />
                      {formatDate(date, lang)}
                      <br />
                    </Link>
                  </Span>
                </Table.Cell>
                {printableLink && (
                  <Table.Cell id="printable" style={{ display: 'inline-block', whiteSpace: 'nowrap' }}>
                    <Table align="right">
                      <Table.Row>
                        <Table.Cell
                          align="right"
                          style={{ width: 'auto', display: 'inline-block', whiteSpace: 'nowrap', textAlign: 'right' }}
                        >
                          <Link href={printableLink} style={{ textDecoration: 'none', ...titleStyles }}>
                            <Image
                              src={process.env.PUBLIC_URL + '/assets/img/print.png'}
                              style={{ width: '32px', height: '32px', maxWidth: '32px', maxHeight: '32px', margin: '8px 4px 8px 10px' }}
                              alt=""
                            />
                          </Link>
                        </Table.Cell>
                        <Table.Cell align="right" style={{ display: 'inline-block', whiteSpace: 'nowrap' }}>
                          <Link href={printableLink} style={{ textDecoration: 'none', ...linksStyles }}>
                            {locale.printable}
                            <br />
                            {locale.version}
                          </Link>
                        </Table.Cell>
                      </Table.Row>
                    </Table>
                  </Table.Cell>
                )}
              </Table.Row>
            </Table>
          </Table.Cell>
        </Table.Row>
      </Table>
    </>
  );
}

EmailHeader.propTypes = {
  cernLink: PropTypes.string,
  bannerImage: PropTypes.string,
  date: PropTypes.string,
  issue: PropTypes.string,
  links: PropTypes.array,
  printableLink: PropTypes.string,
  globalStyles: PropTypes.object
};

EmailHeader.defaultProps = {
  cernLink: 'http://home.cern',
  bannerImage: '',
  date: '',
  issue: '',
  links: [],
  printableLink: '',
  globalStyles: { header: { backgroundColor: '#1e1e1e', color: '#999' }, title: { color: '#fff' }, links: { color: '#ccc' } }
};

export default EmailHeader;
