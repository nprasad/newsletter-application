import React from 'react';
import PropTypes from 'prop-types';
import Table from 'components/emails/Table';
import Link from 'components/emails/Link';
import Image from 'components/emails/Image';
import { getStorage } from '../../helpers/Localstorage';
import moment from 'moment';
import { LOCALE_STORAGE_KEYS, NEWSLETTER } from '../../constants/emails';

const styles = {
  table: {
    width: '100%',
    backgroundColor: '#eeeeee',
    color: '#696969',
    height: '170px',
    marginTop: '20px'
  },
  link: {
    textDecoration: 'none',
    color: '#198763'
  }
};

const translations = {
  en: {
    privacyPolicy: {
      href: 'https://home.cern/data-privacy-protection-policy',
      title: 'Privacy Policy',
    },
    unsubscribe: {
      href: 'https://cnpdfs.web.cern.ch/subscriptions',
      title: 'Unsubscribe',
    },
  },
  fr: {
    privacyPolicy: {
      href: 'https://home.cern/data-privacy-protection-policy',
      title: 'Protection des données personnelles',
    },
    unsubscribe: {
      href: 'https://cnpdfs.web.cern.ch/subscriptions',
      title: 'Se désabonner',
    },
  }
};

const EmailFooter = ({ links, globalStyles: { footer, footerLinks }, newsLetterType }) => {
  const lang = getStorage(LOCALE_STORAGE_KEYS.LANG) || 'en';
  moment.locale(lang);
  const locale = translations[lang];
  return (
    <Table style={{ ...styles.table, ...footer }}>
      <Table.Row>
        <Table.Cell style={{ margin: '20px 0 20px 0', paddingLeft: '20px', width: '33.33333333%' }}>
          <ul style={{ listStyleType: 'none', padding: 0, margin: 0 }}>
            <li>
              <Link href="http://copyright.web.cern.ch/" style={{ ...styles.link, ...footerLinks, fontSize: footer.fontSize }}>
                Copyright © {new Date().getFullYear()} CERN
              </Link>
            </li>
            <li>
              <Link href={locale.privacyPolicy.href} style={{ ...styles.link, ...footerLinks, fontSize: footer.fontSize }}>
                {locale.privacyPolicy.title}
              </Link>
            </li>
            {newsLetterType === NEWSLETTER.TYPE_2 && (
              <li>
                <Link href={locale.unsubscribe.href} style={{ ...styles.link, ...footerLinks, fontSize: footer.fontSize }}>
                  {locale.unsubscribe.title}
                </Link>
              </li>
            )}
          </ul>
        </Table.Cell>
        <Table.Cell style={{ margin: '20px 0 20px 0', width: '33.33333333%' }}>
          <ul style={{ listStyleType: 'none', padding: 0, margin: 0 }}>
            {links.map((link, index) => (
              <li key={link.title + '-' + index}>
                <Link href={link.href} style={{ ...styles.link, ...footerLinks }}>
                  {link.title}
                </Link>
              </li>
            ))}
          </ul>
        </Table.Cell>
        <Table.Cell align="right" style={{ margin: '20px 0 20px 0', paddingRight: '15px' }}>
          <Link href="http://home.cern/" style={{ ...styles.link, ...footerLinks }}>
            <Image style={{ height: '120px', width: '120px' }} src={process.env.PUBLIC_URL + '/assets/img/cern-logo-large.png'} />
          </Link>
        </Table.Cell>
      </Table.Row>
    </Table>
  );
};
EmailFooter.propTypes = {
  links: PropTypes.array,
  globalStyles: PropTypes.object,
  newsLetterType: PropTypes.string,
};
EmailFooter.defaultProps = {
  globalStyles: { footer: { backgroundColor: '#eeeeee', color: '#696969' }, footerLinks: { color: '#198763' } }
};

export default EmailFooter;
