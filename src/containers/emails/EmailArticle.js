import React from 'react';
import PropTypes from 'prop-types';
// import Email from 'components/emails/Email';
import Span from 'components/emails/Span';
import Image from 'components/emails/Image';
import Link from 'components/emails/Link';
import Table from 'components/emails/Table';
// import EditableUi from 'components/designer/EditableUi';

const styles = {
  container: {
    marginTop: 20,
    position: 'relative',
    zIndex: 0,
    paddingLeft: 0,
    maxWidth: '665px',
    fontFamily: "'Source Sans Pro', sans-serif}"
  },
  title: {
    marginTop: 0,
    color: '#198763',
    fontSize: '19px',
    textAlign: 'left',
    textTransform: 'uppercase',
    marginBottom: '10px',
    textDecoration: 'none'
  },

  featuredImage: { objectFit: 'cover', objectPosition: 'center', height: '350px', width: '100%' },
  entryImage: { objectFit: 'cover', objectPosition: 'center', height: '200px', width: '100%' },
  excerpt: { position: 'relative', marginBottom: '10px', color: 'dimgray' },
  link: {
    textDecoration: 'none',
    color: 'white',
    padding: '5px',
    backgroundColor: '#198763',
    fontStyle: 'italic',
    position: 'absolute',
    bottom: 0,
    right: 0
  },
  divider: {
    opacity: '0.5',
    display: 'block',
    color: '#ccc',
    height: '1px',
    background: '-webkit-gradient(radial, 50% 50%, 0, 50% 50%, 350, from(#1ea176), to(#fff))',
    border: 0,
    marginTop: '30px',
    marginBottom: '30px'
  }
};
const translations = {
  en: { btnlabel: 'more '},
  fr: { btnlabel: 'plus' }
};

function MainTitle({ title, isFeatured, link, globalStyles: { global, articleTitle } }) {
  return (
    <Table cellSpacing={20} width="100%" style={{ marginTop: 20 }}>
      <Table.Row>
        <Table.Cell align="left" valign="top" style={{ textAlign: 'left' }}>
          <Link href={link} style={{ textDecoration: 'none', color: global.color }}>
            <h2 style={{ ...styles.title, fontSize: isFeatured ? '26px' : '19px', color: global.color, ...articleTitle }}>{title}</h2>
          </Link>
        </Table.Cell>
      </Table.Row>
    </Table>
  );
}
MainTitle.propTypes = { title: PropTypes.string, isFeatured: PropTypes.bool, link: PropTypes.string, globalStyles: PropTypes.object };

function FeaturedImage({ featuredImage, heightAuto }) {
  return (
    <Table width="100%">
      <Table.Row>
        <Table.Cell valign="top" width="100%">
          <Image src={featuredImage} alt="" style={{ ...styles.featuredImage, height: heightAuto ? 'auto' : '350px' }} />
        </Table.Cell>
      </Table.Row>
    </Table>
  );
}
FeaturedImage.propTypes = { featuredImage: PropTypes.string, heightAuto: PropTypes.bool };

function Excerpt({ excerpt, globalStyles: { excerptTitle } }) {
  return (
    <Table width="100%">
      <Table.Row>
        <Table.Cell valign="top" width="100%">
          <Span style={{ ...styles.excerpt, ...excerptTitle }}>{excerpt}</Span>
        </Table.Cell>
      </Table.Row>
    </Table>
  );
}
Excerpt.propTypes = { excerpt: PropTypes.string, globalStyles: PropTypes.object };

function MoreBtn({ link, globalStyles: { global }, fullWidth, lang }) {
  const locale = translations[lang];
  const align = fullWidth ? 'center' : 'right';
  const extraStyles = {
    width: fullWidth ? '100%' : 'auto',
    padding: fullWidth ? '10px' : '5px',
    fontSize: fullWidth && '15px',
    right: fullWidth ? '5px' : '0px',
    left: fullWidth && '5px'
  };

  return (
    <Table width="100%" style={{ position: 'relative', marginTop: fullWidth ? '50px' : '30px' }}>
      <Table.Row>
        <Table.Cell align={align} style={{ textAlign: align }}>
          <Link href={link} style={{ ...styles.link, ...extraStyles, backgroundColor: global.backgroundColor }}>
            {locale.btnlabel} &gt;
          </Link>
        </Table.Cell>
      </Table.Row>
    </Table>
  );
}
MoreBtn.propTypes = { lang: PropTypes.string, link: PropTypes.string, globalStyles: PropTypes.object, fullWidth: PropTypes.bool };

function EmailArticle({ isSidebar, title, featuredImage, entryImage, excerpt, link, isFeatured, withImage, globalStyles, lang }) {
  return withImage ? (
    <>
      <Table width="100%">
        <Table.Row>
          <Table.Cell valign="top" style={{ width: '41.666666667%' }}>
            <Image src={entryImage} alt="" style={styles.entryImage} />
          </Table.Cell>
          <Table.Cell valign="top" style={{ paddingLeft: 15 }}>
            <MainTitle title={title} isFeatured={isFeatured} link={link} globalStyles={globalStyles} />
            {isFeatured && <FeaturedImage featuredImage={featuredImage} heightAuto={isSidebar} />}
            <Excerpt excerpt={excerpt} globalStyles={globalStyles} />
            <MoreBtn link={link} globalStyles={globalStyles} fullWidth={isSidebar} lang={lang} />
          </Table.Cell>
        </Table.Row>
      </Table>

      <hr
        style={{
          ...styles.divider,
          background: `-webkit-gradient(radial, 50% 50%, 0, 50% 50%, 350, from(${global.backgroundColor}), to(#fff))`
        }}
      />
    </>
  ) : (
    <>
      <MainTitle title={title} isFeatured={isFeatured} link={link} globalStyles={globalStyles} />
      {isFeatured && <FeaturedImage featuredImage={featuredImage} heightAuto={isSidebar} />}
      <Excerpt excerpt={excerpt} globalStyles={globalStyles} />
      <MoreBtn link={link} globalStyles={globalStyles} fullWidth={isSidebar} lang={lang} />
      <hr
        style={{
          ...styles.divider,
          background: `-webkit-gradient(radial, 50% 50%, 0, 50% 50%, 350, from(${global.backgroundColor}), to(#fff))`
        }}
      />
    </>
  );
}

EmailArticle.propTypes = {
  lang: PropTypes.string,
  title: PropTypes.string,
  featuredImage: PropTypes.string,
  entryImage: PropTypes.string,
  excerpt: PropTypes.string,
  link: PropTypes.string,
  isFeatured: PropTypes.bool,
  withImage: PropTypes.bool,
  globalStyles: PropTypes.object,
  isSidebar: PropTypes.bool
};
EmailArticle.defaultProps = {
  lang: 'en',
  title: 'LS2 Report: The PS Booster restarts',
  featuredImage: 'https://cds.cern.ch/images/CERN-PHOTO-202006-091-4/file?size=large',
  entryImage: 'https://cds.cern.ch/images/CERN-HOMEWEB-PHO-2020-073-1/file?size=large',
  excerpt: 'It’s the end of Long Shutdown 2 for the PS Booster, the first accelerator to be recommissioned, alongside Linac 4',
  link:
    'https://home.cern/news/news/accelerators/ls2-report-ps-booster-restarts?utm_source=Bulletin&utm_medium=Email&utm_content=2020-07-08E&utm_campaign=BulletinEmail',
  isFeatured: false,
  withImage: false,
  globalStyles: { global: { backgroundColor: '#198763', color: '#198763' } }
};

export default EmailArticle;
