import React from 'react';
import PropTypes from 'prop-types';
import Link from 'components/emails/Link';
import Table from 'components/emails/Table';

const styles = {
  wrapper: {
    marginTop: 20,
    boxShadow: '0 0 5px rgba(0, 0, 0, 0.15)',
    border: '1px solid #1EA176'
  }
};

function ArticlesLinksWidget(props) {
  const {
    title,
    articles,
    articlesList,
    globalStyles: {
      global: { backgroundColor },
      widgetTitle,
      widgetLinks
    }
  } = props;
  const _articles = articlesList.filter((article) => articles.includes(article.id));

  return (
    <Table width="100%" style={{ ...styles.wrapper, border: `1px solid ${backgroundColor}` }}>
      <Table.Row>
        <Table.Cell width="100%" style={{ textAlign: 'center', backgroundColor, borderBottom: `1px solid ${backgroundColor}` }}>
          <h1
            style={{
              fontSize: '20px',
              color: 'white',
              marginTop: '0px',
              marginBottom: '0px',
              marginLeft: '10px',
              marginRight: '10px',
              padding: '10px 0 10px 0',
              textTransform: 'uppercase',
              ...widgetTitle
            }}
          >
            {title}
          </h1>
        </Table.Cell>
      </Table.Row>

      <Table.Row>
        <Table.Cell style={{ paddingTop: 10, paddingBottom: 10 }}>
          {_articles.map(({ attributes: { title, field_url } }, index) => (
            <Link key={index} href={field_url} style={{ textDecoration: 'none', color: 'black' }}>
              <h2 style={{ marginLeft: 10, fontSize: 15, marginRight: 10, ...widgetLinks }}> {title}&nbsp;&nbsp; &gt; </h2>
            </Link>
          ))}
        </Table.Cell>
      </Table.Row>
    </Table>
  );
}
ArticlesLinksWidget.propTypes = {
  title: PropTypes.string,
  articles: PropTypes.array,
  articlesList: PropTypes.array,
  links: PropTypes.array,
  globalStyles: PropTypes.object
};
ArticlesLinksWidget.defaultProps = {
  title: '',
  articles: [],
  articlesList: [],
  globalStyles: { global: { backgroundColor: '#198763', color: '#198763' } }
};
export default ArticlesLinksWidget;
