/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Button, CustomInput, Input, Label } from 'reactstrap';
import Select from 'react-select';
import CustomSelectInput from 'components/common/CustomSelectInput';
import { useDispatch, useSelector } from 'react-redux';
import { postNewsletter, editNewsletter } from 'redux/actions';
import { NavLink } from 'react-router-dom';
import useTaxonomies from 'hooks/use-taxonomies';
import useFeeds from 'hooks/use-feeds';

const INITIAL_FORM = {
  title: '',
  feeds: [],
  categories: [],
  topics: [],
  type: '',
  interval: '',
  rationale: '',
  adminGroup: 'newsletter-service-',
  preferredLang: 'en',
  archive: false
};

const AddNewsletter = ({ selectedNewsletter, mode }) => {
  const dispatch = useDispatch();
  const { feeds } = useFeeds();
  const { newNewsletter } = useSelector((state) => state.newsletter);
  const { categoryOptions, topicOptions, typeOptions } = useTaxonomies();
  const [form, setForm] = useState(INITIAL_FORM);
  const [clickSave, setClickSave] = useState(false);
  const handleChange = (event) => {
    const { name, value } = event.target;
    setForm({ ...form, [name]: value });
  };
  const handleChangeCheckbox = (event) => {
    const { name } = event.target;
    setForm({ ...form, [name]: !form[name] });
  };
  const handleChangeLang = ({ value }) => setForm({ ...form, preferredLang: value });
  const handleChangeSelect = (values, { name }) =>
    ['type', 'adminGroup'].includes(name)
      ? setForm({ ...form, [name]: values.value })
      : setForm({ ...form, [name]: values ? values.map(({ value }) => value) : [] });

  const isEditing = mode === 'edit';

  useEffect(() => {
    if (selectedNewsletter) {
      const {
        attributes: { title, field_interval, field_rationale, field_admin_group, field_archive },
        relationships: { field_categories, field_feed, field_topics, field_type }
      } = selectedNewsletter;
      setForm({
        ...form,
        title,
        feeds: field_feed.data.map((item) => item.id),
        categories: field_categories.data.map((item) => item.id),
        topics: field_topics.data.map((item) => item.id),
        type: field_type.data && field_type.data.id,
        interval: field_interval,
        rationale: field_rationale ? field_rationale.value : '',
        adminGroup: field_admin_group,
        archive: field_archive
      });
    }
  }, [selectedNewsletter]);

  const createNewsletterData = () => {
    return {
      type: 'node--newsletter',
      attributes: {
        title: form.title,
        field_interval: +form.interval,
        field_rationale: form.rationale,
        field_admin_group: form.adminGroup,
        field_archive: form.archive
      },
      relationships: {
        field_categories: { data: form.categories.map((id) => ({ id, type: 'taxonomy_term--categories' })) },
        field_feed: { data: form.feeds.map((id) => ({ id, type: 'node--feed' })) },
        field_topics: { data: form.topics.map((id) => ({ id, type: 'taxonomy_term--topics' })) },
        field_type: { data: form.type ? { id: form.type, type: 'taxonomy_term--types' } : null }
      }
    };
  };

  const onSave = () => {
    const data = createNewsletterData();
    if (isEditing) return dispatch(editNewsletter({ data: { ...data, id: selectedNewsletter.id } }));
    const en = { ...data, attributes: { ...data.attributes, title: `${form.title} (English)` } }; // langcode: 'en'
    const fr = { ...data, attributes: { ...data.attributes, title: `${form.title} (French)` } };
    if (form.preferredLang === 'en') dispatch(postNewsletter(en));
    if (form.preferredLang === 'fr') dispatch(postNewsletter(fr));
    if (form.preferredLang === 'both') {
      dispatch(postNewsletter(en));
      dispatch(postNewsletter(fr));
    }
    setClickSave(true);
  };

  const feedOptions = feeds ? feeds.map(({ id, attributes: { title } }) => ({ label: title, value: id, key: id })) : [];
  const langOptions = [
    { key: 'preferred-language-format-both', value: 'both', label: 'Both' },
    { key: 'preferred-language-format-en', value: 'en', label: 'English' },
    { key: 'preferred-language-format-fr', value: 'fr', label: 'French' }
  ];
  const selectedNewsletterId = isEditing ? selectedNewsletter.id : newNewsletter ? newNewsletter.id : null;
  const isValid = Boolean(form.title && form.adminGroup && form.type && form.feeds.length > 0);
  const canCreateIssue = Boolean(newNewsletter && clickSave) || isEditing;
  return (
    <>
      <Label>Title *</Label>
      <Input name="title" value={form.title} onChange={handleChange} />

      <Label className="mt-4">Feeds *</Label>
      <Select
        isMulti
        components={{ Input: CustomSelectInput }}
        className="react-select"
        name="feeds"
        options={feedOptions}
        value={feedOptions.filter((item) => form.feeds.includes(item.value))}
        onChange={handleChangeSelect}
      />
      <Label className="mt-4">Categories</Label>
      <Select
        isMulti
        components={{ Input: CustomSelectInput }}
        className="react-select"
        name="categories"
        options={categoryOptions}
        value={categoryOptions.filter((item) => form.categories.includes(item.value))}
        onChange={handleChangeSelect}
      />
      <Label className="mt-4">Topics</Label>
      <Select
        isMulti
        components={{ Input: CustomSelectInput }}
        className="react-select"
        name="topics"
        options={topicOptions}
        value={topicOptions.filter((item) => form.topics.includes(item.value))}
        onChange={handleChangeSelect}
      />
      <Label className="mt-4">Type *</Label>
      <Select
        components={{ Input: CustomSelectInput }}
        className="react-select"
        name="type"
        options={typeOptions}
        value={typeOptions.filter((item) => form.type === item.value)}
        onChange={handleChangeSelect}
      />
      <Label className="mt-4">Admin Group *</Label>
      <Input name="adminGroup" value={form.adminGroup} onChange={handleChange} />

      <Label className="mt-4">Interval</Label>
      <Input type="number" name="interval" value={form.interval} onChange={handleChange} />
      <Label className="mt-4">Rationale</Label>
      <Input type="textarea" name="rationale" value={form.rationale} onChange={handleChange} />
      <Label className="mt-4">Preferred language</Label>
      <Select
        components={{ Input: CustomSelectInput }}
        className="react-select"
        classNamePrefix="react-select"
        options={langOptions}
        value={langOptions.find((item) => form.preferredLang === item.value)}
        onChange={handleChangeLang}
      />
      <div className="mt-4">
        <CustomInput
          id="checkbox-archive"
          type="checkbox"
          label="Archive"
          name="archive"
          checked={form.archive}
          onClick={handleChangeCheckbox}
          inline
        />
      </div>
      <Button className="mt-4" color="primary" onClick={onSave} disabled={!isValid}>
        Save Newsletter
      </Button>
      {canCreateIssue && (
        <NavLink to={`/newsletter/${selectedNewsletterId}/issue-add`}>
          <Button className="ml-3 mt-4" color="info">
            Create New Issue
          </Button>
        </NavLink>
      )}
    </>
  );
};

AddNewsletter.propTypes = { selectedNewsletter: PropTypes.object, mode: PropTypes.string };
export default AddNewsletter;
