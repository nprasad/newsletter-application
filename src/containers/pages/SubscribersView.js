import React, { useState } from 'react';
import { Card, Button } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import moment from 'moment';
import { Colxx } from '../../components/common/CustomBootstrap';
import ConfirmationModal from 'components/common/ConfirmationModal';

const SubscribersView = ({ item, newsletters, onEdit, onDelete }) => {
  const [showConfirmation, setShowConfirmation] = useState(false);
  const {
    attributes: { title, created },
    relationships: { field_newsletter }
  } = item;

  const newsletterId = field_newsletter && field_newsletter.data ? field_newsletter.data.id : '';
  const foundNewsletter = newsletters && newsletterId ? newsletters.find((item) => item.id === newsletterId) : null;
  const dateCreated = moment(created).format('D MMM, YYYY');

  return (
    <>
      <Colxx xxs="12" className="mb-3">
        <Card className="d-flex flex-row">
          <div className="pl-2 d-flex flex-grow-1 min-width-zero">
            <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
              <NavLink to={`?p=${item.id}`} className="w-40 w-sm-100">
                <p className="list-item-heading mb-1 truncate">{title}</p>
              </NavLink>
              <p className="mb-1 text-muted text-small w-15 w-sm-100">{dateCreated}</p>
              <p className="mb-1 text-muted text-small w-15 w-sm-100 truncate">{foundNewsletter && foundNewsletter.attributes.title}</p>
            </div>
            <div className="custom-control custom-checkbox pl-1 align-self-center pr-4 btn-group">
              <Button style={{ minWidth: 70 }} size="xs" color="dark" onClick={() => onEdit(item)}>
                Edit
              </Button>
              <Button style={{ minWidth: 70 }} size="xs" color="danger" onClick={() => setShowConfirmation(true)}>
                Delete
              </Button>
            </div>
          </div>
        </Card>
      </Colxx>
      <ConfirmationModal
        isOpen={showConfirmation}
        text="Are you sure that you want to delete this content?"
        onCancel={() => setShowConfirmation(false)}
        onConfirm={() => {
          onDelete(item.id);
          setShowConfirmation(false);
        }}
      />
    </>
  );
};

/* React.memo detail : https://reactjs.org/docs/react-api.html#reactpurecomponent  */
export default React.memo(SubscribersView);
