import React, { useEffect, useState } from 'react';
import CustomSelectInput from 'components/common/CustomSelectInput';
import useTaxonomies from 'hooks/use-taxonomies';
import { useIntl } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import Select from 'react-select';
import CreatableSelect from 'react-select/creatable';
import { Button, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { editFeed, postFeed } from 'redux/actions';
import { getGroupState } from 'redux/group/selectors';

const INITIAL_FORM = { title: '', link: '', categories: [], topics: [], adminGroups: [] };

const AddNewFeed = ({ modalOpen, toggleModal, edited }) => {
  const dispatch = useDispatch();
  const { formatMessage: f } = useIntl();
  const { categoryOptions, topicOptions } = useTaxonomies();
  const { groups, isAdmin } = useSelector(getGroupState);
  const [form, setForm] = useState({ ...INITIAL_FORM, adminGroups: groups || [] });
  const [existingAdminGroups, setExistingAdminGroups] = useState([]);
  const [adminGroupsList, setAdminGroupsList] = useState(groups || []);

  const handleChange = (event) => {
    const { name, value } = event.target;
    setForm({ ...form, [name]: value });
  };
  const handleChangeSelect = (values, { name }) => setForm({ ...form, [name]: values ? values.map(({ value }) => value) : [] });

  const handleChangeAdminGroups = (values, action) => {
    const createdValue = values && values.find((item) => item.__isNew__);
    const newValue = createdValue
      ? createdValue.value.startsWith('newsletter-service-')
        ? createdValue.value
        : 'newsletter-service-' + createdValue.value
      : undefined;
    if (newValue) setAdminGroupsList([...adminGroupsList, newValue]);
    setForm({
      ...form,
      [action.name]: values
        ? values.map(({ value }) => (value.startsWith('newsletter-service-') ? value : 'newsletter-service-' + value))
        : []
    });
  };
  const saveFeed = () => {
    const data = {
      type: 'node--feed',
      id: edited ? edited.id : undefined,
      attributes: {
        title: form.title,
        field_admin_group: form.adminGroups.join(','),
        field_feed_source: { uri: form.link, title: '', options: [] }
      },
      relationships: {
        field_categories: { data: form.categories.map((id) => ({ id, type: 'taxonomy_term--categories' })) },
        field_topics: { data: form.topics.map((id) => ({ id, type: 'taxonomy_term--topics' })) }
      }
    };
    dispatch(edited ? editFeed({ data, id: edited.id }) : postFeed(data));
    onToggle();
  };

  const resetForm = () => setForm(INITIAL_FORM);

  const onToggle = () => {
    resetForm();
    toggleModal();
  };

  useEffect(() => {
    if (edited) {
      const {
        attributes: { title, field_feed_source, field_admin_group },
        relationships: { field_categories, field_topics }
      } = edited;
      const link = field_feed_source ? field_feed_source.uri : '';
      setForm({
        title,
        link,
        adminGroups: field_admin_group ? field_admin_group.split(',') : [],
        categories: field_categories.data.map((item) => item.id),
        topics: field_topics.data.map((item) => item.id)
      });
      if (field_admin_group) {
        setExistingAdminGroups(field_admin_group.split(','));
      }
    }
  }, [edited]);

  const adminGroupsOptions = adminGroupsList
    ? [...adminGroupsList, ...existingAdminGroups].map((item, index) => ({
        label: item,
        value: item,
        key: item + '-' + index
      }))
    : [];

  return (
    <Modal isOpen={modalOpen} toggle={onToggle} backdrop="static">
      <ModalHeader toggle={onToggle}>{f({ id: edited ? 'pages.edit' : 'pages.add-new' })}</ModalHeader>
      <ModalBody>
        <Label>Title</Label>
        <Input name="title" value={form.title} onChange={handleChange} />
        <Label className="mt-4">Link</Label>
        <Input name="link" value={form.link} onChange={handleChange} />
        <Label className="mt-4">Categories</Label>
        <Select
          isMulti
          components={{ Input: CustomSelectInput }}
          className="react-select"
          name="categories"
          options={categoryOptions}
          value={categoryOptions.filter((item) => form.categories.includes(item.value))}
          onChange={handleChangeSelect}
        />
        <Label className="mt-4">Topics</Label>
        <Select
          isMulti
          components={{ Input: CustomSelectInput }}
          className="react-select"
          name="topics"
          options={topicOptions}
          value={topicOptions.filter((item) => form.topics.includes(item.value))}
          onChange={handleChangeSelect}
        />
        <Label className="mt-4">Admin Group</Label>

        <CreatableSelect
          isMulti
          components={{ Input: CustomSelectInput }}
          className="react-select"
          name="adminGroups"
          options={adminGroupsOptions}
          value={adminGroupsOptions.filter((item) => form.adminGroups.includes(item.value))}
          onChange={handleChangeAdminGroups}
          isDisabled={!isAdmin}
        />
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" outline onClick={onToggle}>
          {f({ id: 'pages.cancel' })}
        </Button>
        <Button color="primary" onClick={saveFeed}>
          {f({ id: 'pages.submit' })}
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default AddNewFeed;
