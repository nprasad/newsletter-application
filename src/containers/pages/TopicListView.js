import React from 'react';
import { Card } from 'reactstrap';
import moment from 'moment';
import classnames from 'classnames';
import { ContextMenuTrigger } from 'react-contextmenu';
import { Colxx } from 'components/common/CustomBootstrap';

const TopicListView = ({ item, isSelect, collect, onCheckItem }) => {
  const {
    attributes: { name, revision_created }
  } = item;

  const dateCreated = moment(revision_created).format('D MMM, YYYY');

  return (
    <Colxx xxs="4" className="mb-3">
      <ContextMenuTrigger id="menu_id" data={item.id} collect={collect}>
        <Card
          onClick={(event) => onCheckItem(event, item.id)}
          className={classnames('d-flex flex-row', {
            active: isSelect
          })}
        >
          <div className="pl-2 d-flex flex-grow-1 min-width-zero">
            <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
              <p className="w-50 w-sm-100 mb-1 list-item-heading truncate">{name}</p>
              <p className="w-50 w-sm-100 mb-1 text-muted text-small ">{dateCreated}</p>
            </div>
          </div>
        </Card>
      </ContextMenuTrigger>
    </Colxx>
  );
};

/* React.memo detail : https://reactjs.org/docs/react-api.html#reactpurecomponent  */
export default React.memo(TopicListView);
