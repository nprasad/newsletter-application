import React from 'react';
import PropTypes from 'prop-types';
import { Card, Button } from 'reactstrap';
import moment from 'moment';
import { Colxx } from '../../components/common/CustomBootstrap';
import useTaxonomies from 'hooks/use-taxonomies';
import { compare } from 'helpers/Utils';

const LatestNewsletters = ({ counter, newsletters, onClone }) => {
  const { findTaxonomy } = useTaxonomies();
  const filteredNewsletters = newsletters.sort((a, b) => compare(a.attributes['created'], b.attributes['created']));

  return filteredNewsletters.slice(0, counter).map((newsletter) => {
    const { id, attributes, relationships } = newsletter;
    const categories = findTaxonomy(relationships, 'field_categories');
    return (
      <Colxx xxs="12" key={id} className="mb-3">
        <Card onClick={(event) => console.log(event, id)} className="d-flex flex-row">
          <div className="pl-1 d-flex flex-grow-1 min-width-zero">
            <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
              <p className="list-item-heading mb-1 truncate w-40">{attributes.title}</p>
              <p className="mb-1 text-muted text-small w-20 w-sm-100 truncate">{moment(attributes.created).format('D MMM, YYYY')}</p>
              <div className="w-20 w-sm-100 text-muted truncate">{!!categories.length && categories[0].name}</div>
            </div>
            <div className="custom-control pl-1 align-self-center pr-2">
              <Button
                size="xs"
                color="dark"
                onClick={(e) => {
                  e.stopPropagation();
                  onClone(newsletter);
                }}
              >
                Clone
              </Button>
            </div>
          </div>
        </Card>
      </Colxx>
    );
  });
};

LatestNewsletters.propTypes = { counter: PropTypes.number, newsletters: PropTypes.array, onClone: PropTypes.func };
LatestNewsletters.defaultProps = { newsletters: [] };
/* React.memo detail : https://reactjs.org/docs/react-api.html#reactpurecomponent  */
export default React.memo(LatestNewsletters);
