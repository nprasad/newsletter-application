import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Card, CardBody } from 'reactstrap';
import LinesEllipsis from 'react-lines-ellipsis';
import responsiveHOC from 'react-lines-ellipsis/lib/responsiveHOC';
import useTaxonomies from 'hooks/use-taxonomies';

const ResponsiveEllipsis = responsiveHOC()(LinesEllipsis);

const NewsletterDetails = ({ newsletter, isFull, children }) => {
  const { categories, topics, types, findTaxonomy } = useTaxonomies();
  const categoriesList = newsletter && categories ? findTaxonomy(newsletter.relationships, 'field_categories') : [];
  const categoryNames = categoriesList.length ? categoriesList.map((category) => category.name) : [];
  const topicsList = newsletter && topics ? findTaxonomy(newsletter.relationships, 'field_topics') : [];
  const topicNames = topicsList.length ? topicsList.map((category) => category.name) : [];
  const typesList = newsletter && types ? findTaxonomy(newsletter.relationships, 'field_type') : [];
  const typesNames = typesList.length ? typesList.map((category) => category.name) : [];

  return (
    <Card className="mb-4 h-100">
      <CardBody>
        <ResponsiveEllipsis
          className="listing-heading d-inline"
          text={newsletter.attributes.title}
          maxLine="2"
          trimRight
          basedOn="words"
          component="h5"
        />
        <p className="text-muted text-small d-inline pl-2">
          ( {moment(newsletter.attributes.created).format('D MMM, YYYY')} ) {newsletter.attributes.field_archive && '(Archive)'}
        </p>
        <p className="mt-3 mb-2">
          Interval: <span className="text-muted text-small">{newsletter.attributes.field_interval} days</span>
        </p>

        {!!categoryNames.length && (
          <p className="mb-2">
            Categories:
            <span className="text-muted text-small"> {categoryNames.join(', ')}</span>
          </p>
        )}
        {!!topicNames.length && (
          <p className="mb-2">
            Topics:
            <span className="text-muted text-small"> {topicNames.join(', ')}</span>
          </p>
        )}
        {!!typesNames.length && (
          <p className="mb-2">
            Type:
            <span className="text-muted text-small"> {typesNames.join(', ')}</span>
          </p>
        )}

        <p className="mb-2">
          Admin Group:<span className="text-muted text-small"> {newsletter.attributes.field_admin_group}</span>
        </p>

        {isFull && newsletter.attributes.field_rationale && (
          <>
            <p className="mb-2">Rationale </p>
            <p className="text-muted">{newsletter.attributes.field_rationale.value}</p>
          </>
        )}
      </CardBody>
      {children}
    </Card>
  );
};

NewsletterDetails.propTypes = { newsletter: PropTypes.object, isFull: PropTypes.bool, children: PropTypes.element };

export default NewsletterDetails;
