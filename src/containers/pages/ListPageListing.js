import React from 'react';
import { Row } from 'reactstrap';
import Pagination from './Pagination';
import ContextMenuContainer from './ContextMenuContainer';
import FeedListView from './FeedListView';

function collect(props) {
  return { data: props.data };
}

const ListPageListing = ({
  items,
  selectedItems,
  onCheckItem,
  currentPage,
  totalPage,
  onContextMenuClick,
  onContextMenu,
  onChangePage
}) => {
  return (
    <Row>
      {items.map((item) => (
        <FeedListView key={item.id} item={item} isSelect={selectedItems.includes(item.id)} onCheckItem={onCheckItem} collect={collect} />
      ))}
      <Pagination currentPage={currentPage} totalPage={totalPage} onChangePage={(i) => onChangePage(i)} />
      <ContextMenuContainer onContextMenuClick={onContextMenuClick} onContextMenu={onContextMenu} />
    </Row>
  );
};

export default ListPageListing;
