import React from 'react';
import { Card } from 'reactstrap';
import moment from 'moment';
import classnames from 'classnames';
import { ContextMenuTrigger } from 'react-contextmenu';
import { Colxx } from 'components/common/CustomBootstrap';

const CategoryListView = ({ item, isSelect, collect, onCheckItem }) => {
  const {
    attributes: { name, revision_created, field_relatedroles, drupal_internal__tid: categoryId }
  } = item;

  const dateCreated = moment(revision_created).format('D MMM, YYYY');

  return (
    <Colxx xxs="6" className="mb-3">
      <ContextMenuTrigger id="menu_id" data={item.id} collect={collect}>
        <Card
          onClick={(event) => onCheckItem(event, item.id)}
          className={classnames('d-flex flex-row', {
            active: isSelect
          })}
        >
          <div className="pl-2 d-flex flex-grow-1 min-width-zero">
            <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
              <div className="w-40 w-sm-100">
                <p className="list-item-heading mb-1 truncate">{name}</p>
                <p className="list-item-heading text-muted mb-1 truncate">
                  <strong>Group: </strong>
                  {field_relatedroles}
                </p>
                <p className="list-item-heading text-muted mb-1 truncate">
                  <strong> Category Id: </strong>
                  {categoryId}
                </p>
              </div>

              <p className="mb-1 text-muted text-small w-15 w-sm-100">{dateCreated}</p>
            </div>
          </div>
        </Card>
      </ContextMenuTrigger>
    </Colxx>
  );
};

/* React.memo detail : https://reactjs.org/docs/react-api.html#reactpurecomponent  */
export default React.memo(CategoryListView);
