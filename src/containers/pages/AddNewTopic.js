import React, { useState, useEffect } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, Label } from 'reactstrap';
import { useIntl } from 'react-intl';
import { useDispatch } from 'react-redux';
import { postTaxonomy, editTaxonomy } from 'redux/actions';

const INITIAL_FORM = { name: '', description: '' };
const AddNewTopic = ({ modalOpen, toggleModal, edited }) => {
  const dispatch = useDispatch();
  const { formatMessage: f } = useIntl();

  const [form, setForm] = useState(INITIAL_FORM);
  const handleChange = (event) => {
    const { name, value } = event.target;
    setForm({ ...form, [name]: value });
  };

  const saveTaxonomy = () => {
    const data = {
      type: 'taxonomy_term--topics',
      id: edited ? edited.id : undefined,
      attributes: {
        name: form.name,
        description: form.description
      }
    };
    dispatch(edited ? editTaxonomy({ taxonomy: 'topics', data, id: edited.id }) : postTaxonomy({ taxonomy: 'topics', data }));
    onToggle();
  };

  const resetForm = () => setForm(INITIAL_FORM);

  const onToggle = () => {
    resetForm();
    toggleModal();
  };

  useEffect(() => {
    if (edited) {
      const {
        attributes: { name, description, field_relatedroles }
      } = edited;
      setForm({ name, description: description ? description.value : '', adminGroup: field_relatedroles });
    }
  }, [edited]);

  return (
    <Modal isOpen={modalOpen} toggle={onToggle} backdrop="static">
      <ModalHeader toggle={onToggle}>{f({ id: edited ? 'pages.edit' : 'pages.add-new' })}</ModalHeader>
      <ModalBody>
        <Label>Name</Label>
        <Input name="name" value={form.name} onChange={handleChange} />
        <Label className="mt-4">Description</Label>
        <Input type="textarea" name="description" value={form.description} onChange={handleChange} />
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" outline onClick={onToggle}>
          {f({ id: 'pages.cancel' })}
        </Button>
        <Button color="primary" onClick={saveTaxonomy}>
          {f({ id: 'pages.submit' })}
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default AddNewTopic;
