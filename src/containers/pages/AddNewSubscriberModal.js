/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, Label } from 'reactstrap';
import Select from 'react-select';
import CustomSelectInput from '../../components/common/CustomSelectInput';
import { useIntl } from 'react-intl';
import { useDispatch } from 'react-redux';
import { postSubscriber, editSubscriber } from 'redux/actions';

const INITIAL_FORM = { title: '', email: '', newsletters: '' };
const AddNewSubbscriberModal = ({ modalOpen, toggleModal, edited, newsletters }) => {
  const { formatMessage: f } = useIntl();
  const dispatch = useDispatch();
  const [form, setForm] = useState(INITIAL_FORM);
  const handleChange = (event) => {
    const { name, value } = event.target;
    setForm({ ...form, [name]: value });
  };
  const handleChangeNewsletter = ({ value }) => {
    // const values = target.map(({ value }) => value);
    setForm({ ...form, newsletters: value });
  };

  const onSave = () => {
    const data = {
      type: 'node--subscription',
      id: edited ? edited.id : undefined,
      attributes: {
        title: form.title,
        field_email: form.email
      },
      relationships: {
        // field_newsletter: { data: form.newsletters.map((val) => ({ id: val, type: 'node--newsletter' })) }
        field_newsletter: { data: { id: form.newsletters, type: 'node--newsletter' } }
      }
    };
    dispatch(edited ? editSubscriber({ data, id: edited.id }) : postSubscriber(data));
    onToggle();
  };

  const resetForm = () => setForm(INITIAL_FORM);

  const onToggle = () => {
    resetForm();
    toggleModal();
  };

  useEffect(() => {
    if (edited) {
      const {
        attributes: { title, field_email: email },
        relationships: {
          field_newsletter: { data }
        }
      } = edited;
      let newsletters = '';
      if (data) {
        newsletters = data instanceof Array ? data.map((data) => data.id) : data.id;
      }
      setForm({ title, email, newsletters });
    }
  }, [edited]);

  const newsletterOptions = newsletters ? newsletters.map(({ attributes: { title }, id }) => ({ label: title, value: id, key: id })) : [];

  return (
    <Modal isOpen={modalOpen} toggle={onToggle} backdrop="static">
      <ModalHeader toggle={onToggle}>{f({ id: 'pages.add-new-subscriber' })}</ModalHeader>
      <ModalBody>
        <Label>Title</Label>
        <Input name="title" value={form.title} onChange={handleChange} />
        <Label className="mt-4">Email</Label>
        <Input name="email" value={form.email} onChange={handleChange} />
        <Label className="mt-4">Newsletters</Label>
        <Select
          components={{ Input: CustomSelectInput }}
          className="react-select"
          classNamePrefix="react-select"
          name="form-field-name"
          // isMulti
          options={newsletterOptions}
          value={newsletterOptions.filter((item) => form.newsletters === item.value)}
          onChange={handleChangeNewsletter}
        />
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" outline onClick={onToggle}>
          {f({ id: 'pages.cancel' })}
        </Button>
        <Button color="primary" onClick={onSave}>
          {f({ id: 'pages.submit' })}
        </Button>
      </ModalFooter>
    </Modal>
  );
};

AddNewSubbscriberModal.propTypes = {
  modalOpen: PropTypes.bool,
  toggleModal: PropTypes.func,
  newsletters: PropTypes.array,
  edited: PropTypes.shape({
    id: PropTypes.string,
    attributes: PropTypes.shape({
      title: PropTypes.string,
      field_email: PropTypes.string
    }),
    relationships: PropTypes.shape({
      field_newsletter: PropTypes.shape({
        data: PropTypes.oneOfType([PropTypes.array, PropTypes.object])
      })
    })
  })
};

export default AddNewSubbscriberModal;
