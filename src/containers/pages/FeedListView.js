import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Card, Button } from 'reactstrap';
import classnames from 'classnames';
import { ContextMenuTrigger } from 'react-contextmenu';
import moment from 'moment';
import { Colxx } from 'components/common/CustomBootstrap';
import { NotificationManager } from 'components/common/react-notifications';
import useTaxonomies from 'hooks/use-taxonomies';

const taxonomiesToString = (feedTaxonomy, taxonomy) => {
  const includedIds = feedTaxonomy ? feedTaxonomy.data.map((d) => d.id) : [];
  const filtered = taxonomy.filter((t) => includedIds.includes(t.value));
  const names = filtered.map((c) => c.label);
  return names.join(', ');
};

const FeedListView = ({ item, isSelect, collect, onCheckItem }) => {
  const [hasRun, setHasRun] = useState(false);
  const { categoryOptions, topicOptions } = useTaxonomies();
  const {
    attributes: { title, created, field_feed_source, field_admin_group },
    relationships: { field_categories, field_topics }
  } = item;
  const uri = field_feed_source ? field_feed_source.uri : '';

  const dateCreated = moment(created).format('D MMM, YYYY');

  const runCron = async (e) => {
    e.stopPropagation();
    const response = await fetch(
      'https://cnpdfs.web.cern.ch/cron/r5FNZmYsAbrIREmRmdJHAPX58lJkw50z68vFMwjQ7oDEzjhvpIpqusSB-tTm8hS2jNxttZYg2g'
    );
    if (response.ok) setHasRun(true);
  };
  useEffect(() => {
    if (hasRun) {
      NotificationManager.info(hasRun, 'Run was successful', 3000, null, null, '');
      setHasRun(false);
    }
  }, [hasRun]);

  const categoriesToString = () => taxonomiesToString(field_categories, categoryOptions);
  const topicsToString = () => taxonomiesToString(field_topics, topicOptions);

  return (
    <Colxx xxs="12" className="mb-3">
      <ContextMenuTrigger id="menu_id" data={item.id} collect={collect}>
        <Card
          onClick={(event) => onCheckItem(event, item.id)}
          className={classnames('d-flex flex-row', {
            active: isSelect
          })}
        >
          <div className="pl-2 d-flex flex-grow-1 min-width-zero">
            <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
              <div className="w-40 w-sm-100">
                <p className="list-item-heading mb-1 truncate">{title}</p>
                <p className="list-item-heading text-muted mb-1 truncate">{uri}</p>
                <p className="list-item-heading text-muted mb-1 truncate">
                  <strong>Group: </strong>
                  {field_admin_group}
                </p>
                <p className="list-item-heading text-muted mb-1 truncate">
                  <strong>Categories: </strong>
                  {categoriesToString()}
                </p>
                <p className="list-item-heading text-muted mb-1 truncate">
                  <strong>Topics: </strong>
                  {topicsToString()}
                </p>
              </div>

              <p className="mb-1 text-muted text-small w-15 w-sm-100">{dateCreated}</p>
            </div>
            <div className="custom-control custom-checkbox pl-1 align-self-center pr-4">
              <Button color="primary" onClick={runCron}>
                RUN
              </Button>
            </div>
          </div>
        </Card>
      </ContextMenuTrigger>
    </Colxx>
  );
};
FeedListView.propTypes = {
  item: PropTypes.any.isRequired,
  isSelect: PropTypes.bool,
  collect: PropTypes.any,
  onCheckItem: PropTypes.func
};

export default FeedListView;
