import React, { Suspense, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import { IntlProvider } from 'react-intl';
import AppLocale from 'lang';
import { NotificationContainer } from 'components/common/react-notifications';
import AuthRoute from 'components/AuthRoute';
import useKeycloak from 'hooks/use-keycloak';
import { fetchTaxonomy } from 'redux/actions';

const ViewApp = React.lazy(() => import('views'));
const ViewError = React.lazy(() => import('views/error'));

const App = () => {
  const dispatch = useDispatch();
  const { locale } = useSelector((state) => state.settings);
  const { authenticated } = useKeycloak();

  useEffect(() => {
    if (authenticated) {
      dispatch(fetchTaxonomy('categories'));
      dispatch(fetchTaxonomy('topics'));
      dispatch(fetchTaxonomy('types'));
    }
  }, [authenticated, dispatch]);

  return (
    <div className="h-100">
      <IntlProvider locale={locale} messages={AppLocale[locale]}>
        <NotificationContainer />
        <Suspense fallback={<div className="loading" />}>
          <Router>
            <Switch>
              <AuthRoute path="/" authenticated={authenticated} component={ViewApp} />
              <Route path="/error" exact render={(props) => <ViewError {...props} />} />
              <Redirect to="/error" />
            </Switch>
          </Router>
        </Suspense>
      </IntlProvider>
    </div>
  );
};

export default App;
