/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  setKeycloakAuthenticated,
  setKeycloak,
  setUserToken,
  setKeycloakRoles,
  setKeycloakUserName,
  setExchangeToken,
  fetchIdentity,
  setKeycloakCernUpn
} from 'redux/actions';
import KeycloakJs from 'keycloak-js';
import * as cfg from 'constants/config';
import httpService from 'helpers/HttpService';

const Keycloak = process.env.NODE_ENV === 'development' ? KeycloakJs : window.Keycloak;
const keycloakInitOptions =
  process.env.NODE_ENV === 'development'
    ? { onLoad: 'login-required' }
    : { onLoad: 'login-required', flow: 'implicit', promiseType: 'native' };

export default () => {
  const dispatch = useDispatch();
  const { authenticated, keycloak } = useSelector((state) => state.keycloak);

  useEffect(() => {
    if (!authenticated) {
      const keycloak = Keycloak({
        url: cfg.keycloakUrl,
        realm: cfg.keycloakRealm,
        clientId: cfg.keycloakClientId
      });
      keycloak.init(keycloakInitOptions).then((authenticated) => {
        dispatch(setKeycloak(keycloak));
        dispatch(setKeycloakAuthenticated(authenticated));
        dispatch(setUserToken(keycloak.token));

        // Fetch Login user identity
        httpService
          .getAuthApiToken()
          .then((exchangeToken) => {
            if (exchangeToken) {
              dispatch(setExchangeToken(exchangeToken));
              dispatch(fetchIdentity());
            }
          })
          .catch(console.error);

        if (cfg.refreshKeycloakToken && keycloakInitOptions.flow !== 'implicit') {
          // But let's keep it alive!
          setInterval(() => {
            keycloak
              .updateToken(30)
              .success((refreshed) => {
                if (refreshed) {
                  console.debug('Token refreshed' + refreshed);
                } else {
                  console.warn(
                    'Token not refreshed, valid for ' +
                      Math.round(keycloak.tokenParsed.exp + keycloak.timeSkew - new Date().getTime() / 1000) +
                      ' seconds'
                  );
                }
              })
              .error(() => {
                console.error('Failed to refresh token');
                keycloak.logout();
              });
          }, 30000);
        }
      });
    }
  }, []);

  useEffect(() => {
    const _roles = keycloak && keycloak.realmAccess ? keycloak.realmAccess.roles : [];
    dispatch(setKeycloakRoles(_roles));
    const loggedInUserName = () => {
      let userName = 'unknown user';
      if (keycloak && keycloak.tokenParsed) {
        const {
          tokenParsed: { given_name, family_name, preferred_username }
        } = keycloak;
        userName = given_name && family_name ? `${given_name} ${family_name}` : preferred_username;
      }
      return userName;
    };
    const getCernUpn = () => (keycloak && keycloak.tokenParsed && keycloak.tokenParsed.cern_upn) || 'Cern User';
    const userName = loggedInUserName();
    const upn = getCernUpn();
    dispatch(setKeycloakCernUpn(upn));
    dispatch(setKeycloakUserName(userName));
  }, [keycloak]);

  return Object.assign([keycloak, authenticated], { authenticated, keycloak });
};
