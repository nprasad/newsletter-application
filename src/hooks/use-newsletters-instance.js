import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchInstances, fetchTaxonomy } from 'redux/actions';
import { getGroupState } from 'redux/group/selectors';

export default () => {
  const dispatch = useDispatch();
  const { categories } = useSelector((state) => state.taxonomy);
  const { instance, instances } = useSelector((state) => state.newsletter);
  const { groups, isAdmin } = useSelector(getGroupState);

  const query =
    !isAdmin && groups && groups.length
      ? `?filter[field_newsletter.field_admin_group][operator]=IN${groups
          .map((group) => `&filter[field_newsletter.field_admin_group][value][]=${group}`)
          .join('')}`
      : '';

  useEffect(() => {
    if (!categories) dispatch(fetchTaxonomy('categories'));
    dispatch(fetchInstances(query));
    return () => {};
  }, [query, categories, dispatch]);

  // const sentInstances = instances.filter((i) => new Date(i.attributes.field_schedule) < new Date());
  // const nextSendingInstances = instances.filter((i) => new Date(i.attributes.field_schedule) >= new Date());
  const sentInstances = instances && instances.length ? instances.filter((i) => i.attributes.field_processed) : [];
  const nextSendingInstances = instances && instances.length ? instances.filter((i) => !i.attributes.field_processed) : [];
  return { instance, instances, sentInstances, nextSendingInstances };
};
