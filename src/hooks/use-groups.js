import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchGroups } from 'redux/actions';
import { getGroupState } from 'redux/group/selectors';

export default () => {
  const dispatch = useDispatch();
  const { identity, groups } = useSelector(getGroupState);

  useEffect(() => {
    if (!groups) dispatch(fetchGroups());
    return () => {};
  }, [dispatch, groups, identity]);

  return { groups };
};
