/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect } from 'react';
import { editTaxonomy, deleteTaxonomy, postTaxonomy, fetchTaxonomy } from 'redux/actions';
import { useDispatch, useSelector } from 'react-redux';

export default () => {
  const dispatch = useDispatch();
  const { categories, topics, types } = useSelector((state) => state.taxonomy);

  useEffect(() => {
    if (!categories) dispatch(fetchTaxonomy('categories'));
    if (!topics) dispatch(fetchTaxonomy('topics'));
    if (!types) dispatch(fetchTaxonomy('types'));
    return () => {};
  }, []);

  const categoryOptions = categories ? categories.map(({ id, attributes: { name } }) => ({ label: name, value: id, key: id })) : [];
  const topicOptions = topics ? topics.map(({ id, attributes: { name } }) => ({ label: name, value: id, key: id })) : [];
  const typeOptions = types ? types.map(({ id, attributes: { name } }) => ({ label: name, value: id, key: id })) : [];

  // Giving a elationship it returns a taxonomy
  const findTaxonomy = (relationships, key) => {
    const taxonomyData = key === 'field_categories' ? categories : key === 'field_topics' ? topics : types;
    const mappedTaxonomy = taxonomyData.map((cat) => ({ id: cat.id, name: cat.attributes.name }));
    const taxonomies = relationships[key].data;
    let results = Array.isArray(taxonomies)
      ? taxonomies.reduce((acc, cur) => (acc = [...acc, mappedTaxonomy.find((category) => category.id === cur.id)]), [])
      : taxonomies
      ? [mappedTaxonomy.find((category) => category.id === taxonomies.id)]
      : [];
    return results.filter(Boolean);
  };

  const _post = (feedId) => dispatch(postTaxonomy(feedId));
  const _edit = (feedId) => dispatch(editTaxonomy(feedId));
  const _delete = (feedId) => dispatch(deleteTaxonomy(feedId));

  return {
    categories,
    topics,
    types,
    categoryOptions,
    topicOptions,
    typeOptions,
    findTaxonomy,
    postTaxonomy: _post,
    editTaxonomy: _edit,
    deleteTaxonomy: _delete
  };
};
