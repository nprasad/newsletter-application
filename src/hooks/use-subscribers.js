import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchSubscribers } from 'redux/actions';
import { getGroupState } from 'redux/group/selectors';

export default () => {
  const dispatch = useDispatch();
  const { subscribers } = useSelector((state) => state.subscriber);
  const { groups, isAdmin } = useSelector(getGroupState);

  const query =
    !isAdmin && groups && groups.length
      ? `?filter[field_newsletter.field_admin_group][operator]=IN${groups
          .map((group) => `&filter[field_newsletter.field_admin_group][value][]=${group}`)
          .join('')}`
      : '';

  useEffect(() => {
    if (!subscribers) dispatch(fetchSubscribers(query));
  }, [query, subscribers, dispatch]);

  return { subscribers };
};
