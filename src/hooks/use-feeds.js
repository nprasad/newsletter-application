import { useEffect } from 'react';
import { deleteFeed, fetchFeeds } from 'redux/actions';
import { useDispatch, useSelector } from 'react-redux';
import { getGroupState } from 'redux/group/selectors';

export default () => {
  const dispatch = useDispatch();
  const { feeds } = useSelector((state) => state.feed);
  const { groups, isAdmin } = useSelector(getGroupState);

  const query =
    !isAdmin && groups && groups.length
      ? `?filter[field_admin_group][operator]=IN${groups.map((group) => `&filter[field_admin_group][value][]=${group}`).join('')}`
      : '';

  useEffect(() => {
    if (!feeds) dispatch(fetchFeeds(query));
  }, [query, feeds, dispatch]);

  const _delete = (feedId) => dispatch(deleteFeed(feedId));
  return { feeds, deleteFeed: _delete };
};
