import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchNewsletters } from 'redux/actions';

export default () => {
  const dispatch = useDispatch();
  const { newsletter, newsletters, totalNewsletters } = useSelector((state) => state.newsletter);

  useEffect(() => {
    if (!newsletters) dispatch(fetchNewsletters());
  }, [newsletters, dispatch]);

  return { newsletter, newsletters, totalNewsletters };
};
