import { all, fork, put, takeEvery } from 'redux-saga/effects';
import * as Actions from '../actions';
import Api from 'helpers/Api';
import { NotificationManager } from 'components/common/react-notifications';

const singular = { topics: 'Topic', categories: 'Category', types: 'Type' };
export function* watchFetchTaxonomy() {
  yield takeEvery(Actions.FETCH_TOPICS, fetchTaxonomy);
}

function* fetchTaxonomy({ payload }) {
  try {
    yield put(Actions.setLoading('taxonomies', true));
    const { taxonomy } = payload;
    const url = `taxonomy_term/${taxonomy}`;
    const response = yield Api.get(url);
    const responseData = yield response.json();
    if (response.ok) {
      const { data } = responseData;
      if (taxonomy === 'topics') yield put(Actions.fetchTopicsSuccess(data));
      if (taxonomy === 'categories') yield put(Actions.fetchCategoriesSuccess(data));
      if (taxonomy === 'types') yield put(Actions.fetchTypesSuccess(data));
    } else {
      NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    }
    yield put(Actions.setLoading('taxonomies', false));
  } catch (error) {
    yield put(Actions.setLoading('taxonomies', false));
    NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
  }
}

export function* watchPostTaxonomy() {
  yield takeEvery(Actions.POST_TAXONOMY, postTaxonomy);
}

function* postTaxonomy({ payload }) {
  try {
    yield put(Actions.setLoading('taxonomies', true));
    const { taxonomy, data } = payload;
    const url = `taxonomy_term/${taxonomy}`;
    const response = yield Api.post(url, { data });
    if (response.ok) {
      NotificationManager.success(true, `${singular[taxonomy]} Created`, 3000, null, null, '');
      yield put(Actions.fetchTaxonomy(taxonomy));
    } else {
      NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    }
    yield put(Actions.setLoading('taxonomies', false));
  } catch (error) {
    NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    yield put(Actions.setLoading('taxonomies', false));
  }
}

export function* watchEditTaxonomy() {
  yield takeEvery(Actions.EDIT_TAXONOMY, editTaxonomy);
}

function* editTaxonomy({ payload }) {
  try {
    yield put(Actions.setLoading('taxonomies', true));
    const { taxonomy, data, id } = payload;
    const url = `taxonomy_term/${taxonomy}/${id}`;
    const response = yield Api.update(url, { data });
    if (response.ok) {
      NotificationManager.success(true, `${singular[taxonomy]} Modified`, 3000, null, null, '');
      yield put(Actions.fetchTaxonomy(taxonomy));
    } else {
      NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    }
    yield put(Actions.setLoading('taxonomies', false));
  } catch (error) {
    NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    yield put(Actions.setLoading('taxonomies', false));
  }
}

export function* watchDeleteTaxonomy() {
  yield takeEvery(Actions.DELETE_TAXONOMY, deleteTaxonomy);
}

function* deleteTaxonomy({ payload }) {
  try {
    yield put(Actions.setLoading('taxonomies', true));
    const { taxonomy, id } = payload;
    const url = `taxonomy_term/${taxonomy}/${id}`;
    const { ok, status } = yield Api.delete(url);
    if (ok && status === 204) {
      NotificationManager.success(true, `${singular[taxonomy]} Deleted`, 3000, null, null, '');
      yield put(Actions.fetchTaxonomy(taxonomy));
    } else {
      NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    }
    yield put(Actions.setLoading('taxonomies', false));
  } catch (error) {
    NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    yield put(Actions.setLoading('taxonomies', false));
  }
}

export default function* rootSaga() {
  yield all([fork(watchFetchTaxonomy), fork(watchPostTaxonomy), fork(watchEditTaxonomy), fork(watchDeleteTaxonomy)]);
}
