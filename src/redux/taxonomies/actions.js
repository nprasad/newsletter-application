import * as Actions from '../actions';

export const fetchTaxonomy = (taxonomy) => ({ type: Actions.FETCH_TOPICS, payload: { taxonomy } });
export const fetchTopicsSuccess = (data) => ({ type: Actions.FETCH_TOPICS_SUCCESS, payload: data });
export const fetchCategoriesSuccess = (data) => ({ type: Actions.FETCH_CATEGORIES_SUCCESS, payload: data });
export const fetchTypesSuccess = (data) => ({ type: Actions.FETCH_TYPES_SUCCESS, payload: data });
export const postTaxonomy = (payload) => ({ type: Actions.POST_TAXONOMY, payload });
export const editTaxonomy = (payload) => ({ type: Actions.EDIT_TAXONOMY, payload });
export const deleteTaxonomy = (payload) => ({ type: Actions.DELETE_TAXONOMY, payload });
