import * as Actions from '../actions';

const INITIAL_STATE = {
  topics: undefined,
  categories: undefined,
  types: undefined
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Actions.FETCH_TOPICS_SUCCESS:
      return { ...state, topics: action.payload };
    case Actions.FETCH_CATEGORIES_SUCCESS:
      return { ...state, categories: action.payload };
    case Actions.FETCH_TYPES_SUCCESS:
      return { ...state, types: action.payload };
    default:
      return { ...state };
  }
};
