import * as Actions from '../actions';

export const onSetDesignerState = (state) => ({
  type: Actions.ON_SET_DESIGNER_STATE,
  payload: state
});
export const onChangeStyles = (styles) => ({
  type: Actions.ON_CHANGE_STYLES,
  payload: styles
});

export const onAddComponent = (component) => ({
  type: Actions.ON_ADD_COMPONENT,
  payload: component
});
export const onDragEndComponent = (result) => ({
  type: Actions.ON_DRAG_END_COMPONENT,
  payload: result
});
export const onChangeComponent = (componentId, key, value, withProps) => ({
  type: Actions.ON_CHANGE_ATTRIBUTES,
  payload: { componentId, key, value, withProps }
});
export const onEditComponent = (componentId) => ({
  type: Actions.ON_EDIT_COMPONENT,
  payload: componentId
});
export const onRemoveComponent = (componentId, type) => ({
  type: Actions.ON_REMOVE_COMPONENT,
  payload: { componentId, type }
});
export const onTogglePreview = () => ({
  type: Actions.ON_TOGGLE_PREVIEW
});
export const onSaveTemplate = (html) => ({
  type: Actions.ON_SAVE_TEMPLATE,
  payload: html
});

export const resetDesigner = () => ({ type: Actions.RESET_DESIGNER });
