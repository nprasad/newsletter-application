import * as Actions from '../actions';

const INITIAL_COMPONENT_ATTRIBUTES = {
  droppableKey: '',
  props: {},
  html: ''
};

const INITIAL_STYLES = {
  global: { backgroundColor: '#198763', color: '#198763', fontFamily: 'Arial, sans-serif' },
  header: { backgroundColor: '#1e1e1e' },
  headerTitle: { color: '#fff', fontSize: '14px' },
  headerLinks: { color: '#ccc', fontSize: '12px' },
  widgetTitle: { fontSize: '25px' },
  widgetLinks: { fontSize: '12px' },
  articleTitle: { fontSize: '25px' },
  excerptTitle: { fontSize: '12px' },
  footer: { backgroundColor: '#eeeeee', color: '#696969', fontSize: '14px' },
  footerLinks: { color: '#198763', fontSize: '12px' }
};

const INITIAL_STATE = {
  showPreview: false,
  styles: INITIAL_STYLES,
  editedComponent: '',
  components: {},
  droppables: {
    header: {
      id: 'header',
      title: 'header',
      componentIds: []
    },
    article: {
      id: 'article',
      title: 'article',
      componentIds: []
    },
    sidebar: {
      id: 'sidebar',
      title: 'sidebar',
      componentIds: []
    },
    preFooter: {
      id: 'preFooter',
      title: 'preFooter',
      componentIds: []
    },
    footer: {
      id: 'footer',
      title: 'footer',
      componentIds: []
    }
  },
  html: ''
};

const onAddComponents = (state, action) => {
  const { payload: component } = action;
  let droppableKey;
  if (component.type === 'header') droppableKey = 'header';
  if (component.type === 'article') droppableKey = 'article';
  if (component.type === 'emptyArticle') droppableKey = 'article';
  if (component.type === 'articlesWidget') droppableKey = 'article';
  if (component.type === 'titleWidget') droppableKey = 'article';
  if (component.type === 'emptyArticleSidebar') droppableKey = 'sidebar';
  if (component.type === 'linksWidget') droppableKey = 'sidebar';
  if (component.type === 'socialWidget') droppableKey = 'sidebar';
  if (component.type === 'articlesLinksWidget') droppableKey = 'sidebar';
  if (component.type === 'cdsWidget') droppableKey = 'preFooter';
  if (component.type === 'footer') droppableKey = 'footer';

  const selectedDroppable = state.droppables[droppableKey];
  const newDroppable = {
    ...selectedDroppable,
    componentIds: [...selectedDroppable.componentIds, component.id]
  };

  return {
    ...state,
    components: { ...state.components, [component.id]: { ...INITIAL_COMPONENT_ATTRIBUTES, ...component, droppableKey } },
    droppables: {
      ...state.droppables,
      [newDroppable.id]: newDroppable
    }
  };
};

const onRemoveComponent = (state, action) => {
  const {
    payload: { componentId, type }
  } = action;
  const { components, droppables } = state;
  const componentIds = droppables[type].componentIds;
  const newComponents = { ...components };
  delete newComponents[componentId];
  const filteredComponents = componentIds.filter((c) => c !== componentId);

  return {
    ...state,
    components: newComponents,
    droppables: { ...state.droppables, [type]: { ...state.droppables[type], componentIds: filteredComponents } }
  };
};

const onDragEnd = (state, action) => {
  const { destination, source, draggableId } = action.payload;

  if (!destination) return state;

  const canDragCDS =
    !['cdsWidget'].some((c) => draggableId.split('-')[0] === c) || !['sidebar', 'preFooter'].includes(destination.droppableId);

  if (destination.droppableId === source.droppableId && destination.index === source.index) return state;
  if (destination.droppableId !== source.droppableId && canDragCDS) return state;
  const start = state.droppables[source.droppableId];
  const finish = state.droppables[destination.droppableId];
  if (start === finish) {
    const newcomponentIds = [...start.componentIds];
    newcomponentIds.splice(source.index, 1);
    newcomponentIds.splice(destination.index, 0, draggableId);

    const newDroppable = {
      ...start,
      componentIds: newcomponentIds
    };

    const newState = {
      ...state,
      components: { ...state.components, [draggableId]: { ...state.components[draggableId], droppableKey: destination.droppableId } },
      droppables: {
        ...state.droppables,
        [newDroppable.id]: newDroppable
      }
    };
    return newState;
  }

  const startComponentIds = Array.from(start.componentIds);
  startComponentIds.splice(source.index, 1);

  const newStart = {
    ...start,
    componentIds: startComponentIds
  };
  const finishComponentIds = Array.from(finish.componentIds);
  finishComponentIds.splice(destination.index, 0, draggableId);
  const newFinish = {
    ...finish,
    componentIds: finishComponentIds
  };
  const newState = {
    ...state,
    components: { ...state.components, [draggableId]: { ...state.components[draggableId], droppableKey: destination.droppableId } },
    droppables: {
      ...state.droppables,
      [newStart.id]: newStart,
      [newFinish.id]: newFinish
    }
  };
  return newState;
};

const onChangeComponent = (state, action) => {
  const { componentId, key, value, withProps } = action.payload;

  if (withProps) {
    return {
      ...state,
      components: {
        ...state.components,
        [componentId]: { ...state.components[componentId], props: { ...state.components[componentId].props, [key]: value } }
      }
    };
  }
  return { ...state, components: { ...state.components, [componentId]: { ...state.components[componentId], [key]: value } } };
};

const onSetDesignerState = (state, action) => {
  const { droppables, ...rest } = action.payload;
  return { ...state, ...rest, droppables: { ...state.droppables, ...droppables } };
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Actions.ON_SET_DESIGNER_STATE:
      return onSetDesignerState(state, action);
    case Actions.ON_CHANGE_STYLES:
      return { ...state, styles: action.payload };
    case Actions.ON_ADD_COMPONENT:
      return onAddComponents(state, action);
    case Actions.ON_DRAG_END_COMPONENT:
      return onDragEnd(state, action);
    case Actions.ON_CHANGE_ATTRIBUTES:
      return onChangeComponent(state, action);
    case Actions.ON_EDIT_COMPONENT:
      return { ...state, editedComponent: action.payload };
    case Actions.ON_REMOVE_COMPONENT:
      return onRemoveComponent(state, action);
    case Actions.ON_TOGGLE_PREVIEW:
      return { ...state, showPreview: !state.showPreview };
    case Actions.ON_SAVE_TEMPLATE:
      return { ...state, html: action.payload };
    case Actions.RESET_DESIGNER:
      return INITIAL_STATE;
    default:
      return { ...state };
  }
};
