import { SET_FILTER, RESET_FILTER } from '../actions';

const INITIAL_STATE = {
  openModal: false,
  component: null,
  dateRange: {
    start: null,
    end: null
  }
};

const setFilter = (state, action) => {
  const {
    payload: { key, value }
  } = action;

  return { ...state, [key]: value };
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_FILTER:
      return setFilter(state, action);
    case RESET_FILTER:
      return INITIAL_STATE;
    default:
      return state;
  }
};
