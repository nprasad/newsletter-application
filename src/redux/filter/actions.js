import { SET_FILTER, RESET_FILTER } from '../actions';

export const setFilter = (payload) => ({ type: SET_FILTER, payload });
export const resetFilter = () => ({ type: RESET_FILTER });
