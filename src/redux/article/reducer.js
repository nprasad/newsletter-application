import * as Actions from '../actions';

const INITIAL_STATE = {
  articles: undefined,
  selectedArticleIds: [],
  articlesByFeed: {}
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Actions.FETCH_ARTICLES_SUCCESS:
      return { ...state, articles: action.payload };
    case Actions.FETCH_ARTICLES_BY_FEED_SUCCESS:
      return { ...state, articlesByFeed: { ...state.articlesByFeed, [action.payload.feed_nid]: action.payload.data } };
    case Actions.SELECT_ARTICLE:
      return { ...state, selectedArticleIds: [...state.selectedArticleIds, action.payload] };
    case Actions.RESET_ARTICLES:
      return INITIAL_STATE;
    default:
      return { ...state };
  }
};
