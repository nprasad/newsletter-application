import * as Actions from '../actions';

export const fetchArticles = (query, options = {}) => ({ type: Actions.FETCH_ARTICLES, payload: { query, options } });

export const fetchArticlesSuccess = (data) => ({
  type: Actions.FETCH_ARTICLES_SUCCESS,
  payload: data
});

export const fetchArticlesByFeedSuccess = (data) => ({
  type: Actions.FETCH_ARTICLES_BY_FEED_SUCCESS,
  payload: data
});

export const selectArticle = (id) => ({
  type: Actions.SELECT_ARTICLE,
  payload: id
});

export const resetArticles = () => ({ type: Actions.RESET_ARTICLES });
