import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import * as Actions from '../actions';
import Api from 'helpers/Api';
import { parseHTMLArticlesStr } from '../../helpers/Utils';

export function* watchFetchArticles() {
  yield takeEvery(Actions.FETCH_ARTICLES, fetchArticles);
}

function* fetchArticles(action, offset = 0, prevData = []) {
  try {
    yield put(Actions.setLoading('article', true));
    const { query, options } = action.payload;
    const { feed_nid, total } = options;
    const path = 'node/article';
    const pager = `${query ? '&' : '?'}page[offset]=${offset}&page[limit]=50&sort=-created`;
    const _query = query || '';
    const url = path + _query + pager;
    const response = yield Api.get(url);
    const responseData = yield response.json();
    yield put(Actions.setLoading('article', false));
    if (response.ok) {
      const {
        data,
        links,
        meta: { count }
      } = responseData;
      const hasNextPage = links.hasOwnProperty('next');
      const newData = [...prevData, ...parseHTMLArticlesStr(data)];

      const maxTotalItems = Number(total || count);
      if (hasNextPage && newData.length < maxTotalItems) yield call(fetchArticles, action, offset + data.length, newData);
      else {
        if (feed_nid) yield put(Actions.fetchArticlesByFeedSuccess({ data: newData, feed_nid }));
        else yield put(Actions.fetchArticlesSuccess(newData));
      }
    } else {
      const { code, error } = responseData;
      yield put(Actions.setError('article', { name: 'Fetch Error', code, error }));
    }
  } catch (error) {
    const { name, message, stack } = error;
    yield put(Actions.setLoading('article', false));
    yield put(Actions.setError('article', { name, originalMessage: message, code: 'error.problem', stack }));
  }
}

export default function* rootSaga() {
  yield all([fork(watchFetchArticles)]);
}
