import { all } from 'redux-saga/effects';
import taxonomySagas from './taxonomies/saga';
import articleSagas from './article/saga';
import feedSagas from './feed/saga';
import newsletterSagas from './newsletter/saga';
import subscriberSagas from './subscriber/saga';
import groupSagas from './group/saga';

export default function* rootSaga() {
  yield all([taxonomySagas(), articleSagas(), feedSagas(), newsletterSagas(), subscriberSagas(), groupSagas()]);
}
