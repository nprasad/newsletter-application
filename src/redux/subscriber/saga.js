import { all, fork, put, takeEvery } from 'redux-saga/effects';
import * as Actions from '../actions';
import Api from 'helpers/Api';
import { NotificationManager } from 'components/common/react-notifications';

export function* watchFetchSubscribers() {
  yield takeEvery(Actions.FETCH_SUBSCRIBERS, fetchSubscribers);
}

function* fetchSubscribers(action) {
  try {
    yield put(Actions.setLoading('subscriber', true));
    const query = (action && action.payload) || '';
    const url = `node/subscription${query}`;
    const response = yield Api.get(url);
    const responseData = yield response.json();
    if (response.ok) {
      const { data } = responseData;
      yield put(Actions.fetchSubscribersSuccess(data));
    } else {
      NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    }
    yield put(Actions.setLoading('subscriber', false));
  } catch (error) {
    NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    yield put(Actions.setLoading('subscriber', false));
  }
}

export function* watchPostSubscriber() {
  yield takeEvery(Actions.POST_SUBSCRIBER, postSubscriber);
}

function* postSubscriber({ payload }) {
  try {
    yield put(Actions.setLoading('subscriber', true));
    const url = 'node/subscription';
    const response = yield Api.post(url, payload);

    if (response.ok) {
      const responseData = yield response.json();
      const { data } = responseData;
      NotificationManager.success(true, 'Subscription Created', 3000, null, null, '');
      yield put(Actions.postSubscriberSuccess(data));
    } else {
      NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    }
    yield put(Actions.setLoading('subscriber', false));
  } catch (error) {
    NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    yield put(Actions.setLoading('subscriber', false));
  }
}

export function* watchEditSubscriber() {
  yield takeEvery(Actions.EDIT_SUBSCRIBER, editSubscriber);
}

function* editSubscriber({ payload }) {
  const { id, data } = payload;
  try {
    yield put(Actions.setLoading('subscriber', true));
    const url = 'node/subscription/' + id;
    const response = yield Api.update(url, { data });
    if (response.ok) {
      NotificationManager.success(true, 'Subscription Modified', 3000, null, null, '');
      // yield put(Actions.fetchSubscribers());
      // To handle update.
    } else {
      NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    }
    yield put(Actions.setLoading('subscriber', false));
  } catch (error) {
    NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    yield put(Actions.setLoading('subscriber', false));
  }
}

export function* watchDeleteSubscriber() {
  yield takeEvery(Actions.DELETE_SUBSCRIBER, deleteSubscriber);
}

function* deleteSubscriber({ payload }) {
  try {
    yield put(Actions.setLoading('subscriber', true));
    const url = 'node/subscription/' + payload;
    const { ok, status } = yield Api.delete(url);
    if (ok && status === 204) {
      NotificationManager.success(true, 'Subscription Deleted', 3000, null, null, '');
      yield put(Actions.fetchSubscribers());
    } else {
      NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    }
    yield put(Actions.setLoading('subscriber', false));
  } catch (error) {
    NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    yield put(Actions.setLoading('subscriber', false));
  }
}

export default function* rootSaga() {
  yield all([fork(watchFetchSubscribers), fork(watchPostSubscriber), fork(watchEditSubscriber), fork(watchDeleteSubscriber)]);
}
