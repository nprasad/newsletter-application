import * as Actions from '../actions';

const INITIAL_STATE = {
  subscribers: undefined
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Actions.FETCH_SUBSCRIBERS_SUCCESS:
      return { ...state, subscribers: action.payload };
    case Actions.POST_SUBSCRIBER_SUCCESS:
      return { ...state, subscribers: [...state.subscribers, action.payload] };
    default:
      return { ...state };
  }
};
