import * as Actions from '../actions';
export const fetchSubscribers = (query) => ({ type: Actions.FETCH_SUBSCRIBERS, payload: query });
export const fetchSubscribersSuccess = (data) => ({ type: Actions.FETCH_SUBSCRIBERS_SUCCESS, payload: data });
export const postSubscriber = (data) => ({ type: Actions.POST_SUBSCRIBER, payload: { data } });
export const postSubscriberSuccess = (data) => ({ type: Actions.POST_SUBSCRIBER_SUCCESS, payload: data });
export const editSubscriber = (data) => ({ type: Actions.EDIT_SUBSCRIBER, payload: data });
export const deleteSubscriber = (id) => ({ type: Actions.DELETE_SUBSCRIBER, payload: id });
