import { combineReducers } from 'redux';
import global from './global/reducer';
import settings from './settings/reducer';
import menu from './menu/reducer';
import filter from './filter/reducer';
import taxonomy from './taxonomies/reducer';
import article from './article/reducer';
import feed from './feed/reducer';
import newsletter from './newsletter/reducer';
import subscriber from './subscriber/reducer';
import designer from './designer/reducer';
import keycloak from './keycloak/reducer';
import group from './group/reducer';

const reducers = combineReducers({
  global,
  settings,
  menu,
  filter,
  taxonomy,
  article,
  feed,
  newsletter,
  subscriber,
  designer,
  keycloak,
  group
});

export default reducers;
