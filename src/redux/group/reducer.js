import * as Actions from '../actions';
const INITIAL_STATE = { loading: false, identity: undefined, groups: undefined, isAdmin: false };
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Actions.FETCH_IDENTITY:
    case Actions.FETCH_GROUPS:
      return { ...state, loading: true };
    case Actions.FETCH_IDENTITY_SUCCESS:
      return { ...state, identity: action.payload, loading: false };
    case Actions.FETCH_GROUPS_SUCCESS:
      return { ...state, groups: action.payload, loading: false };
    case Actions.SET_GROUPS_IS_ADMIN:
      return { ...state, isAdmin: action.payload, loading: false };
    default:
      return { ...state };
  }
};
