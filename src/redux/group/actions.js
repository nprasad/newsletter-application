import * as Actions from '../actions';

export const fetchIdentity = () => ({ type: Actions.FETCH_IDENTITY });
export const fetchIdentitySuccess = (data) => ({ type: Actions.FETCH_IDENTITY_SUCCESS, payload: data });
export const fetchGroups = () => ({ type: Actions.FETCH_GROUPS });
export const fetchGroupsSuccess = (data) => ({ type: Actions.FETCH_GROUPS_SUCCESS, payload: data });
export const setGroupIsAdmin = (data) => ({ type: Actions.SET_GROUPS_IS_ADMIN, payload: data });
