import { all, fork, put, select, takeEvery } from 'redux-saga/effects';
import * as Actions from '../actions';
import httpService from 'helpers/HttpService';
import { keycloakExchangeToken, keycloakCernUpn } from 'redux/keycloak/selectors';
import { authServiceApi } from 'constants/config';
import { selectUserIdentity } from './selectors';
import { NotificationManager } from 'components/common/react-notifications';

export function* watchFetchGroups() {
  yield takeEvery(Actions.FETCH_GROUPS, fetchGroups);
}

function* fetchGroups() {
  try {
    yield put(Actions.setLoading('group', true));
    const token = yield select(keycloakExchangeToken);
    const identity = yield select(selectUserIdentity);
    const url = `${authServiceApi}Identity/${identity.id}/groups`;
    const response = yield httpService.get(url, token);
    const responseData = yield response.json();
    if (response.ok) {
      const { data } = responseData;
      const groups = data
        .filter(({ groupIdentifier }) => groupIdentifier.startsWith('newsletter-service-'))
        .map(({ groupIdentifier }) => groupIdentifier);
      const adminGroups = data
        .filter(({ groupIdentifier }) => ['web-team-partners', 'web-team-developers'].includes(groupIdentifier))
        .map(({ groupIdentifier }) => groupIdentifier);
      yield put(Actions.setGroupIsAdmin(!!adminGroups.length));
      yield put(Actions.fetchGroupsSuccess(groups));
    } else {
      NotificationManager.error(true, 'Fetch Error', 3000, null, null, '');
    }
    yield put(Actions.setLoading('group', false));
  } catch (error) {
    const { message } = error;
    yield put(Actions.setLoading('group', false));
    NotificationManager.error(true, message, 3000, null, null, '');
  }
}

export function* watchFetchIdentity() {
  yield takeEvery(Actions.FETCH_IDENTITY, fetchIdentity);
}

function* fetchIdentity() {
  try {
    yield put(Actions.setLoading('group', true));
    const upn = yield select(keycloakCernUpn);
    const token = yield select(keycloakExchangeToken);
    const url = `${authServiceApi}Identity`;
    const response = yield httpService.get(url, token, { filter: `upn:${upn}` });
    const responseData = yield response.json();
    if (response.ok) {
      const {
        data: [identity]
      } = responseData;
      yield put(Actions.fetchIdentitySuccess(identity));
      yield put(Actions.fetchGroups());
    } else {
      NotificationManager.error(true, 'Fetch Error', 3000, null, null, '');
    }
    yield put(Actions.setLoading('group', false));
  } catch (error) {
    const { message } = error;
    NotificationManager.error(true, message, 3000, null, null, '');
    yield put(Actions.setLoading('group', false));
  }
}

export default function* rootSaga() {
  yield all([fork(watchFetchGroups), fork(watchFetchIdentity)]);
}
