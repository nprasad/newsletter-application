export const getGroupState = (state) => state.group;
export const selectUserIdentity = (state) => state.group.identity;
