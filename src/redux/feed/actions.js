import * as Actions from '../actions';

export const fetchFeeds = (query) => ({ type: Actions.FETCH_FEEDS, payload: { query } });
export const fetchFeedsSuccess = (data) => ({
  type: Actions.FETCH_FEEDS_SUCCESS,
  payload: data
});
export const postFeed = (data) => ({ type: Actions.POST_FEED, payload: { data } });
export const editFeed = (data) => ({ type: Actions.EDIT_FEED, payload: data });
export const deleteFeed = (id) => ({ type: Actions.DELETE_FEED, payload: id });
