import * as Actions from '../actions';

const INITIAL_STATE = { feeds: undefined };
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Actions.FETCH_FEEDS_SUCCESS:
      return { ...state, feeds: action.payload };
    default:
      return { ...state };
  }
};
