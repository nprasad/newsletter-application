/* eslint-disable no-prototype-builtins */
import { all, call, fork, put, select, takeEvery } from 'redux-saga/effects';
import * as Actions from '../actions';
import Api from 'helpers/Api';
import { NotificationManager } from 'components/common/react-notifications';
import { getGroupState } from 'redux/group/selectors';

export function* watchFetchFeeds() {
  yield takeEvery(Actions.FETCH_FEEDS, fetchFeeds);
}

function* fetchFeeds(action, offset = 0, prevData = []) {
  try {
    yield put(Actions.setLoading('feed', true));
    const { groups, isAdmin } = yield select(getGroupState);
    const path = 'node/feed';
    const pager = `?page[offset]=${offset}&page[limit]=50`;
    const filter =
      !isAdmin && groups && groups.length
        ? `?filter[field_admin_group][operator]=IN${groups.map((group) => `&filter[field_admin_group][value][]=${group}`).join('')}`
        : '';
    const url = path + pager + filter;
    const response = yield Api.get(url);
    const responseData = yield response.json();
    if (response.ok) {
      const { data, links } = responseData;
      const hasNextPage = links.hasOwnProperty('next');
      if (hasNextPage) yield call(fetchFeeds, action, offset + data.length, [...data]);
      else yield put(Actions.fetchFeedsSuccess([...prevData, ...data]));

      // yield put(Actions.fetchFeedsSuccess(data));
      yield put(Actions.setLoading('feed', false));
    } else {
      NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
      yield put(Actions.setLoading('feed', false));
    }
  } catch (error) {
    NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    yield put(Actions.setLoading('feed', false));
  }
}

export function* watchPostFeed() {
  yield takeEvery(Actions.POST_FEED, postFeed);
}

function* postFeed({ payload }) {
  try {
    yield put(Actions.setLoading('feed', true));
    const url = 'node/feed';
    const response = yield Api.post(url, payload);

    if (response.ok) {
      NotificationManager.success(true, 'Feed Created', 3000, null, null, '');
      yield put(Actions.setLoading('feed', false));
      yield put(Actions.fetchFeeds());
      yield fetch('https://cnpdfs.web.cern.ch/cron/r5FNZmYsAbrIREmRmdJHAPX58lJkw50z68vFMwjQ7oDEzjhvpIpqusSB-tTm8hS2jNxttZYg2g');
    } else {
      NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
      yield put(Actions.setLoading('feed', false));
    }
  } catch (error) {
    NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    yield put(Actions.setLoading('feed', false));
  }
}

export function* watchEditFeed() {
  yield takeEvery(Actions.EDIT_FEED, editFeed);
}

function* editFeed({ payload }) {
  try {
    yield put(Actions.setLoading('feed', true));
    const { id, data } = payload;
    const url = 'node/feed/' + id;
    const response = yield Api.update(url, { data });

    if (response.ok) {
      NotificationManager.success(true, 'Feed Modified', 3000, null, null, '');
      yield put(Actions.setLoading('feed', false));

      yield put(Actions.fetchFeeds());
    } else {
      NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
      yield put(Actions.setLoading('feed', false));
    }
  } catch (error) {
    NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    yield put(Actions.setLoading('feed', false));
  }
}

export function* watchDeleteFeed() {
  yield takeEvery(Actions.DELETE_FEED, deleteFeed);
}

function* deleteFeed({ payload }) {
  try {
    yield put(Actions.setLoading('feed', true));
    const url = 'node/feed/' + payload;
    const { ok, status } = yield Api.delete(url);
    if (ok && status === 204) {
      NotificationManager.success(true, 'Feed Deleted', 3000, null, null, '');
      yield put(Actions.setLoading('feed', false));

      yield put(Actions.fetchFeeds());
    } else {
      NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
      yield put(Actions.setLoading('feed', false));
    }
  } catch (error) {
    NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    yield put(Actions.setLoading('feed', false));
  }
  yield put(Actions.setLoading('feed', false));
}

export default function* rootSaga() {
  yield all([fork(watchFetchFeeds), fork(watchPostFeed), fork(watchEditFeed), fork(watchDeleteFeed)]);
}
