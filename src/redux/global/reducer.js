import { SET_LOADING, SET_ERROR } from '../actions';

const INITIAL_STATE = {
  loading: null,
  error: null
};

const setLoading = (state, { payload }) => {
  const { reducer, loading } = payload;
  return { ...state, loading: { ...state.loading, [reducer]: loading } };
};
const setError = (state, { payload }) => {
  const { reducer, error } = payload;
  return { ...state, error: { ...state.error, [reducer]: error } };
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_LOADING:
      return setLoading(state, action);
    case SET_ERROR:
      return setError(state, action);
    default:
      return state;
  }
};
