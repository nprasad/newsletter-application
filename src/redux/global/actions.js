import * as Actions from '../actions';

export const setLoading = (reducer, loading) => ({ type: Actions.SET_LOADING, payload: { reducer, loading } });
export const setError = (reducer, error) => ({ type: Actions.SET_ERROR, payload: { reducer, error } });
