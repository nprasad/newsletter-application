import * as Actions from '../actions';

export const setKeycloakReady = () => ({ type: Actions.SET_KEYCLOAK_READY });
export const setKeycloakReset = () => ({ type: Actions.SET_KEYCLOAK_READY });
export const setKeycloakAuthenticated = (authenticated) => ({
  type: Actions.SET_KEYCLOAK_AUTHENTICATED,
  payload: authenticated
});
export const setKeycloak = (keycloak) => ({
  type: Actions.SET_KEYCLOAK,
  payload: keycloak
});
export const setUserToken = (token) => ({
  type: Actions.SET_USER_TOKEN,
  payload: token
});
export const setExchangeToken = (token) => ({
  type: Actions.SET_EXCHANGE_TOKEN,
  payload: token
});
export const setKeycloakUserName = (userName) => ({
  type: Actions.SET_KEYCLOAK_USER_NAME,
  payload: userName
});
export const setKeycloakCernUpn = (upn) => ({
  type: Actions.SET_KEYCLOAK_CERN_UPN,
  payload: upn
});
export const setKeycloakRoles = (roles) => ({
  type: Actions.SET_KEYCLOAK_ROLES,
  payload: roles
});
