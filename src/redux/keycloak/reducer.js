import * as Actions from '../actions';

const INITIAL_STATE = {
  ready: false,
  authenticated: false,
  keycloak: null,
  userToken: null,
  exchangeToken: null,
  upn: null,
  userName: 'cern user',
  roles: []
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Actions.SET_KEYCLOAK_READY:
      return {
        ...state,
        ready: true
      };
    case Actions.SET_KEYCLOAK_AUTHENTICATED:
      return {
        ...state,
        authenticated: action.payload
      };
    case Actions.SET_KEYCLOAK:
      return {
        ...state,
        keycloak: action.payload
      };
    case Actions.SET_USER_TOKEN:
      return {
        ...state,
        userToken: action.payload
      };
    case Actions.SET_EXCHANGE_TOKEN:
      return {
        ...state,
        exchangeToken: action.payload
      };
    case Actions.SET_KEYCLOAK_USER_NAME:
      return {
        ...state,
        userName: action.payload
      };
    case Actions.SET_KEYCLOAK_CERN_UPN:
      return {
        ...state,
        upn: action.payload
      };
    case Actions.SET_KEYCLOAK_ROLES:
      return {
        ...state,
        roles: action.payload
      };
    case Actions.SET_KEYCLOAK_RESET:
    default:
      return state;
  }
};
