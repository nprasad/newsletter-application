export const keycloakAuthenticated = (state) => state.keycloak.authenticated;
export const keycloakUserToken = (state) => state.keycloak.userToken;
export const keycloakExchangeToken = (state) => state.keycloak.exchangeToken;
export const keycloakInstance = (state) => state.keycloak.keycloak;
export const keycloakUserName = (state) => state.keycloak.userName;
export const keycloakCernUpn = (state) => state.keycloak.upn;
export const keycloakRoles = (state) => state.keycloak.roles;
