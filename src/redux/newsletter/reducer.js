import * as Actions from '../actions';

const INITIAL_STATE = {
  newsletters: undefined,
  totalNewsletters: 0,
  newsletter: undefined,
  newNewsletter: undefined,
  newsletterInstances: undefined,
  instances: undefined,
  instance: undefined
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Actions.FETCH_NEWSLETTERS_SUCCESS:
      return { ...state, newsletters: action.payload.data, totalNewsletters: action.payload.count };
    case Actions.FETCH_INSTANCES_SUCCESS:
      return { ...state, instances: action.payload };
    case Actions.FETCH_INSTANCE_SUCCESS:
    case Actions.SELECT_INSTANCE:
      return { ...state, instance: action.payload };
    case Actions.FETCH_NEWSLETTER_SUCCESS:
    case Actions.SELECT_NEWSLETTER:
      return { ...state, newsletter: action.payload };
    case Actions.POST_NEWSLETTER:
      return { ...state, newNewsletter: null };
    case Actions.POST_NEWSLETTER_SUCCESS:
      return { ...state, newsletters: [...state.newsletters, action.payload], newNewsletter: action.payload };
    case Actions.FETCH_NEWSLETTER_INSTANCES_SUCCESS:
      return { ...state, newsletterInstances: action.payload };
    case Actions.RESET_NEWSLETTER:
      return INITIAL_STATE;
    default:
      return { ...state };
  }
};
