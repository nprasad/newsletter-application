import * as Actions from '../actions';

export const fetchNewsletters = () => ({ type: Actions.FETCH_NEWSLETTERS });
export const fetchNewslettersByCategory = (category) => ({ type: Actions.FETCH_NEWSLETTERS_BY_CATEGORY, payload: category });
export const fetchNewslettersSuccess = (data) => ({ type: Actions.FETCH_NEWSLETTERS_SUCCESS, payload: data });
export const fetchNewsletter = (id) => ({ type: Actions.FETCH_NEWSLETTER, payload: id });
export const fetchNewsletterSuccess = (data) => ({ type: Actions.FETCH_NEWSLETTER_SUCCESS, payload: data });
export const fetchNewsletterInstances = (id) => ({ type: Actions.FETCH_NEWSLETTER_INSTANCES, payload: id });
export const fetchNewsletterInstancesSuccess = (data) => ({ type: Actions.FETCH_NEWSLETTER_INSTANCES_SUCCESS, payload: data });
export const fetchInstances = (query) => ({ type: Actions.FETCH_INSTANCES, payload: query });
export const fetchInstancesSuccess = (data) => ({ type: Actions.FETCH_INSTANCES_SUCCESS, payload: data });
export const fetchInstance = (id) => ({ type: Actions.FETCH_INSTANCE, payload: id });
export const fetchInstanceSuccess = (data) => ({ type: Actions.FETCH_INSTANCE_SUCCESS, payload: data });
export const selectNewsletter = (data) => ({ type: Actions.SELECT_NEWSLETTER, payload: data });
export const selectInstance = (data) => ({ type: Actions.SELECT_INSTANCE, payload: data });
export const postNewsletter = (data) => ({ type: Actions.POST_NEWSLETTER, payload: { data } });
export const postNewsletterSuccess = (data) => ({ type: Actions.POST_NEWSLETTER_SUCCESS, payload: data });
export const editNewsletter = (data) => ({ type: Actions.EDIT_NEWSLETTER, payload: data });
export const editInstance = (data) => ({ type: Actions.EDIT_INSTANCE, payload: data });
export const deleteInstance = (id) => ({ type: Actions.DELETE_INSTANCE, payload: id });
export const deleteNewsletter = (id) => ({ type: Actions.DELETE_NEWSLETTER, payload: id });
export const archiveNewsletter = (id, value) => ({ type: Actions.ARCHIVE_NEWSLETTER, payload: { id, value } });
export const postInstance = (data) => ({ type: Actions.POST_INSTANCE, payload: { data } });

export const resetNewsletter = () => ({ type: Actions.RESET_NEWSLETTER });
