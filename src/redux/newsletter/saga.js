import { all, fork, put, select, takeEvery, call } from 'redux-saga/effects';
import * as Actions from '../actions';
import Api from 'helpers/Api';
import { NotificationManager } from 'components/common/react-notifications';
import { getGroupState } from 'redux/group/selectors';

export function* watchFetchNewsletters() {
  yield takeEvery(Actions.FETCH_NEWSLETTERS, fetchNewsletters);
}

function* fetchNewsletters(action, offset = 0, prevData = []) {
  try {
    yield put(Actions.setLoading('newsletter', true));
    const { groups, isAdmin } = yield select(getGroupState);
    const path = 'node/newsletter';
    const pager = `?page[offset]=${offset}&page[limit]=50`;
    const filter =
      !isAdmin && groups && groups.length
        ? `?filter[field_admin_group][operator]=IN${groups.map((group) => `&filter[field_admin_group][value][]=${group}`).join('')}`
        : '';
    // If no groups dont fetch
    const url = path + pager + filter;
    const response = yield Api.get(url);
    const responseData = yield response.json();
    if (response.ok) {
      const { data, links, meta } = responseData;
      const count = meta ? meta.count : 0;
      const hasNextPage = links.hasOwnProperty('next');
      if (hasNextPage) yield call(fetchNewsletters, action, offset + data.length, [...data]);
      else yield put(Actions.fetchNewslettersSuccess({ data: [...prevData, ...data], count }));
    } else {
      NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    }
    yield put(Actions.setLoading('newsletter', false));
  } catch (error) {
    NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    yield put(Actions.setLoading('newsletter', false));
  }
}

export function* watchFetchNewslettersByCategory() {
  yield takeEvery(Actions.FETCH_NEWSLETTERS_BY_CATEGORY, fetchNewslettersByCategory);
}

function* fetchNewslettersByCategory(action) {
  try {
    yield put(Actions.setLoading('newsletter', true));
    const categoryId = action.payload;
    const url = `node/newsletter?filter[field_categories.id][value]=${categoryId}`;
    const response = yield Api.get(url);
    const responseData = yield response.json();
    if (response.ok) {
      const { data } = responseData;
      yield put(Actions.fetchNewslettersSuccess(data));
    } else {
      NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    }
    yield put(Actions.setLoading('newsletter', false));
  } catch (error) {
    NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    yield put(Actions.setLoading('newsletter', false));
  }
}

export function* watchFetchNewsletter() {
  yield takeEvery(Actions.FETCH_NEWSLETTER, fetchNewsletter);
}

function* fetchNewsletter({ payload: id }) {
  try {
    yield put(Actions.setLoading('newsletter', true));
    const url = 'node/newsletter/' + id;
    const response = yield Api.get(url);
    const responseData = yield response.json();
    if (response.ok) {
      const { data } = responseData;
      yield put(Actions.fetchNewsletterSuccess(data));
    } else {
      NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    }
    yield put(Actions.setLoading('newsletter', false));
  } catch (error) {
    NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    yield put(Actions.setLoading('newsletter', false));
  }
}
export function* watchFetchInstance() {
  yield takeEvery(Actions.FETCH_INSTANCE, fetchInstance);
}

function* fetchInstance({ payload: id }) {
  try {
    yield put(Actions.setLoading('newsletter', true));
    const url = 'node/instance/' + id;
    const response = yield Api.get(url);
    const responseData = yield response.json();
    if (response.ok) {
      const { data } = responseData;
      yield put(Actions.fetchInstanceSuccess(data));
    } else {
      NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    }
    yield put(Actions.setLoading('newsletter', false));
  } catch (error) {
    NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    yield put(Actions.setLoading('newsletter', false));
  }
}
export function* watchFetchInstances() {
  yield takeEvery(Actions.FETCH_INSTANCES, fetchInstances);
}

function* fetchInstances(action) {
  try {
    yield put(Actions.setLoading('newsletter', true));
    const query = action && action.payload;
    const url = `node/instance${query ? query : ''}`;
    const response = yield Api.get(url);
    const responseData = yield response.json();
    if (response.ok) {
      const { data } = responseData;
      yield put(Actions.fetchInstancesSuccess(data));
    } else {
      NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    }
    yield put(Actions.setLoading('newsletter', false));
  } catch (error) {
    NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    yield put(Actions.setLoading('newsletter', false));
  }
}

export function* watchFetchNewsletterInstances() {
  yield takeEvery(Actions.FETCH_NEWSLETTER_INSTANCES, fetchNewsletterInstances);
}

function* fetchNewsletterInstances({ payload: id }) {
  try {
    yield put(Actions.setLoading('newsletter', true));
    const url = 'node/instance?filter[field_newsletter.id]=' + id + '&include=field_newsletter';
    const response = yield Api.get(url);
    const responseData = yield response.json();
    if (response.ok) {
      const { data } = responseData;
      yield put(Actions.fetchNewsletterInstancesSuccess(data));
    } else {
      NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    }
    yield put(Actions.setLoading('newsletter', false));
  } catch (error) {
    yield put(Actions.setLoading('newsletter', false));
    NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
  }
}

export function* watchPostNewsletter() {
  yield takeEvery(Actions.POST_NEWSLETTER, postNewsletter);
}

function* postNewsletter({ payload }) {
  try {
    yield put(Actions.setLoading('newsletter', true));
    const url = 'node/newsletter';
    const response = yield Api.post(url, payload);
    const responseData = yield response.json();
    if (response.ok) {
      const { data } = responseData;
      const { title } = data.attributes;
      NotificationManager.success(true, `Newsletter ${title} Created`, 3000, null, null, '');
      yield put(Actions.postNewsletterSuccess(data));
    } else {
      NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    }
    yield put(Actions.setLoading('newsletter', false));
  } catch (error) {
    NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    yield put(Actions.setLoading('newsletter', false));
  }
}

export function* watchEditNewsletter() {
  yield takeEvery(Actions.EDIT_NEWSLETTER, editNewsletter);
}

function* editNewsletter({ payload }) {
  try {
    yield put(Actions.setLoading('newsletter', true));
    const { data } = payload;
    const id = data.id;
    if (!id) throw new Error('Newsletter Id Not Found');
    const url = 'node/newsletter/' + id;
    const response = yield Api.update(url, { data });
    if (response.ok) {
      NotificationManager.success(true, 'Newsletter Modified', 3000, null, null, '');
      yield put(Actions.fetchNewsletters());
    } else {
      NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    }
    yield put(Actions.setLoading('newsletter', false));
  } catch (error) {
    NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    yield put(Actions.setLoading('newsletter', false));
  }
}

export function* watchDeleteNewsletter() {
  yield takeEvery(Actions.DELETE_NEWSLETTER, deleteNewsletter);
}

function* deleteNewsletter({ payload }) {
  try {
    yield put(Actions.setLoading('newsletter', true));
    const url = 'node/newsletter/' + payload;
    const { ok, status } = yield Api.delete(url);
    if (ok && status === 204) {
      NotificationManager.success(true, 'Newsletter Deleted', 3000, null, null, '');
      yield put(Actions.fetchNewsletters());
    } else {
      NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    }
    yield put(Actions.setLoading('newsletter', false));
  } catch (error) {
    NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    yield put(Actions.setLoading('newsletter', false));
  }
}
export function* watchArchiveNewsletter() {
  yield takeEvery(Actions.ARCHIVE_NEWSLETTER, archiveNewsletter);
}

function* archiveNewsletter({ payload: { id, value } }) {
  try {
    yield put(Actions.setLoading('newsletter', true));
    const data = {
      type: 'node--newsletter',
      id,
      attributes: { field_archive: value }
    };
    if (!id) throw new Error('Newsletter Id Not Found');
    const url = 'node/newsletter/' + id;
    const response = yield Api.update(url, { data });
    if (response.ok) {
      NotificationManager.success(true, 'Newsletter Modified', 3000, null, null, '');
      yield put(Actions.fetchNewsletters());
    } else {
      NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    }
    yield put(Actions.setLoading('newsletter', false));
  } catch (error) {
    NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    yield put(Actions.setLoading('newsletter', false));
  }
}

export function* watchPostInstance() {
  yield takeEvery(Actions.POST_INSTANCE, postInstance);
}

function* postInstance({ payload }) {
  try {
    yield put(Actions.setLoading('newsletter', true));
    const url = 'node/instance';
    const response = yield Api.post(url, payload);
    const responseData = yield response.json();

    if (response.ok) {
      const { data } = responseData;
      yield put(Actions.selectInstance(data));
      NotificationManager.success(true, 'Issue Created', 3000, null, null, '');
    } else {
      NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    }
    yield put(Actions.setLoading('newsletter', false));
  } catch (error) {
    NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    yield put(Actions.setLoading('newsletter', false));
  }
}

export function* watchEditInstance() {
  yield takeEvery(Actions.EDIT_INSTANCE, editInstance);
}

function* editInstance({ payload }) {
  try {
    const { id, data } = payload;
    yield put(Actions.setLoading('newsletter', true));
    const url = 'node/instance/' + id;
    const response = yield Api.update(url, { data });
    if (response.ok) NotificationManager.success(true, 'Issue Saved', 3000, null, null, '');
    else NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    yield put(Actions.setLoading('newsletter', false));
  } catch (error) {
    yield put(Actions.setLoading('newsletter', false));
    NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
  }
}

export function* watchDeleteInstance() {
  yield takeEvery(Actions.DELETE_INSTANCE, deleteInstance);
}

function* deleteInstance({ payload }) {
  try {
    yield put(Actions.setLoading('newsletter', true));
    const url = 'node/instance/' + payload;
    const { ok, status } = yield Api.delete(url);
    if (ok && status === 204) {
      NotificationManager.success(true, 'Issue Deleted', 3000, null, null, '');
    } else {
      NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    }
    yield put(Actions.setLoading('newsletter', false));
  } catch (error) {
    NotificationManager.error(true, 'Something Went Wrong', 3000, null, null, '');
    yield put(Actions.setLoading('newsletter', false));
  }
}

export default function* rootSaga() {
  yield all([
    fork(watchFetchNewsletters),
    fork(watchFetchNewslettersByCategory),
    fork(watchFetchNewsletter),
    fork(watchFetchInstances),
    fork(watchFetchInstance),
    fork(watchFetchNewsletterInstances),
    fork(watchPostNewsletter),
    fork(watchEditNewsletter),
    fork(watchEditInstance),
    fork(watchDeleteNewsletter),
    fork(watchArchiveNewsletter),
    fork(watchPostInstance),
    fork(watchDeleteInstance)
  ]);
}
