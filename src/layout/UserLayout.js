import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

const UserLayout = ({ children }) => {
  useEffect(() => {
    document.body.classList.add('background');
    document.body.classList.add('no-footer');

    return () => {
      document.body.classList.remove('background');
      document.body.classList.remove('no-footer');
    };
  }, []);

  return (
    <>
      <div className="fixed-background" />
      <main>
        <div className="container">{children}</div>
      </main>
    </>
  );
};

UserLayout.propTypes = {
  children: PropTypes.element
};

export default UserLayout;
