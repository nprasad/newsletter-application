const data = [
  {
    id: 'dashboards',
    icon: 'iconsminds-shop-4',
    label: 'menu.dashboards',
    to: '/dashboards/analytics'
  },
  {
    id: 'feeds',
    icon: 'simple-icon-book-open',
    label: 'menu.feeds',
    to: '/feeds/list'
  },
  {
    id: 'taxonomies',
    icon: 'iconsminds-newspaper',
    label: 'menu.taxonomies',
    to: '/taxonomies',
    subs: [
      {
        icon: 'simple-icon-note',
        label: 'menu.categories',
        to: '/taxonomies/categories'
      },
      {
        icon: 'simple-icon-doc',
        label: 'menu.topics',
        to: '/taxonomies/topics'
      }
    ]
  },
  {
    id: 'newsletter',
    icon: 'iconsminds-library',
    label: 'menu.newsletter',
    to: '/newsletter',
    subs: [
      {
        icon: 'simple-icon-list',
        label: 'menu.newsletter-list',
        to: '/newsletter/list'
      },
      {
        icon: 'iconsminds-add',
        label: 'menu.newsletter-add',
        to: '/newsletter/add'
      },
      {
        icon: 'iconsminds-add',
        label: 'menu.newsletter-add-issue',
        to: '/newsletter/issue-add'
      }
    ]
  },
  {
    id: 'subscribers',
    icon: 'iconsminds-network',
    label: 'menu.subscribers',
    to: '/subscribers/list'
  },
  {
    id: 'designer',
    icon: 'iconsminds-mail-photo',
    label: 'menu.email-designer',
    to: '/designer'
  },
  {
    id: 'widget',
    icon: 'iconsminds-plug-in',
    label: 'menu.widget',
    to: '/widget'
  }
];
export default data;
