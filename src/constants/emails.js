export const LOCALE_STORAGE_KEYS = {
  LANG: 'LANG'
}

export const NEWSLETTER = {
  TYPE_1: 'TYPE 1',
  TYPE_2: 'TYPE 2',
  TYPE_3: 'TYPE 3',
}
