import React from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';

const AuthRoute = ({ component: Component, authenticated, ...rest }) => (
  <Route {...rest} render={(props) => (authenticated ? <Component {...props} /> : <div className="loading" />)} />
);

AuthRoute.propTypes = {
  component: PropTypes.object,
  authenticated: PropTypes.bool
};

export default AuthRoute;
