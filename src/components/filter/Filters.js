import React from 'react';
// import PropTypes from 'prop-types';
import { Row } from 'reactstrap';
import DatePicker from 'react-datepicker';
import { Colxx } from 'components/common/CustomBootstrap';
import 'react-datepicker/dist/react-datepicker.css';
import { useDispatch, useSelector } from 'react-redux';
import { setFilter } from 'redux/actions';

function Filters() {
  const dispatch = useDispatch();
  const { dateRange } = useSelector((state) => state.filter);

  const startDateRange = dateRange ? dateRange.start : null;
  const endDateRange = dateRange ? dateRange.end : null;
  const setStartDateRange = (date) => dispatch(setFilter({ key: 'dateRange', value: { start: date, end: endDateRange } }));
  const setEndDateRange = (date) => dispatch(setFilter({ key: 'dateRange', value: { start: startDateRange, end: date } }));
  return (
    <>
      <label>Date Range</label>
      <Row className="mb-5">
        <Colxx xxs="6">
          <DatePicker
            selected={startDateRange}
            selectsStart
            startDate={startDateRange}
            endDate={endDateRange}
            onChange={setStartDateRange}
            placeholderText="Start"
          />
        </Colxx>
        <Colxx xxs="6">
          <DatePicker
            selected={endDateRange}
            selectsEnd
            startDate={startDateRange}
            endDate={endDateRange}
            onChange={setEndDateRange}
            placeholderText="End"
          />
        </Colxx>
      </Row>
    </>
  );
}

// Filters.propTypes = {};

export default Filters;
