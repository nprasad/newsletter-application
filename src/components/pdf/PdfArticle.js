import React from 'react';
import PropTypes from 'prop-types';
import { addIdToAnchorTag } from '../../helpers/Utils';

const PdfArticle = ({ article, isMultiColumn, styles: { color }, lang }) => {
  const {
    id,
    attributes: { title, field_strap, field_body, field_img_thumb, field_image_caption, field_byline }
  } = article;
  const image = field_img_thumb;
  const imageCaption = field_image_caption || '';
  const strap = field_strap ? field_strap.value : '';
  const body = addIdToAnchorTag(field_body ? field_body.value : '');
  const byline = field_byline ? field_byline : '';
  const columnStyles = { ...(isMultiColumn ? { columnCount: 3, columnGap: 15 } : {}) };

  return (
    <article id={id} style={{ minHeight: 200, marginTop: 30 }}>
      <h2 style={{ fontSize: 25, fontWeight: 800 }}>{title}</h2>
      {strap ?
      <div style={{ fontSize: 16, marginTop: 15, marginBottom: 15, fontWeight: 800 }}>{strap}</div> :
          <div style={{ fontSize: 16, marginTop: 15, marginBottom: 15, fontWeight: 800 }}>{strap}</div>
      }
      <div
        style={{
          ...columnStyles,
          fontSize: 12,
          paddingBottom: 40,
          marginBottom: 40,
          borderBottomWidth: 1,
          borderBottomColor: color || '#0055a8',
          borderBottomStyle: 'solid',
          textAlign: 'justify',
          wordWrap: 'break-word',
          hyphens: 'auto'
        }}
      >
        {image && (
          <>
            <img style={{ maxWidth: '100%' }} src={image} alt={imageCaption} />
            <div
              className="text-center d-block p-0 mb-2"
              style={{ fontSize: 10, fontStyle: 'italic', textAlign: 'center', marginTop: 15, marginBottom: 25 }}
            >
              {imageCaption}
            </div>
          </>
        )}
        <div
          dangerouslySetInnerHTML={{
            __html: body
          }}
        />
          <div style={{ fontSize: 12,  marginTop: 10, marginBottom: 10, textAlign: 'right', fontStyle: 'italic' }}>{byline}</div>
      </div>
    </article>
  );
};
PdfArticle.propTypes = { lang: PropTypes.string, article: PropTypes.object, isMultiColumn: PropTypes.bool, styles: PropTypes.object };
PdfArticle.defaultProps = { lang: 'en', article: null };

export default PdfArticle;
