import React from 'react';
import PropTypes from 'prop-types';

const css = `
a {
  color: #000;
  text-decoration: none;
}
p {
  margin-top: 0;
}
img {
  max-width: 100%;
}
figure {
  margin: 10px;
}
.hrefClass:after 
 {
  content: ' ('attr(href)')';
  font-size: 12px;
  line-break: anywhere;
}
@media print {
  ul {
    padding: 0px;
  }
}
`.trim();

export default function PdfHtml(props) {
  return (
    <html lang={props.lang} xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>{props.title}</title>
        <style>{css}</style>
      </head>
      <body
        style={{
          width: props.width,
          paddingLeft: 30,
          paddingRight: 30,
          fontFamily: props.styles.fontFamily
        }}
      >
        {props.children}
      </body>
    </html>
  );
}

PdfHtml.propTypes = {
  lang: PropTypes.string,
  title: PropTypes.string,
  styles: PropTypes.any,
  width: PropTypes.string,
  children: PropTypes.node
};

PdfHtml.defaultProps = {
  lang: 'en',
  width: '800px',
  styles: undefined,
  children: undefined,
  title: 'CERN Newsletter Pdf'
};
