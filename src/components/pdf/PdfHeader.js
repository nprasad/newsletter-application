import React from 'react';
import PropTypes from 'prop-types';

const styles = {
  container: {
    display: 'flex',
    borderBottomWidth: 1,
    borderBottomColor: '#0055a8',
    borderBottomStyle: 'solid',
    alignItems: 'stretch',
    marginBottom: 20
  },
  titleColumn: { flex: 2},
  infoColumn: {
    flex: 1,
    alignSelf: 'center'
  },
  title: {
    fontSize: 62,
    color: '#0055a8',
    fontWeight: 'bold',
    margin: 0,
    padding: 0
  },
  info: {
    fontSize: 11,
    color: '#0055a8',
    lineHeight: 0.6
  }
};

const PdfHeader = ({ title, info, styles: { color } }) => {
  return (
    <section id="header">
      <div style={{ ...styles.container, borderBottomColor: color }}>
        <div style={styles.titleColumn}>
          <h2 style={{ ...styles.title, color }}>{title}</h2>
        </div>
        <div style={styles.infoColumn}>
          {info.map((info, index) => (
            <p key={`info-${index}`} style={{ ...styles.info, color }}>
              {info}
            </p>
          ))}
        </div>
      </div>
    </section>
  );
};

PdfHeader.propTypes = { title: PropTypes.string, info: PropTypes.arrayOf(PropTypes.string), styles: PropTypes.object };
PdfHeader.defaultProps = {
  title: 'Bulletin CERN',
  info: ['Issue title', 'http://home.cern/cern-people'],
  styles: { color: '#000' }
};

export default PdfHeader;
