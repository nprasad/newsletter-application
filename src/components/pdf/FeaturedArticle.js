import React from 'react';
import PropTypes from 'prop-types';
import { addIdToAnchorTag } from '../../helpers/Utils';

const styles = {
  title: {
    fontSize: 25,
    fontWeight: 800
  },
  subtitle: {
    fontSize: 16,
    marginTop: 10,
    fontWeight: 800
  },
  text: { fontSize: 12, textAlign: 'justify' },
  textColumn: { columnCount: 2, columnGap: 10, wordWrap: 'break-word', hyphens: 'auto' },
  image: {
    width: '100%',
    maxHeight: 400,
    marginTop: 30
  },
  caption: { fontSize: 10, fontStyle: 'italic', textAlign: 'center', marginTop: 15, marginBottom: 25 }
};

// eslint-disable-next-line no-unused-vars
const FeaturedArticle = ({ article, isMultiColumn, lang }) => {
  const {
    attributes: { title, field_strap, field_body, field_img_thumb, field_image_caption, field_byline }
  } = article;
  const image = field_img_thumb || '';
  const imageCaption = field_image_caption || '(Image: CERN)';
  const strap = field_strap ? field_strap.value : '';
  const byline = field_byline ? field_byline : '';
  const body = addIdToAnchorTag(field_body ? field_body.value : '');
  const textStyle = { ...styles.text, ...(isMultiColumn ? styles.textColumn : {}) };

  return (
    <article>
      <h2 style={styles.title}>{title}</h2>
      {strap ?
          <div style={styles.subtitle}>{strap}</div>:
          <div style={styles.subtitle}></div>
      }
      <img style={styles.image} src={image} alt={imageCaption} />
      <div style={styles.caption}>{imageCaption}</div>
      <div style={textStyle}>
        <div
          dangerouslySetInnerHTML={{
            __html: `${body?.replace(/(<([^>]+)>)/gi, '').substr(0, 1000)} ...`.trim()
          }}
        />
        <a style={{ display: 'block', marginBottom: 10, color: '#9d9d9d', fontSize: 18, borderRadius: "50%", background: "#DDDDDD", textAlign:"right" }} href={`#${article.id}`}>
          {/* eslint-disable-next-line react/no-unescaped-entities */}
          >>>
        </a>
        <div style={{ fontSize: 12,  marginTop: 10, marginBottom: 10, textAlign: 'right', fontStyle: 'italic' }}>{byline}</div>
      </div>
    </article>
  );
};
FeaturedArticle.propTypes = { lang: PropTypes.string, article: PropTypes.object, isMultiColumn: PropTypes.bool };
export default FeaturedArticle;
