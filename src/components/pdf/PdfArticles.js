import React from 'react';
import PropTypes from 'prop-types';
import PdfArticle from './PdfArticle';

const PdfArticles = ({ article, sidebarArticle, articles, articlesSection, isMultiColumn, styles, lang }) => {
  return (
    <section id="articles" style={{ pageBreakBefore: 'always' }}>
      {sidebarArticle && <PdfArticle article={sidebarArticle.article} isMultiColumn={isMultiColumn} styles={styles} />}
      {article && <PdfArticle article={article} isMultiColumn={isMultiColumn} styles={styles} />}
      {articles.length > 0 &&
        articles.map((article) => <PdfArticle key={article.id} article={article} isMultiColumn={isMultiColumn} styles={styles} />)}
      {articlesSection.length > 0 &&
        articlesSection.map((section) => (
          <React.Fragment key={section.id}>
            <b style={{ display: 'block', textAlign: 'center', fontSize: 30, color: styles.color }}>{section.title}</b>
            {section.articles.map((article) => (
              <PdfArticle key={article.id} article={article} isMultiColumn={isMultiColumn} styles={styles} />
            ))}
          </React.Fragment>
        ))}
    </section>
  );
};

PdfArticles.propTypes = {
  lang: PropTypes.string,
  article: PropTypes.object,
  sidebarArticle: PropTypes.object,
  articles: PropTypes.array,
  articlesSection: PropTypes.array,
  isMultiColumn: PropTypes.bool,
  styles: PropTypes.object
};
PdfArticles.defaultProps = { lang: 'en', article: null, sidebarArticle: null, articles: [], articlesSection: [], styles: { color: '#000' } };

export default PdfArticles;
