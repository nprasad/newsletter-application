import React, { useEffect, useState } from 'react';
import ReactDOMServer from 'react-dom/server';
import { IFrame } from './Iframe';
import PdfContent from './PdfContent';
import { decodeHtml } from 'helpers/Utils';

const PdfTest = () => {
  const [articles, setArticles] = useState([]);
  const [article, setArticle] = useState();
  const restOptions = { styles: { color: 'red', fontFamily: "'Courier New', monospace" } };

  useEffect(() => {
    fetch('https://cnpdfs.web.cern.ch/anNvbmFwaWJhc2V1cmw/node/article')
      .then((res) => res.json())
      .then(({ data }) => {
        const articles = data;
        const article = articles.find((a) => a.id === '0005f165-4073-44bc-95f1-8dfc3e799a9f') || articles[0];
        setArticles(articles);
        setArticle(article);
      });
  }, []);

  const downloadPDF = async () => {
    const html = await ReactDOMServer.renderToStaticMarkup(
      <PdfContent article={article} articles={articles} isMultiColumn options={restOptions} />
    );

    const decodedHtml = decodeHtml(html);

    console.log(decodedHtml);

    fetch('https://newsletter-ui.web.cern.ch/api/create-pdf', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ html: decodedHtml })
    }).then((res) => {
      console.log(res);
      return res
        .arrayBuffer()
        .then((res) => {
          const blob = new Blob([res], { type: 'application/pdf' });
          const blobURL = URL.createObjectURL(blob);
          window.open(blobURL);
        })
        .catch((e) => alert(e));
    });
  };

  return (
    <div>
      <button onClick={downloadPDF} type="primary">
        Download PDF
      </button>

      <section style={{ overflow: 'auto', margin: 'auto', width: 900 }}>
        <IFrame width={850} height={850}>
          <PdfContent article={article} articles={articles} isMultiColumn options={restOptions} />
        </IFrame>
      </section>
    </div>
  );
};

export default PdfTest;
