import React from 'react';
import PropTypes from 'prop-types';
import PdfArticles from './PdfArticles';
import PdfFirstPage from './PdfFirstPage';
import PdfHeader from './PdfHeader';
import PdfHtml from './PdfHtml';

const PdfContent = ({ article, sidebarArticle, articles, articlesSection, options, isMultiColumn, lang }) => {
  const { styles, header: headerProps, firstPage: firstPageProps } = options;
  return (
    <PdfHtml styles={styles}>
      <PdfHeader {...headerProps} styles={styles} />
      <PdfFirstPage
        article={article}
        sidebarArticle={sidebarArticle}
        articles={articles}
        articlesSection={articlesSection}
        isMultiColumn={isMultiColumn}
        {...firstPageProps}
        styles={styles}
        lang={lang}
      />
      <PdfArticles
        article={article}
        sidebarArticle={sidebarArticle}
        articles={articles}
        articlesSection={articlesSection}
        isMultiColumn={isMultiColumn}
        styles={styles}
        lang={lang}
      />
    </PdfHtml>
  );
};

PdfContent.propTypes = {
  lang: PropTypes.string,
  article: PropTypes.object,
  sidebarArticle: PropTypes.object,
  articles: PropTypes.array,
  articlesSection: PropTypes.array,
  options: PropTypes.object,
  isMultiColumn: PropTypes.bool
};
PdfContent.defaultProps = {
  lang: 'en',
  article: null,
  sidebarArticle: null,
  articles: [],
  articlesSection: [],
  options: { header: null },
  isMultiColumn: false
};

export default PdfContent;
