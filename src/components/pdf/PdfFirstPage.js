import React from 'react';
import PropTypes from 'prop-types';
import FeaturedArticle from './FeaturedArticle';

const PdfFirstPage = ({ article, sidebarArticle, articles, articlesSection, isMultiColumn, lang, styles: { color } }) => {
  return (
    <section id="featured-article">
      <div style={{ display: 'flex' }}>
        <div style={{ flex: 2 }}>{article && <FeaturedArticle article={article} isMultiColumn={isMultiColumn} />}</div>
        <div style={{ flex: 1, marginLeft: 30 }}>
          {sidebarArticle && (
            <div style={{ backgroundColor: '#e6e6e6', padding: 5 }}>
              <b style={{ display: 'block', fontSize: 23, color }}>{sidebarArticle.title}</b>
              <div style={{ fontSize: 12 }} dangerouslySetInnerHTML={{ __html: sidebarArticle.content }}></div>
              <a style={{ display: 'block', marginBottom: 10, color: '#9d9d9d', fontSize: 18, borderRadius: "50%", background: "#DDDDDD", textAlign:"right" }} href={`#${sidebarArticle.article.id}`}>
                >>>
              </a>
            </div>
          )}
          <b style={{ display: 'block', fontSize: 19, marginBottom: 5, color }}>Contents / Sommaire</b>
          <b style={{ display: 'block', marginBottom: 5, fontSize: 14 }}>News / Actualités </b>
          {articles.slice(0, 10).map(({ id, attributes: { title } }) => (
            <a style={{ display: 'block', marginBottom: 5, color: '#9d9d9d', fontSize: 12 }} href={`#${id}`} key={`#${id}`}>
              {title}
            </a>
          ))}
          {articlesSection.map((section) => (
            <div style={{ marginTop: 5 }} key={section.id}>
              <b style={{ display: 'block', marginBottom: 5, fontSize: 14 }}>{section.title}</b>
              {section.articles.slice(0, 10).map(({ id, attributes: { title } }) => (
                <a style={{ display: 'block', marginBottom: 5, color: '#9d9d9d', fontSize: 12 }} href={`#${id}`} key={`#${id}`}>
                  {title}
                </a>
              ))}{' '}
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};

PdfFirstPage.propTypes = {
  lang: PropTypes.string,
  article: PropTypes.object,
  sidebarArticle: PropTypes.object,
  articles: PropTypes.array,
  articlesSection: PropTypes.array,
  isMultiColumn: PropTypes.bool,
  styles: PropTypes.object
};
PdfFirstPage.defaultProps = { lang: 'en', article: null, sidebarArticle: null, articles: [], styles: { color: '#000' } };

export default PdfFirstPage;
