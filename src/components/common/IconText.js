import React from 'react';
import PropTypes from 'prop-types';

const IconText = ({ id, className, icon, text, onClick }) => (
  <div
    id={id}
    className={`mb-4 icon-text d-flex flex-column justify-content-center align-items-center text-center ${className}`}
    style={{ cursor: 'pointer' }}
    onClick={onClick}
  >
    <i className={icon} />
    <p className="card-text font-weight-semibold mb-0">{text}</p>
  </div>
);

IconText.propTypes = {
  id: PropTypes.string,
  className: PropTypes.string,
  icon: PropTypes.string,
  text: PropTypes.string,
  onClick: PropTypes.func
};

export default React.memo(IconText);
