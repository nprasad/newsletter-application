import React from 'react';
import PropTypes from 'prop-types';
import { useIntl } from 'react-intl';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';

const ConfirmationModal = ({ isOpen, title, text, onCancel, onConfirm }) => {
  const { formatMessage: f } = useIntl();
  return (
    <Modal isOpen={isOpen} backdrop="static">
      <ModalHeader>{title}</ModalHeader>
      <ModalBody>
        <h3>{text}</h3>
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" outline onClick={onCancel}>
          {f({ id: 'confirmation.cancel' })}
        </Button>
        <Button color="primary" onClick={onConfirm}>
          {f({ id: 'confirmation.confirm' })}
        </Button>
      </ModalFooter>
    </Modal>
  );
};

ConfirmationModal.propTypes = {
  isOpen: PropTypes.bool,
  title: PropTypes.string,
  text: PropTypes.string
};

ConfirmationModal.defaultProps = {
  isOpen: false,
  title: 'Confirmation',
  text: 'Are you sure?'
};

export default ConfirmationModal;
