import React from 'react';
import PropTypes from 'prop-types';
import includeDataProps from 'constants/includeDataProps';

export default function Link(props) {
  return (
    <a
      id={props.id}
      {...includeDataProps(props)}
      download={props.download}
      href={props.href}
      target="_blank"
      rel="noopener noreferrer"
      style={{
        color: props.color,
        textDecoration: props.textDecoration,
        ...props.style
      }}
    >
      {props.children}
    </a>
  );
}

Link.propTypes = {
  href: PropTypes.string,
  download: PropTypes.string,
  color: PropTypes.string,
  textDecoration: PropTypes.string,
  style: PropTypes.any,
  children: PropTypes.node
};

Link.defaultProps = {
  textDecoration: 'underline',
  href: undefined,
  download: undefined,
  color: undefined,
  style: undefined,
  children: undefined
};
