import React from 'react';
import PropTypes from 'prop-types';
import includeDataProps from 'constants/includeDataProps';

export default function Image(props) {
  return (
    <img
      {...includeDataProps(props)}
      alt={props.alt}
      src={props.src}
      width={props.width}
      height={props.height}
      style={{
        display: 'block',
        outline: 'none',
        border: 'none',
        textDecoration: 'none',
        ...props.style
      }}
    />
  );
}

Image.propTypes = {
  alt: PropTypes.string,
  src: PropTypes.string.isRequired,
  width: PropTypes.number,
  height: PropTypes.number,
  style: PropTypes.any
};

Image.defaultProps = {
  style: undefined
};
