import React from 'react';
import PropTypes from 'prop-types';

const translations = {
  en: { heading: 'If the e-mail is not displayed properly, please visit ', headingLink: 'this link' },
  fr: { heading: `Si l'e-mail ne s'affiche pas correctement, veuillez consulter `, headingLink: 'ce lien.' }
};

// inspired by http://htmlemailboilerplate.com
export default function Email(props) {
  // default nested 600px wide outer table container (see http://templates.mailchimp.com/development/html/)
  const locale = translations[props.lang];
  const {
    globalStyles: {
      global: { fontFamily }
    }
  } = props;

  return (
    <html lang={props.lang} xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>{props.title}</title>
        {props.headCSS && <style>{props.headCSS}</style>}
          {props.matomo}
          {props.ganalytics}
      </head>
      <body
        style={{
          width: '100%',
          maxWidth: props.width,
          margin: '50px auto',
          padding: 0,
          WebkitTextSizeAdjust: '100%',
          MsTextSizeAdjust: '100%',
          ...props.bodyStyle
        }}
      >
        <div className="no-css" style={{ textAlign: 'center' }}>
          <h2>
            {locale.heading}
            <a href={props.emailLink} style={{ textDecoration: 'none', marginLeft: 5 }} target="_blank" rel="noopener noreferrer">
              {locale.headingLink}
            </a>
          </h2>
        </div>
        <div
          className="email-content"
          style={{
            margin: '0 auto',
            width: props.width,
            maxWidth: props.width,
            fontFamily: `${fontFamily}`,
            fontSize: '15px'
          }}
        >
          {props.children}
        </div>
      <div><a href="https://cnpdfs.web.cern.ch/cmda/form/archive?nid=${instanceId}">Download PDF Newsletter</a> </div>
      </body>
    </html>
  );
}

Email.propTypes = {
  lang: PropTypes.string,
  title: PropTypes.string,
  bgcolor: PropTypes.string,
  cellPadding: PropTypes.number,
  cellSpacing: PropTypes.number,
  style: PropTypes.any,
  headCSS: PropTypes.string,
  width: PropTypes.string,
  align: PropTypes.oneOf(['left', 'center', 'right']),
  valign: PropTypes.oneOf(['top', 'middle', 'bottom']),
  bodyStyle: PropTypes.any,
  children: PropTypes.node,
  emailLink: PropTypes.string,
  matomo: PropTypes.any,
  ganalytics: PropTypes.any
};

Email.defaultProps = {
  lang: 'en',
  width: '1000px',
  align: 'center',
  valign: 'top',
  bgcolor: undefined,
  cellPadding: undefined,
  cellSpacing: undefined,
  style: undefined,
  headCSS: undefined,
  bodyStyle: undefined,
  children: undefined,
  title: 'CERN Newsletter'
};
