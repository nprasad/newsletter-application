import React from 'react';

const tableStyle = {
  table: {
    width: '100%',
    borderCollapse: 'collapse'
  }
};

function Cell({ children, ...props }) {
  return <td {...props}>{children}</td>;
}

function Row({ children, style = {} }) {
  return (
    <tr style={style}>
      {React.Children.map(children, (el) => {
        if (el.type === Cell) return el;
        return <td>{el}</td>;
      })}
    </tr>
  );
}

function Table({ children, style = {}, ...props }) {
  return (
    <table style={{ ...tableStyle.table, ...style }} {...props}>
      <tbody>
        {React.Children.map(children, (el) => {
          if (!el) return;

          // We want this content the be on it's own row.
          if (el.type === Row) return el;

          // The content is all inside a single cell (so a row)
          if (el.type === Cell) {
            return <tr>{el}</tr>;
          }

          // The content is one cell inside it's own row
          return (
            <tr>
              <td>{el}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

Table.Row = Row;
Table.Cell = Cell;

export default Table;
