import React from 'react';
import EmailArticle from 'containers/emails/EmailArticle';
import SocialWidget from 'containers/emails/SocialWidget';
import Table from 'components/emails/Table';
import LinksWidget from 'containers/emails/LinksWidget';
import TitleWidget from 'containers/emails/TitleWidget';
import EmailArticlesWidget from 'containers/emails/EmailArticlesWidget';
import ArticlesLinksWidget from 'containers/emails/ArticlesLinksWidget';
import CDSWidget from 'containers/emails/CDSWidget';
import SidebarArticle from 'containers/emails/SidebarArticle';

const _sidebarComponents = {
  socialWidget: SocialWidget,
  linksWidget: LinksWidget,
  articlesLinksWidget: ArticlesLinksWidget,
  cdsWidget: CDSWidget,
  emptyArticleSidebar: SidebarArticle
};

export const RenderWithSideBar = ({ articleComponents, sidebarComponents, globalStyles, lang }) => {
  return (
    <Table width="100%">
      <Table.Row>
        <Table.Cell id="articles" valign="top" style={{ whiteSpace: 'normal', paddingRight: 30, width: 666 }}>
          <div
            className="news"
            style={{
              float: 'left',
              marginTop: 20,
              position: 'relative',
              zIndex: 0,
              paddingLeft: 0,
              width: '100%'
            }}
          >
            <span dangerouslySetInnerHTML={{ __html: `<!-- replace with content -->` }} />
            {articleComponents.map(({ id, props, type }) => (
              <div key={id} className={type} style={{ width: '100%' }}>
                {type === 'titleWidget' ? (
                  <TitleWidget {...props} globalStyles={globalStyles} />
                ) : type === 'articlesWidget' ? (
                  <EmailArticlesWidget {...props} globalStyles={globalStyles} lang={lang}/>
                ) : (
                  <EmailArticle {...props} globalStyles={globalStyles} lang={lang} />
                )}
              </div>
            ))}
          </div>
          <span dangerouslySetInnerHTML={{ __html: `<!-- end replace with content -->` }} />
        </Table.Cell>
        <Table.Cell id="sidebar" valign="top" style={{ width: 336 }}>
           <div className="sidebar" style={{ marginTop: 20 }}>
            {sidebarComponents.map((payload) => {
                if(payload) {
                    const { id, props, type, ...rest } = payload;
                    const SidebarComponent = _sidebarComponents[type || 'SocialWidget'];
                    return <SidebarComponent key={id} {...props} {...rest} globalStyles={globalStyles} lang={lang} />;
                }
            })}
          </div>
        </Table.Cell>
      </Table.Row>
    </Table>
  );
};
