export const css = `@media only screen and (max-width: 600px) {
    body {
        font-size: 3vw !important;
    }
    body, div.email-content {
        width: 99vw !important;
        max-width: 600px !important;
    }
    body p {
        font-size: 2vw !important;
    }
    a {
        text-decoration: none;
        color: #1ea176;
        font-size: 3vw !important;
    }
    h1 {
        font-size: 3vw !important;
    }
    h2 {
        font-size: 3vw !important;
    }
    h4 {
        font-size: 3vw !important;
    }
    table {
        display: table;
        width: 99vw !important;
        border-collapse: collapse;
        border-spacing: 0;
    }
    td {
        max-width: 100% !important;
    }
    img {
        height: auto !important;
        max-width: 100% !important;
    }
    #header-menu-table {
        display: inline-block;
        width: 60vw !important;
    }
    td#header-menu {
        display: table-cell;
        width: 50vw !important;
    }
    td#articles {
        padding-right: 10px !important;
        display: table-row !important;
        max-width: 99vw !important;
        margin-right: 5px;
    }
    td#sidebar {
        display: table-row !important;
        max-width: 99vw !important;
        padding: 10px;
        margin-right: 5px;
    }
    td#printable {
        width: 100% !important;
    }
    img#cern-logo {
        width: 30vw;
        height: 30vw;
        max-height: 70px !important;
        max-width: 70px !important;
    }
    table#printable-table {
        width: 30vw !important;
        display: inline-block;
    }
    table.social-wrapper {
        padding: 3px !important;
        max-width: 98vw !important;
    }
    table.social-wrapper img {
        width: 3vw !important;
        height: 3vw !important;
        max-width: 10px !important;
        max-height: 10px !important;
        margin-left: 2px !important;
        margin-right: 2px !important;
    }
}
`.trim();
