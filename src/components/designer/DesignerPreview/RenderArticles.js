import React from 'react';
import EmailArticle from 'containers/emails/EmailArticle';
import Table from 'components/emails/Table';
import TitleWidget from 'containers/emails/TitleWidget';
import EmailArticlesWidget from 'containers/emails/EmailArticlesWidget';

export const RenderArticles = ({ components, globalStyles, lang }) => (
  <Table width="100%">
    <Table.Row>
      <Table.Cell id="articles" valign="top" style={{ whiteSpace: 'normal' }}>
        <div
          className="news"
          style={{
            marginTop: 20,
            position: 'relative',
            zIndex: 0,
            paddingLeft: 0
          }}
        >
          <span dangerouslySetInnerHTML={{ __html: `<!-- replace with content -->` }} />
          {components.map(({ id, props, type }) => (
            <div key={id} className={type} style={{ width: '100%' }}>
              {type === 'titleWidget' ? (
                <TitleWidget {...props} globalStyles={globalStyles} lang={lang} />
              ) : type === 'articlesWidget' ? (
                <EmailArticlesWidget {...props} globalStyles={globalStyles} lang={lang} />
              ) : (
                <EmailArticle {...props} globalStyles={globalStyles} lang={lang} />
              )}
            </div>
          ))}
          <span dangerouslySetInnerHTML={{ __html: `<!-- end replace with content -->` }} />
        </div>
      </Table.Cell>
    </Table.Row>
  </Table>
);
