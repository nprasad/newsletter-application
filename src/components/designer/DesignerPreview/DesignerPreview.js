import React from 'react';
import PropTypes from 'prop-types';
import Email from 'components/emails/Email';
import CDSWidget from 'containers/emails/CDSWidget';
import EmailFooter from 'containers/emails/EmailFooter';
import EmailHeader from 'containers/emails/EmailHeader';
import { css } from './css';
import { RenderArticles } from './RenderArticles';
import { RenderWithSideBar } from './RenderWithSideBar';

const DesignerPreview = ({
  lang,
  instanceId,
  globalStyles,
  headerComponents,
  articleComponents,
  sidebarComponents,
  preFooterComponents,
  footerComponents,
  newsLetterType,
}) => {
  return (
    <Email
      headCSS={css}
      globalStyles={globalStyles}
      lang={lang}
      emailLink={`https://cnpdfs.web.cern.ch/cmda/controller/newsletterview?nid=${instanceId}`}
    >

      {headerComponents.map(({ id, props }) => (
        <EmailHeader key={id} {...props} globalStyles={globalStyles} />
      ))}
      <div className="content">
        {!sidebarComponents.length ? (
          <RenderArticles components={articleComponents} globalStyles={globalStyles} lang={lang} />
        ) : (
          <RenderWithSideBar articleComponents={articleComponents} sidebarComponents={sidebarComponents} globalStyles={globalStyles} lang={lang} />
        )}
        {preFooterComponents.map(({ id, props }) => (
          <CDSWidget key={id} {...props} globalStyles={globalStyles} />
        ))}
      </div>
      {footerComponents.map(({ id, props }) => (
        <EmailFooter key={id} {...props} globalStyles={globalStyles} newsLetterType={newsLetterType} />
      ))}
    </Email>
  );
};

DesignerPreview.propTypes = {
  lang: PropTypes.string.isRequired,
  instanceId: PropTypes.number.isRequired,
  globalStyles: PropTypes.object.isRequired,
  headerComponents: PropTypes.array.isRequired,
  articleComponents: PropTypes.array.isRequired,
  sidebarComponents: PropTypes.array.isRequired,
  preFooterComponents: PropTypes.array.isRequired,
  footerComponents: PropTypes.array.isRequired,
  newsLetterType: PropTypes.string.isRequired,
};

export default DesignerPreview;
