import React, { useState } from 'react';
import './Editable.css';
import { Popover, PopoverBody, Form, FormGroup, Label, Input, PopoverHeader } from 'reactstrap';

const MyPopover = ({ title, open, target, toggle, children }) => {
  return (
    <Popover placement="auto" isOpen={open} target={target} toggle={toggle}>
      <PopoverHeader className="d-flex justify-content-between px-2">
        <span> {title}</span>
        <div style={{ cursor: 'pointer' }} onClick={toggle}>
          X
        </div>
      </PopoverHeader>
      <PopoverBody>{children}</PopoverBody>
    </Popover>
  );
};

const LinkPopover = ({ state, open, target, toggle, onChange }) => (
  <MyPopover title="Link" open={open} target={target} toggle={toggle}>
    <Form>
      <FormGroup>
        <Label for="text">Text</Label>
        <Input name="text" id="text" value={state.text} onChange={onChange} />
      </FormGroup>
      <FormGroup>
        <Label for="url">Url Link</Label>
        <Input name="url" id="url" value={state.url} onChange={onChange} />
      </FormGroup>
    </Form>
  </MyPopover>
);

const EditableUi = ({ target, component: Component, type, children, ...props }) => {
  const [isEditing, setEditing] = useState(false);
  const [linkState, setLinkState] = useState({ text: '', url: '' });
  const changeLinkState = (e) => setLinkState({ ...linkState, [e.target.name]: e.target.value });

  const handleKeyDown = (event, type) => {
    const { key } = event;
    const keys = ['Escape', 'Tab'];
    const enterKey = 'Enter';
    const allKeys = [...keys, enterKey];
    if ((type === 'textarea' && keys.indexOf(key) > -1) || (type !== 'textarea' && allKeys.indexOf(key) > -1)) {
      setEditing(false);
    }
  };

  return (
    <>
      {isEditing ? (
        <Component {...props} className="editable" onKeyDown={(e) => handleKeyDown(e, type)}>
          {children}
        </Component>
      ) : (
        <Component
          {...props}
          className="editable"
          onClick={(e) => {
            e.preventDefault();
            setEditing(true);
          }}
        >
          {children}
        </Component>
      )}
      {type === 'link' && (
        <LinkPopover open={isEditing} target={target} state={linkState} toggle={() => setEditing(false)} onChange={changeLinkState} />
      )}
    </>
  );
};

export default EditableUi;
