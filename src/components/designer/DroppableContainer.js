import React from 'react';
import PropTypes from 'prop-types';
import { Droppable } from 'react-beautiful-dnd';
import DraggableItem from './DraggableItem';
import DesignerComponent from './DesignerComponent';

const styles = {
  container: { height: '100%', width: '100%' },
  dragStyles: {
    minHeight: 200,
    backgroundColor: 'rgba(41,242,253,.1)',
    border: '2px dashed rgba(116,127,247,.842)',
    margin: 5,
    borderRadius: 12
  }
};

const DroppableContainer = ({ id, components, globalStyles, newsLetterType }) =>
  components ? (
    <Droppable droppableId={id}>
      {(provided) => (
        <div ref={provided.innerRef} {...provided.droppableProps} style={styles.container}>
          {components.length ? (
            components.filter(Boolean).map((component, index) => {
              return (
                <DraggableItem
                  innerRef={provided.innerRef}
                  key={component.id}
                  draggableId={component.id}
                  index={index}
                  component={component}
                >
                  <DesignerComponent
                    type={component.type}
                    {...component.props}
                    newsLetterType={newsLetterType}
                    droppableKey={component.droppableKey}
                    globalStyles={globalStyles}
                  />
                </DraggableItem>
              );
            })
          ) : (
            <div
              style={{
                height: '100%',
                minHeight: 200,
                width: '100%',
                backgroundColor: 'rgba(41,242,253,.1)',
                border: '2px dashed rgba(116,127,247,.842)',
                margin: 5,
                borderRadius: 12,
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                fontSize: '3rem',
                textTransform: 'capitalize'
              }}
            >
              {id}
            </div>
          )}
          {provided.placeholder}
        </div>
      )}
    </Droppable>
  ) : null;

DroppableContainer.propTypes = {
  id: PropTypes.string.isRequired,
  components: PropTypes.array.isRequired,
  globalStyles: PropTypes.object,
  newsLetterType: PropTypes.string,
};
export default DroppableContainer;
