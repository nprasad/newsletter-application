/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import { Colxx } from 'components/common/CustomBootstrap';
import DesignerMenu from 'components/designer/DesignerMenu/DesignerMenu';
import { DragDropContext } from 'react-beautiful-dnd';
import ReactDOMServer from 'react-dom/server';
import { useDispatch, useSelector } from 'react-redux';
import { Row } from 'reactstrap';
import { editInstance, onDragEndComponent, onSaveTemplate } from 'redux/actions';
import DesignerDataPickerModal from './DesignerDataPickerModal';
import DesignerIframe from './DesignerIframe';
import DesignerPreview from './DesignerPreview/DesignerPreview';
import DroppableContainer from './DroppableContainer';
import { setStorage } from '../../helpers/Localstorage';
import { LOCALE_STORAGE_KEYS } from '../../constants/emails';
import { parseHTMLArticlesComponentsStr } from '../../helpers/Utils';
import useTaxonomies from '../../hooks/use-taxonomies';

function Designer() {
  const [staticHtml, setStaticHtml] = useState(null);
  const [showSheduler, setShowSheduler] = useState(false);
  const dispatch = useDispatch();
  const { instance, newsletter } = useSelector((state) => state.newsletter)
  const { types, findTaxonomy } = useTaxonomies();
  const typesList = newsletter && types ? findTaxonomy(newsletter.relationships, 'field_type') : [];
  const typesNames = typesList.length ? typesList.map((category) => category.name) : [];
  const newsLetterType = typesNames.join(', ');
  setStorage(LOCALE_STORAGE_KEYS.LANG,newsletter?.attributes?.title.includes('(French)') ? 'fr' : 'en');
  const { showPreview, components, droppables, editedComponent, styles: globalStyles } = useSelector((state) => state.designer);
  const onDragEnd = (result) => dispatch(onDragEndComponent(result));
  const headerComponents = components && droppables ? droppables.header.componentIds.map((id) => components[id]) : [];
  const articleComponents = components && droppables ? droppables.article.componentIds.map((id) => components[id]) : [];
  const parsedArticleComponents = parseHTMLArticlesComponentsStr(articleComponents);
  const sidebarComponents = components && droppables ? droppables.sidebar.componentIds.map((id) => components[id]) : [];
  const footerComponents = components && droppables ? droppables.footer.componentIds.map((id) => components[id]) : [];
  const preFooterComponents = components && droppables ? droppables.preFooter.componentIds.map((id) => components[id]) : [];

  const htmlToStaticMarkup = () => ReactDOMServer.renderToStaticMarkup(
    <DesignerPreview
      lang={newsletter.attributes.title.includes('(French)') ? 'fr' : 'en'}
      globalStyles={globalStyles}
      instanceId={instance.attributes.drupal_internal__nid}
      headerComponents={headerComponents}
      articleComponents={parsedArticleComponents}
      sidebarComponents={sidebarComponents}
      preFooterComponents={preFooterComponents}
      footerComponents={footerComponents}
      newsLetterType={newsLetterType}
    />
  );


  const onSaveHtml = (attributes) => {
    const selectedArticles = parsedArticleComponents.reduce((acc, curr) => {
      if (curr.type === 'article' && curr.props) return [...acc, curr.props];
      if (curr.type === 'articlesWidget' && curr.props) {
        const { articles } = curr.props;
        if (articles.length) return [...acc, ...articles];
        return curr;
      } else return acc;
    }, []);
    const selectedArticlesBySidebar = sidebarComponents?.reduce((acc, curr) => {
      if (curr && curr.type === 'articlesLinksWidget' && curr.props) {
        const { articlesList, articles } = curr.props;
        if (articles.length && articlesList.length) {
          const filteredArticles = articlesList.filter((article) => articles.includes(article.id));
          return [...acc, ...filteredArticles];
        }
        return acc;
      } else return acc;
    }, []);

    const selectedArticleNodeIds = [
      ...selectedArticles.map((a) => a.nodeId),
      ...selectedArticlesBySidebar.map(({ attributes }) => attributes.drupal_internal__nid)
    ];

    const htmlToRender = htmlToStaticMarkup();
    const data = {
      type: 'node--instance',
      id: instance.id,
      attributes: {
        ...attributes,
        body: htmlToRender,
        field_designer_object: JSON.stringify({
          showPreview: false,
          styles: globalStyles,
          editedComponent: '',
          components,
          droppables,
          html: htmlToRender
        }),
        field_articleids: selectedArticleNodeIds.join()
      }
    };
    dispatch(editInstance({ data, id: instance.id }));
    dispatch(onSaveTemplate(htmlToRender));
  };

  const toggleSheduler = (e, value) => {
    setShowSheduler(!showSheduler);
    if (value) onSaveHtml({ field_schedule: value });
  };

  useEffect(() => {
    if (showPreview) {
      const htmlToRender = htmlToStaticMarkup();
      setStaticHtml(htmlToRender);
    }
  }, [showPreview]);

  return (
    <>
      <DragDropContext onDragEnd={onDragEnd} onDragStart={console.log}>
        <Row>
          <Colxx xxs="2" className="mb-4">
            <DesignerMenu onSaveHtml={toggleSheduler} />
          </Colxx>

          <Colxx xxs="9" className="mb-4">
            {showPreview && staticHtml ? (
              <DesignerIframe html={staticHtml} />
            ) : (
              <Row style={{ fontFamily: `${globalStyles.global.fontFamily}` }}>
                <Colxx xxs="12" className="mb-4">
                  <DroppableContainer
                    id="header"
                    components={headerComponents}
                    editedComponent={editedComponent}
                    globalStyles={globalStyles}
                  />
                </Colxx>
                <Colxx xxs="8" className="mb-4">
                  <DroppableContainer id="article" components={parsedArticleComponents} globalStyles={globalStyles} />
                </Colxx>
                <Colxx xxs="4" className="mb-4">
                  <DroppableContainer id="sidebar" components={sidebarComponents} globalStyles={globalStyles} />
                </Colxx>
                <Colxx xxs="12" className="mb-4">
                  <DroppableContainer id="preFooter" components={preFooterComponents} globalStyles={globalStyles} />
                </Colxx>
                <Colxx xxs="12" className="mb-4">
                  <DroppableContainer id="footer" components={footerComponents} globalStyles={globalStyles} newsLetterType={newsLetterType} />
                </Colxx>
              </Row>
            )}
          </Colxx>
        </Row>
      </DragDropContext>
      <DesignerDataPickerModal openModal={showSheduler} toggle={toggleSheduler} initialDate={instance.attributes.field_schedule} />
    </>
  );
}

export default Designer;
