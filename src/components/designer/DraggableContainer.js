import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { onEditComponent, onRemoveComponent } from 'redux/actions';
import EditComponentModal from './edit-components/EditComponentModal';
import ConfirmationModal from 'components/common/ConfirmationModal';

const Handler = ({ isHovered, ...props }) => (
  <div {...props}>
    <div className={`${isHovered ? 'opacity-100' : 'opacity-0'} transition-opacity-10 `}>
      <img src="/assets/img/icon-drag.svg" alt="" />
    </div>
  </div>
);

const Extra = ({ onEdit, hide, isHovered, ...props }) => (
  <div {...props} onClick={() => !hide && onEdit()}>
    <div
      className={`${
        isHovered ? 'opacity-100' : 'opacity-0'
      } transition-opacity-10 d-flex justify-content-center align-items-center w-10 text-white p-3 h-10`}
      style={{ cursor: 'pointer', background: 'rgb(36 39 57)', height: 30, width: 30, visibility: hide ? 'hidden' : 'visible' }}
    >
      <i className="simple-icon-options" />
    </div>
  </div>
);
const Delete = ({ onRemove, isHovered, ...props }) => (
  <div {...props} onClick={onRemove}>
    <div
      className={`${
        isHovered ? 'opacity-100' : 'opacity-0'
      } transition-opacity-10 d-flex justify-content-center align-items-center w-10 text-white p-3 h-10`}
      style={{ cursor: 'pointer', background: 'rgb(36 39 57)', height: 30, width: 30 }}
    >
      <i className="iconsminds-securiy-remove" />
    </div>
  </div>
);

const DraggableContainer = (props) => {
  const { component, className, provided, snapshot, children } = props;

  const dispatch = useDispatch();
  const selectEditedComponent = () => {
    dispatch(onEditComponent(component.id));
    toggleEditModal();
  };
  const removeComponent = () => {
    const { id, type } = component;
    const types = {
      header: 'header',
      article: 'article',
      emptyArticle: 'article',
      articlesWidget: 'article',
      titleWidget: 'article',
      emptyArticleSidebar: 'sidebar',
      linksWidget: 'sidebar',
      socialWidget: 'sidebar',
      articlesLinksWidget: 'sidebar',
      cdsWidget: 'preFooter',
      footer: 'footer'
    };
    toggleConfirmation();
    return dispatch(onRemoveComponent(id, types[type]));
  };

  const [hovered, setHovered] = useState(false);
  const [openEditModal, setOpenEditModal] = useState(false);
  const [showConfirmation, setShowConfirmation] = useState(false);
  const { isDragging } = snapshot;
  const onHovered = () => setHovered(true);
  const onRemoveHovered = () => setHovered(false);
  const preventDefault = (e) => e.preventDefault();
  const toggleEditModal = () => setOpenEditModal(!openEditModal);
  const showHandler = !['header', 'footer'].includes(component.type);
  const showExtra = true;
  const minimizeOnDrag = isDragging && ['cdsWidget'].includes(component.type);
  const toggleConfirmation = () => setShowConfirmation(!showConfirmation);
  return (
    <>
      <div
        className={className}
        ref={provided.innerRef}
        {...provided.draggableProps}
        onMouseEnter={onHovered}
        onMouseLeave={onRemoveHovered}
        onClick={preventDefault}
      >
        {showHandler && <Handler className="mr-2" {...provided.dragHandleProps} isHovered={hovered || isDragging} />}
        <div style={{ maxWidth: minimizeOnDrag ? 250 : '100%' }} className="w-100 overflow-hidden">
          {children}
        </div>
        <div>
          <Delete className="ml-4" isHovered={hovered || isDragging} onRemove={toggleConfirmation} />
          <Extra className="ml-4 mt-2" hide={!showExtra} isHovered={hovered || isDragging} onEdit={selectEditedComponent} />
        </div>
      </div>
      <EditComponentModal openModal={openEditModal} toggle={toggleEditModal}></EditComponentModal>
      <ConfirmationModal
        isOpen={showConfirmation}
        text="Are you sure that you want to delete this content?"
        onCancel={toggleConfirmation}
        onConfirm={removeComponent}
      />
    </>
  );
};

DraggableContainer.propTypes = {
  className: PropTypes.string,
  component: PropTypes.object.isRequired,
  provided: PropTypes.object.isRequired,
  snapshot: PropTypes.object.isRequired,
  children: PropTypes.element
};
export default DraggableContainer;
