import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class DesignerIframe extends Component {
  constructor() {
    super();
    this.iframe_ref = null;
  }

  componentDidMount() {
    this.writeHTML();
  }

  writeHTML = () => {
    if (!this.iframe_ref) {
      return;
    }

    const frame = this.iframe_ref;

    let doc = frame.contentDocument;

    doc.open();
    doc.write(this.props.html);
    doc.close();

    frame.style.width = '100%';

    frame.style.height = `75vh`;
  };

  render() {
    return (
      <iframe id="html-email-preview" title="html email preview" scrolling="auto" frameBorder="0" ref={(ref) => (this.iframe_ref = ref)} />
    );
  }
}

DesignerIframe.propTypes = {
  html: PropTypes.any
};
