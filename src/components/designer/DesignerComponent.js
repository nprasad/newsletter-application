import React from 'react';
import PropTypes from 'prop-types';
import EmailHeader from 'containers/emails/EmailHeader';
import EmailArticle from 'containers/emails/EmailArticle';
import EmailFooter from 'containers/emails/EmailFooter';
import SocialWidget from 'containers/emails/SocialWidget';
import LinksWidget from 'containers/emails/LinksWidget';
import TitleWidget from 'containers/emails/TitleWidget';
import EmailArticlesWidget from 'containers/emails/EmailArticlesWidget';
import ArticlesLinksWidget from 'containers/emails/ArticlesLinksWidget';
import CDSWidget from 'containers/emails/CDSWidget';
import SidebarArticle from 'containers/emails/SidebarArticle';

const componentList = {
  header: EmailHeader,
  article: EmailArticle,
  emptyArticle: EmailArticle,
  emptyArticleSidebar: SidebarArticle,
  articlesWidget: EmailArticlesWidget,
  titleWidget: TitleWidget,
  socialWidget: SocialWidget,
  linksWidget: LinksWidget,
  articlesLinksWidget: ArticlesLinksWidget,
  cdsWidget: CDSWidget,
  footer: EmailFooter
};

function DesignerComponent({ type, ...props }) {
  const TypeComponent = componentList[type || 'header'];
  return <TypeComponent {...props} />;
}

DesignerComponent.propTypes = { type: PropTypes.string.isRequired };

export default DesignerComponent;
