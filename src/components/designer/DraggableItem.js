import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import ReactDOMServer from 'react-dom/server';
import { Draggable } from 'react-beautiful-dnd';
import DraggableContainer from './DraggableContainer';
import { useDispatch } from 'react-redux';
import { onChangeComponent } from 'redux/actions';

const DraggableItem = ({ children, component, draggableId, index }) => {
  const dispatch = useDispatch();
  const renderEmailHeader = () => ReactDOMServer.renderToStaticMarkup(children);
  useEffect(() => {
    const html = renderEmailHeader();
    dispatch(onChangeComponent(component.id, 'html', html));
  }, []); // eslint-disable-line
  const isDragDisabled = ['header', 'footer'].includes(component.type);
  return (
    <Draggable draggableId={draggableId} index={index} isDragDisabled={isDragDisabled}>
      {(provided, snapshot) => (
        <DraggableContainer className="d-flex" component={component} provided={provided} snapshot={snapshot}>
          {children}
        </DraggableContainer>
      )}
    </Draggable>
  );
};

DraggableItem.propTypes = {
  children: PropTypes.element,
  component: PropTypes.object,
  draggableId: PropTypes.string,
  index: PropTypes.number
};

export default DraggableItem;
