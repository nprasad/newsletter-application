export const moduleItems = [
  {
    icon: 'iconsminds-full-view-2',
    text: 'Header',
    type: 'header'
  },
  {
    icon: 'iconsminds-digital-drawing',
    text: 'Title Widget',
    type: 'titleWidget'
  },
  {
    icon: 'iconsminds-paper',
    text: 'Empty Article',
    type: 'emptyArticle'
  },
  {
    icon: 'iconsminds-file-copy',
    text: 'Article Sidebar',
    type: 'emptyArticleSidebar'
  },
  {
    icon: 'iconsminds-testimonal',
    text: 'Article',
    type: 'article'
  },
  {
    icon: 'iconsminds-duplicate-layer',
    text: 'Articles Widget',
    type: 'articlesWidget'
  },
  {
    icon: 'iconsminds-box-with-folders',
    text: 'Links Widget',
    type: 'linksWidget'
  },
  {
    icon: 'simple-icon-docs',
    text: 'Articles Links',
    type: 'articlesLinksWidget'
  },
  {
    icon: 'iconsminds-sharethis',
    text: 'Social Widget',
    type: 'socialWidget'
  },
  {
    icon: 'simple-icon-picture',
    text: 'CDS Widget',
    type: 'cdsWidget'
  },
  {
    icon: 'iconsminds-down-2',
    text: 'Footer',
    type: 'footer'
  }
];
