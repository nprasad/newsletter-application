import IconText from 'components/common/IconText';
import { NotificationManager } from 'components/common/react-notifications';
import useFeeds from 'hooks/use-feeds';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Card, CardBody } from 'reactstrap';
import { fetchArticles, onAddComponent, onTogglePreview, resetArticles, resetDesigner, resetNewsletter } from 'redux/actions';
import { v4 as uuidv4 } from 'uuid';
import SelectArticlesModal from './SelectArticlesModal';
import { ModulesMenu } from './ModulesMenu';
import { formatArticles } from 'helpers/Utils';
import { StylesModal } from './StylesModal';

const DesignerMenu = ({ onSaveHtml }) => {
  const history = useHistory();
  const dispatch = useDispatch();

  const { showPreview } = useSelector((state) => state.designer);
  const { instance, newsletter } = useSelector((state) => state.newsletter);
  const { articles, articlesByFeed } = useSelector((state) => state.article);
  const [articlesFetchTries, setArticlesFetchTries] = useState(0);
  const { feeds } = useFeeds();
  const [articleList, setArticleList] = useState([]);
  const [showArticlesModal, setShowArticlesModal] = useState(false);
  const [articleCounter, setArticleCounter] = useState(0);
  // Styles Modals
  const [openStyles, setOpenStyles] = useState(false);

  useEffect(() => {
    if (articlesFetchTries === 3) NotificationManager.error(true, 'Articles Not Imported Yet.', 5000, null, null, '');
  }, [articlesFetchTries]);

  useEffect(() => {
    if (!articles && newsletter && articlesFetchTries < 2) {
      const newsletterFeeds = newsletter ? newsletter.relationships.field_feed.data : [];
      const newsletterNID = newsletter.attributes.drupal_internal__nid;
      const query = newsletterNID ? `?filter[field_source]=${newsletterNID}` : '';
      if (query && newsletterFeeds.length) {
        setArticlesFetchTries(articlesFetchTries + 1);
        dispatch(fetchArticles(query));
      } else NotificationManager.error(true, 'Not feed found for this newsletter.', 5000, null, null, '');
    }

    if (articles) {
      const formattedArticles = formatArticles(articles);
      setArticleList(formattedArticles);
    }
  }, [articles, articlesFetchTries, newsletter, dispatch]);

  const createComponentProps = (type, options) => {
    const articlesNumber = options ? options.articlesNumber : 5;
    const feed_nid = options && options.feed_nid;
    const newsletterFeedIds = newsletter ? newsletter.relationships.field_feed.data.map((f) => f.id) : [];
    const _feed = feeds.length && newsletterFeedIds.length ? feeds.find((f) => newsletterFeedIds.includes(f.id)) : undefined;
    const feedByNID =
      feeds.length && feed_nid ? feeds.find(({ attributes: { drupal_internal__nid } }) => drupal_internal__nid === feed_nid) : undefined;

    let props = {};
    if (type === 'header' && instance) {
      const { title, field_schedule, field_newsletterformat, drupal_internal__nid: instanceId } = instance.attributes;
      props = {
        ...props,
        cernLink: `http://home.cern${newsletter.attributes.title.includes('(French)') ? '/fr' : ''}`,
        issue: title,
        date: field_schedule,
        printableLink: ['both', 'pdf'].includes(field_newsletterformat)
          ? `https://cnpdfs.web.cern.ch/cmda/form/archive?nid=${instanceId}`
          : '',
        bannerImage: `${process.env.PUBLIC_URL}/assets/img/bulletin.jpg`,
        links: [
          { title: 'link 1', href: '' },
          { title: 'link 2', href: '' },
          { title: 'link 3', href: '' }
        ]
      };
    }
    if (type === 'article') {
      const _article =
        articleList.length && articleList.length > articleCounter
          ? articleList[articleCounter]
          : {
              title: 'Article Title',
              featuredImage: '',
              entryImage: '',
              excerpt: 'Article Excerpt',
              link: '',
              isFeatured: false,
              withImage: false
            };
      props = {
        ...props,
        ..._article,
        isSidebar: false
      };
    }
    if (type === 'emptyArticle' || type === 'emptyArticleSidebar') {
      props = {
        ...props,
        title: 'A WORD FROM...',
        content: `<h4>Title</h4> <p>Paragraph</p> <p>Another Paragraph...</p>`,
        article: { id: '' },
        feedNID: ''
      };
    }
    if (type === 'articlesWidget') {
      let title = 'Title Widget';
      if (feedByNID) title = feedByNID.attributes.title;
      const _articlesByFeed = feed_nid ? articlesByFeed[feed_nid] : [];
      const _articles = formatArticles(_articlesByFeed);
      props = {
        ...props,
        title,
        articles: _articles.slice(0, articlesNumber),
        feed_nid: feed_nid
      };
    }
    if (type === 'footer') {
      props = {
        ...props,
        links: [
          {
            title: 'Submit a story',
            href: 'https://cern.service-now.com/service-portal/report-ticket.do?name=story&se=cern-homewebsite-socialmedia'
          },
          {
            title: 'Contact us',
            href: 'mailto:writing-team@cern.ch'
          }
        ]
      };
    }
    if (type === 'linksWidget') {
      props = {
        ...props,
        title: 'Widget With Links',
        links: [
          {
            title: 'Link 1',
            href: ''
          },
          {
            title: 'Link 2',
            href: ''
          }
        ]
      };
    }
    if (type === 'socialWidget') {
      props = {
        ...props,
        facebook: 'https://www.facebook.com/cern',
        twitter: 'http://twitter.com/cern',
        youtube: 'https://www.youtube.com/cern',
        instagram: 'https://www.instagram.com/cern/',
        linkedin: 'https://www.linkedin.com/company/cern'
      };
    }
    if (type === 'titleWidget') {
      let title = 'Title Widget';
      if (_feed) title = _feed.attributes.title;
      props = { ...props, title };
    }
    if (type === 'articlesLinksWidget') {
      props = {
        ...props,
        title: 'Articles Widget With Links',
        articles: [],
        articlesList: []
      };
    }
    if (type === 'cdsWidget') {
      props = {
        ...props,
        title: 'CDS Widget Title',
        description: '',
        entries: []
      };
    }
    return props;
  };

  const addComponent = (type, options) => {
    const props = createComponentProps(type, options);
    dispatch(onAddComponent({ id: `${type}-${uuidv4()}`, type, props }));
  };

  const toggleModal = () => setShowArticlesModal(!showArticlesModal);

  const onAddArticles = ({ number, feed_nid }) => {
    addComponent('articlesWidget', { articlesNumber: number, feed_nid });
    setShowArticlesModal(false);
  };

  const handleAddComponent = (type) => {
    if (type === 'articlesWidget') setShowArticlesModal(true);
    else {
      if (type === 'article') setArticleCounter(articleCounter + 1);
      addComponent(type);
    }
  };

  const toggleStyles = () => setOpenStyles(!openStyles);

  const togglePreview = () => dispatch(onTogglePreview());
  const onResetDesigner = () => {
    dispatch(resetArticles());
    dispatch(resetNewsletter());
    dispatch(resetDesigner());
    history.push('/designer');
  };

  const goToIssueForm = () => history.push(`/newsletter/issue-add/${instance.id}`);

  return (
    <>
      <Card className="designer-menu">
        <CardBody className="text-center">
          <ModulesMenu onClick={handleAddComponent} />
          <IconText icon="simple-icon-note" text="Styles" onClick={toggleStyles} />
          {!showPreview ? (
            <IconText icon="iconsminds-preview" text="Preview" onClick={togglePreview} />
          ) : (
            <IconText icon="iconsminds-file-edit" text="Design" onClick={togglePreview} />
          )}
          <IconText icon="iconsminds-save" text="Save" onClick={onSaveHtml} />
          <IconText icon="iconsminds-reset" text="Reset" onClick={onResetDesigner} />
          <IconText icon="iconsminds-back" text="Issue Form" onClick={goToIssueForm} />
        </CardBody>
      </Card>
      <StylesModal open={openStyles} toggle={toggleStyles} />
      <SelectArticlesModal openModal={showArticlesModal} toggle={toggleModal} onAddArticles={onAddArticles} />
    </>
  );
};

DesignerMenu.propTypes = {
  onSaveHtml: PropTypes.func
};

export default DesignerMenu;
