/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, Input, Label } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { fetchArticles } from 'redux/actions';
import Select from 'react-select';
import CustomSelectInput from 'components/common/CustomSelectInput';

function SelectArticlesModal({ openModal, toggle, onAddArticles }) {
  const dispatch = useDispatch();
  const { newsletter } = useSelector((state) => state.newsletter);
  const { feeds } = useSelector((state) => state.feed);
  const { articlesByFeed } = useSelector((state) => state.article);
  const [number, setNumber] = useState(1);
  const [feedNID, setFeedNID] = useState();

  useEffect(() => {
    if (openModal && feedNID) {
      const newsletterNID = newsletter && newsletter.attributes.drupal_internal__nid;
      const feed_nid = feedNID;
      const query = newsletterNID ? `?filter[field_source]=${newsletterNID}&filter[field_feed_nid]=${feed_nid}` : '';
      const options = { feed_nid };
      dispatch(fetchArticles(query, options));
    }

    if (!openModal) {
      setNumber(1);
      setFeedNID(undefined);
    }
  }, [openModal, feedNID]);

  const articles = (feedNID && articlesByFeed[feedNID]) || [];
  const newsletterFeedIds = newsletter ? newsletter.relationships.field_feed.data.map((f) => f.id) : [];
  const filteredFeeds = feeds && newsletter ? feeds.filter((f) => newsletterFeedIds.includes(f.id)) : [];
  const feedOptions = filteredFeeds
    ? filteredFeeds.map(({ id, attributes: { title, drupal_internal__nid } }) => ({ label: title, value: drupal_internal__nid, key: id }))
    : [];

  const isValid = number && number <= articles.length;
  return (
    <Modal size="md" isOpen={openModal} toggle={toggle}>
      <ModalHeader style={{ textTransform: 'capitalize' }}>Articles Widget</ModalHeader>
      <ModalBody className="p-5">
        <Label for="articles-number">Select Articles Feed</Label>
        <Select
          components={{ Input: CustomSelectInput }}
          className="react-select"
          name="feeds"
          options={feedOptions}
          value={feedOptions.find((item) => feedNID === item.value)}
          onChange={({ value }) => setFeedNID(value)}
        />

        {articles.length > 0 && (
          <>
            <Label className="mt-3" for="articles-number">
              Select Articles Number (max. {articles.length})
            </Label>
            <Input
              id="articles-number"
              type="number"
              value={number}
              onChange={(event) => setNumber(event.target.value)}
              invalid={!isValid}
            />
          </>
        )}
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" onClick={() => onAddArticles({ number, feed_nid: feedNID })} disabled={!isValid}>
          Add Articles
        </Button>
      </ModalFooter>
    </Modal>
  );
}

SelectArticlesModal.propTypes = {
  openModal: PropTypes.bool,
  articlesLength: PropTypes.number,
  toggle: PropTypes.func,
  onAddArticles: PropTypes.func
};

export default SelectArticlesModal;
