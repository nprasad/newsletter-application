import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Popover, PopoverBody, Row } from 'reactstrap';
import IconText from 'components/common/IconText';
import { Colxx } from 'components/common/CustomBootstrap';
import { moduleItems } from './moduleItems';

export const ModulesMenu = ({ onClick }) => {
  const [popoverOpen, setPopoverOpen] = useState(false);
  const toggle = () => setPopoverOpen(!popoverOpen);

  const onAddComponent = (type) => {
    toggle();
    onClick && onClick(type);
  };

  return (
    <>
      <IconText id="module-menu" icon="iconsminds-box-close" text="Modules" onClick={toggle} />
      <Popover className="designer-popover" placement="right-start" isOpen={popoverOpen} target="module-menu" toggle={toggle}>
        <PopoverBody className="p-3">
          <Row>
            {moduleItems.map(({ icon, text, type }, index) => (
              <Colxx key={`module-menu-item-${index}`} xxs="6" className="d-flex justify-content-center">
                <IconText icon={icon} text={text} onClick={() => onAddComponent(type)} />
              </Colxx>
            ))}
          </Row>
        </PopoverBody>
      </Popover>
    </>
  );
};

ModulesMenu.propTypes = { onClick: PropTypes.func };
