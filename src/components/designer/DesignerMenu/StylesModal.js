import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  Form,
  FormGroup,
  Label,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Input
} from 'reactstrap';
import classnames from 'classnames';
import { ChromePicker } from 'react-color';
import { useDispatch, useSelector } from 'react-redux';
import { onChangeStyles } from 'redux/actions';
import Select from 'react-select';
import CustomSelectInput from 'components/common/CustomSelectInput';
import { getFontsFamily } from 'helpers/Utils';

const PickerForm = ({ pickerId, pickerIds, label, color, togglePicker, onChangeStyles }) => (
  <FormGroup style={{ borderBottom: '1px solid #000', paddingBottom: 20 }}>
    <Label
      style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', cursor: 'pointer' }}
      onClick={() => togglePicker(pickerId)}
    >
      {label}
      <div style={{ height: 25, width: 50, padding: 3, marginLeft: 10, backgroundColor: '#000' }}>
        <div style={{ width: '100%', height: '100%', backgroundColor: color }}></div>
      </div>
    </Label>

    {pickerIds.includes(pickerId) && <ChromePicker color={color} onChange={onChangeStyles} />}
  </FormGroup>
);

export function StylesModal({ open, toggle }) {
  const dispatch = useDispatch();
  const { styles } = useSelector((state) => state.designer);
  const [activeTab, setActiveTab] = useState('global');
  const [pickerIds, setPickerIds] = useState([]);
  const [fonts, setFonts] = useState([]);

  // const onChangeForm = (e) => dispatch(onChangeComponent(editedComponent, e.target.name, e.target.value, true));

  const togglePicker = (id) => {
    const picker = pickerIds.find((i) => i === id);
    setPickerIds(picker ? pickerIds.filter((p) => p !== id) : [...pickerIds, id]);
  };

  const changeStyles = (value, key, property) => {
    const newStyles = { ...styles, [key]: { ...styles[key], [property]: value } };
    dispatch(onChangeStyles(newStyles));
  };

  useEffect(() => {
    async function getFonts() {
      const _fonts = await getFontsFamily();
      setFonts(_fonts);
    }
    getFonts();
  }, []);

  const fontOptions = fonts.map((font) => ({
    label: font,
    value: font,
    key: font
  }));

  return (
    <Modal size="lg" isOpen={open} toggle={toggle} wrapClassName="modal-right">
      <ModalHeader style={{ textTransform: 'capitalize' }}>Styles</ModalHeader>
      <ModalBody>
        <Nav tabs className="separator-tabs ml-0 mb-5">
          <NavItem>
            <NavLink
              location={{}}
              to="#"
              className={classnames({
                active: activeTab === 'global',
                'nav-link': true
              })}
              onClick={() => setActiveTab('global')}
            >
              Global
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              location={{}}
              to="#"
              className={classnames({
                active: activeTab === 'header',
                'nav-link': true
              })}
              onClick={() => setActiveTab('header')}
            >
              Header
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              location={{}}
              to="#"
              className={classnames({
                active: activeTab === 'footer',
                'nav-link': true
              })}
              onClick={() => setActiveTab('footer')}
            >
              Footer
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={activeTab}>
          <TabPane tabId="global">
            <Form>
              <PickerForm
                pickerId="bg-global-color"
                pickerIds={pickerIds}
                label="Global Background Color:"
                color={styles.global.backgroundColor}
                togglePicker={togglePicker}
                onChangeStyles={(color) => changeStyles(color.hex, 'global', 'backgroundColor')}
              />
              <PickerForm
                pickerId="global-color"
                pickerIds={pickerIds}
                label="Global Color:"
                color={styles.global.color}
                togglePicker={togglePicker}
                onChangeStyles={(color) => changeStyles(color.hex, 'global', 'color')}
              />
            </Form>
            <FormGroup>
              <Label>Font Family:</Label>
              <Select
                components={{ Input: CustomSelectInput }}
                className="react-select"
                name="font-family"
                options={fontOptions}
                value={fontOptions.find((item) => styles.global.fontFamily === item.value)}
                onChange={({ value }) => changeStyles(value, 'global', 'fontFamily')}
              />
            </FormGroup>
            <FormGroup>
              <Label>Widget title size:</Label>
              <Input
                placeholder="25px"
                value={styles.widgetTitle?.fontSize}
                onChange={(e) => changeStyles(e.target.value, 'widgetTitle', 'fontSize')}
              />
            </FormGroup>
            <FormGroup>
              <Label>Widget links size:</Label>
              <Input
                placeholder="12px"
                value={styles.widgetLinks?.fontSize}
                onChange={(e) => changeStyles(e.target.value, 'widgetLinks', 'fontSize')}
              />
            </FormGroup>
            <FormGroup>
              <Label>Article title size:</Label>
              <Input
                placeholder="25px"
                value={styles.articleTitle?.fontSize}
                onChange={(e) => changeStyles(e.target.value, 'articleTitle', 'fontSize')}
              />
            </FormGroup>
            <FormGroup>
              <Label>Article excerpt size:</Label>
              <Input
                placeholder="12px"
                value={styles.excerptTitle?.fontSize}
                onChange={(e) => changeStyles(e.target.value, 'excerptTitle', 'fontSize')}
              />
            </FormGroup>
          </TabPane>
          <TabPane tabId="header">
            <Form>
              <PickerForm
                pickerId="bg-header-color"
                pickerIds={pickerIds}
                label="Background Color:"
                color={styles.header.backgroundColor}
                togglePicker={togglePicker}
                onChangeStyles={(color) => changeStyles(color.hex, 'header', 'backgroundColor')}
              />
              <PickerForm
                pickerId="title-color"
                pickerIds={pickerIds}
                label="Title Color:"
                color={styles.headerTitle.color}
                togglePicker={togglePicker}
                onChangeStyles={(color) => changeStyles(color.hex, 'headerTitle', 'color')}
              />
              <PickerForm
                pickerId="links-color"
                pickerIds={pickerIds}
                label="Links Color:"
                color={styles.headerLinks.color}
                togglePicker={togglePicker}
                onChangeStyles={(color) => changeStyles(color.hex, 'headerLinks', 'color')}
              />
              <FormGroup>
                <Label>Title size:</Label>
                <Input
                  placeholder="14px"
                  value={styles.headerTitle.fontSize}
                  onChange={(e) => changeStyles(e.target.value, 'headerTitle', 'fontSize')}
                />
              </FormGroup>
              <FormGroup>
                <Label>Links size:</Label>
                <Input
                  placeholder="12px"
                  value={styles.headerLinks.fontSize}
                  onChange={(e) => changeStyles(e.target.value, 'headerLinks', 'fontSize')}
                />
              </FormGroup>
            </Form>
          </TabPane>
          <TabPane tabId="footer">
            <Form>
              <PickerForm
                pickerId="bg-footer-color"
                pickerIds={pickerIds}
                label="Background Color:"
                color={styles.footer.backgroundColor}
                togglePicker={togglePicker}
                onChangeStyles={(color) => changeStyles(color.hex, 'footer', 'backgroundColor')}
              />
              <PickerForm
                pickerId="links-color"
                pickerIds={pickerIds}
                label="Links Color:"
                color={styles.footerLinks.color}
                togglePicker={togglePicker}
                onChangeStyles={(color) => changeStyles(color.hex, 'footerLinks', 'color')}
              />
              <FormGroup>
                <Label>Global size:</Label>
                <Input
                  placeholder="14px"
                  value={styles.footer.fontSize}
                  onChange={(e) => changeStyles(e.target.value, 'footer', 'fontSize')}
                />
              </FormGroup>
              <FormGroup>
                <Label>Links size:</Label>
                <Input
                  placeholder="12px"
                  value={styles.footerLinks.fontSize}
                  onChange={(e) => changeStyles(e.target.value, 'footerLinks', 'fontSize')}
                />
              </FormGroup>
            </Form>
          </TabPane>
        </TabContent>
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" onClick={toggle}>
          Close
        </Button>
      </ModalFooter>
    </Modal>
  );
}
StylesModal.propTypes = { open: PropTypes.bool.isRequired, toggle: PropTypes.func.isRequired };
