import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { onChangeComponent } from 'redux/actions';
import { Colxx } from 'components/common/CustomBootstrap';

export function EditLinkWidgetComponent({ props }) {
  const [links, setLinks] = useState(() => props.links || []);
  const dispatch = useDispatch();
  const { editedComponent } = useSelector((state) => state.designer);
  const onChangeForm = (e) => dispatch(onChangeComponent(editedComponent, e.target.name, e.target.value, true));
  const onChangeLinks = (e, i) => {
    const cloneLinks = links.slice();
    cloneLinks[i] = { ...cloneLinks[i], [e.target.name]: e.target.value };
    setLinks(cloneLinks);
    dispatch(onChangeComponent(editedComponent, 'links', cloneLinks, true));
  };
  const addNewLink = () => setLinks([...links, { title: '', href: '' }]);
  const removeLink = (index) => {
    const cloneLinks = links.slice();
    cloneLinks.splice(index, 1);
    setLinks(cloneLinks);
    dispatch(onChangeComponent(editedComponent, 'links', cloneLinks, true));
  };
  return (
    <Form>
      <FormGroup>
        <Label for="title">Link Widget Title</Label>
        <Input name="title" id="title" value={props.title} onChange={onChangeForm} />
      </FormGroup>
      {links &&
        links.map((link, index) => (
          <FormGroup row key={`links-${index}`} style={{ alignItems: 'center' }}>
            <Colxx sm={5}>
              <FormGroup>
                <Label for="title">Title {index + 1}</Label>
                <Input name="title" id="title" value={link.title} onChange={(event) => onChangeLinks(event, index)} />
              </FormGroup>
            </Colxx>
            <Colxx sm={5}>
              <FormGroup>
                <Label for="href">Link {index + 1}</Label>
                <Input name="href" id="href" value={link.href} onChange={(event) => onChangeLinks(event, index)} />
              </FormGroup>
            </Colxx>
            <Colxx sm={2}>
              <div className="pr-0 text-danger" style={{ cursor: 'pointer', fontSize: 21 }} onClick={() => removeLink(index)}>
                <i className="iconsminds-close m1-2" />
              </div>
            </Colxx>
          </FormGroup>
        ))}
      <Button className="text-center" color="primary" onClick={addNewLink}>
        Add New Link
      </Button>
    </Form>
  );
}
EditLinkWidgetComponent.propTypes = { props: PropTypes.object.isRequired };
