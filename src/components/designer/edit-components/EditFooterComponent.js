import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { onChangeComponent } from 'redux/actions';
import { Colxx } from 'components/common/CustomBootstrap';

export function EditFooterComponent({ props }) {
  const [links, setLinks] = useState(() => props.links || []);
  const dispatch = useDispatch();
  const { editedComponent } = useSelector((state) => state.designer);
  const onChangeLinks = (e, i) => {
    const cloneLinks = links.slice();
    cloneLinks[i] = { ...cloneLinks[i], [e.target.name]: e.target.value };
    setLinks(cloneLinks);
    dispatch(onChangeComponent(editedComponent, 'links', cloneLinks, true));
  };
  const addNewLink = () => setLinks([...links, { title: '', href: '' }]);
  return (
    <Form>
      {links &&
        links.map((link, index) => (
          <FormGroup row key={`links-${index}`}>
            <Colxx sm={6}>
              <FormGroup>
                <Label for="title">Title {index + 1}</Label>
                <Input name="title" id="title" value={link.title} onChange={(event) => onChangeLinks(event, index)} />
              </FormGroup>
            </Colxx>
            <Colxx sm={6}>
              <FormGroup>
                <Label for="href">Link {index + 1}</Label>
                <Input name="href" id="href" value={link.href} onChange={(event) => onChangeLinks(event, index)} />
              </FormGroup>
            </Colxx>
          </FormGroup>
        ))}
      {links && links.length < 6 && (
        <Button className="text-center" color="primary" onClick={addNewLink}>
          Add New Link
        </Button>
      )}
    </Form>
  );
}
EditFooterComponent.propTypes = { props: PropTypes.object.isRequired };
