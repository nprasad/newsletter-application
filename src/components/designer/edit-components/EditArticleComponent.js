/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Button, Form, FormGroup, Label, Input, Collapse } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { onChangeComponent, fetchArticles, selectArticle, setFilter, resetFilter } from 'redux/actions';
import Select from 'react-select';
import CustomSelectInput from 'components/common/CustomSelectInput';
import Filters from 'components/filter/Filters';

function EditArticleComponent({ onChange, props }) {
  const dispatch = useDispatch();
  const { newsletter } = useSelector((state) => state.newsletter);
  const { feeds } = useSelector((state) => state.feed);
  const { articles } = useSelector((state) => state.article);
  const { editedComponent } = useSelector((state) => state.designer);
  const { component, dateRange } = useSelector((state) => state.filter);
  const [showRestForm, setShowRestform] = useState(() => !!props.title);
  const [showFilters, setShowFilters] = useState(false);
  const [feedNID, setFeedNID] = useState();

  const toggleFilters = () => setShowFilters(!showFilters);
  const handleChangeSelect = ({ value }, { name }) => {
    if (value && name === 'original_article') {
      const {
        id: articleId,
        attributes: { title = '', field_url, field_strap, field_image_thumb, field_img_thumb, drupal_internal__nid }
      } = value;

      const excerpt = field_strap ? field_strap.value : '';
      const newProps = {
        ...props,
        articleId,
        nodeId: drupal_internal__nid,
        title,
        link: field_url,
        excerpt,
        featuredImage: field_image_thumb || '',
        entryImage: field_img_thumb || '',
        isFeatured: !!field_image_thumb,
        withImage: !field_image_thumb && !!field_img_thumb
      };
      dispatch(onChangeComponent(editedComponent, 'props', newProps));
      dispatch(selectArticle(articleId));
      setShowRestform(true);
    }
    if (name === 'article_layout') {
      const newProps = {
        ...props,
        isFeatured: value === 'layout-0',
        withImage: value === 'layout-1',
        layout: value
      };
      dispatch(onChangeComponent(editedComponent, 'props', newProps));
    }
  };

  useEffect(() => {
    dispatch(setFilter({ key: 'component', value: 'article' }));
    return () => dispatch(resetFilter());
  }, []);

  useEffect(() => {
    if (feedNID) {
      const newsletterNID = newsletter && newsletter.attributes.drupal_internal__nid;
      const feed_nid = feedNID;
      const query = newsletterNID ? `?filter[field_source]=${newsletterNID}&filter[field_feed_nid]=${feed_nid}` : '';

      dispatch(fetchArticles(query));
    }
    return () => setFeedNID(undefined);
  }, [feedNID]);

  const filteredArticles = articles
    ? component === 'article' && dateRange.start && dateRange.end
      ? articles.filter(
          (a) => new Date(a.attributes.changed) >= new Date(dateRange.start) && new Date(a.attributes.changed) <= new Date(dateRange.end)
        )
      : articles
    : [];

  const articleOptions = filteredArticles.map((article) => ({ label: article.attributes.title, value: article, key: article.id }));
  const layoutOptions = ['Featured', 'Image Preview', 'Simple'].map((option, index) => ({
    label: option,
    value: `layout-${index}`,
    key: `layout-${index}`
  }));

  const isEmptyArticleWidget = editedComponent.startsWith('emptyArticle');
  const newsletterFeedIds = newsletter ? newsletter.relationships.field_feed.data.map((f) => f.id) : [];
  const filteredFeeds = feeds && newsletter ? feeds.filter((f) => newsletterFeedIds.includes(f.id)) : [];
  const feedOptions = filteredFeeds
    ? filteredFeeds.map(({ id, attributes: { title, drupal_internal__nid } }) => ({ label: title, value: drupal_internal__nid, key: id }))
    : [];

  return (
    <>
      {!isEmptyArticleWidget && (
        <>
          <div className="d-flex justify-content-end mb-3">
            <Button className="pr-0" color="link" size="lg" onClick={toggleFilters}>
              <i className="iconsminds-filter-2" />
              filters
            </Button>
          </div>
          <Collapse isOpen={showFilters}>
            <Filters />
          </Collapse>
        </>
      )}
      <Form>
        {!isEmptyArticleWidget && (
          <>
            <FormGroup>
              <Label for="articles-number">Feed</Label>
              <Select
                components={{ Input: CustomSelectInput }}
                className="react-select"
                name="feeds"
                options={feedOptions}
                value={feedOptions.find((item) => feedNID === item.value)}
                onChange={({ value }) => setFeedNID(value)}
              />
            </FormGroup>

            <FormGroup>
              <Label for="issue">Article</Label>
              <Select
                components={{ Input: CustomSelectInput }}
                className="react-select"
                name="original_article"
                options={articleOptions}
                value={articleOptions.filter((item) => props.articleId === item.key)}
                onChange={handleChangeSelect}
              />
            </FormGroup>
          </>
        )}
        {(showRestForm || isEmptyArticleWidget) && (
          <>
            <FormGroup>
              <Label for="title">Article Title</Label>
              <Input name="title" id="title" value={props.title} onChange={onChange} />
            </FormGroup>
            <FormGroup>
              <Label for="excerpt">Article Excerpt</Label>
              <Input name="excerpt" id="excerpt" value={props.excerpt} onChange={onChange} />
            </FormGroup>
            <FormGroup>
              <Label for="link">Article Link</Label>
              <Input name="link" id="link" value={props.link} onChange={onChange} />
            </FormGroup>
            <FormGroup>
              <Label for="featuredImage">Featured Image</Label>
              <Input name="featuredImage" id="featuredImage" value={props.featuredImage} onChange={onChange} />
            </FormGroup>
            <FormGroup>
              <Label for="entryImage">Entry Image</Label>
              <Input name="entryImage" id="entryImage" value={props.entryImage} onChange={onChange} />
            </FormGroup>
            <FormGroup>
              <Label for="issue">Article Layout</Label>
              <Select
                components={{ Input: CustomSelectInput }}
                className="react-select"
                name="article_layout"
                options={layoutOptions}
                value={layoutOptions.filter((item) => props.layout === item.key)}
                onChange={handleChangeSelect}
              />
            </FormGroup>
          </>
        )}
      </Form>
    </>
  );
}
EditArticleComponent.propTypes = { onChange: PropTypes.func.isRequired, props: PropTypes.object.isRequired };
export default EditArticleComponent;
