import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { Form, FormGroup, Input, Label } from 'reactstrap';
import Button from 'reactstrap/lib/Button';
import { onChangeComponent } from 'redux/actions';

function EditCdsWidgetComponent({ props }) {
  const dispatch = useDispatch();
  const { editedComponent } = useSelector((state) => state.designer);
  const [cds, setCds] = useState(props.entries.length > 0
    ? props.entries
    : props.cds ?? []
  );
  const onChangeForm = (e) => dispatch(onChangeComponent(editedComponent, e.target.name, e.target.value, true));

  const onChangeCds = (e, i) => {
    const cloneCds = cds.slice();
    cloneCds[i] = { ...cloneCds[i], [e.target.name]: e.target.value };
    setCds(cloneCds);
    dispatch(onChangeComponent(editedComponent, 'cds', cloneCds, true));
  };



  const addNewCds = () => setCds([...cds, { id: '' }]);

  const fetchCds = async (id) =>   (await fetch(`https://cds.cern.ch/api/mediaexport?id=${id}`)).json();

  const resolveIsNewCds = (id, index) =>
    props.entries.length !== cds.length ||
    props.entries.some((entry, idx) => idx === index && entry.id !== id);

  const getEntry = async (id, index) => {
    try {
      const isNewCds = resolveIsNewCds(id, index);
      if (!isNewCds) {
        return;
      }
      const data = await fetchCds(id);
      const { entry } = data.entries[0];
      const newEntry = entry.images ?? entry;

      const existingEntry = props.entries.some((entry, idx) => idx === index);
      const entries = existingEntry
        ? props.entries.map((entry, idx) => idx !== index ? entry : newEntry)
        : [...props.entries, newEntry];

      dispatch(onChangeComponent(editedComponent, 'entries', entries, true));
    } catch (err) {
      console.error(err);
    }
  };

    const removeCds = (index) => {
      const cloneCds = cds.slice();
      cloneCds.splice(index, 1);
      setCds(cloneCds);
      dispatch(onChangeComponent(editedComponent, 'links', cloneCds, true));
    };

  return (
    <Form>
      <FormGroup>
        <Label for="title">Widget Title</Label>
        <Input name="title" id="title" value={props.title} onChange={onChangeForm} />

        <Label for="description">Description</Label>
        <Input type="textarea" name="description" id="description" value={props.description} onChange={onChangeForm} />

        {cds?.map((cds, index) => (
          <div key={`${cds.id}-${index}`}>
            <Label className="mt-4">Add CDS Media Export Id {index + 1}</Label>
            <Input name="id" id="cds-id" value={cds.id} onChange={(event) => onChangeCds(event, index)} />
            <Button className="my-4 btn-block" color="primary" onClick={() => getEntry(cds.id, index)} disabled={cds.disabled}>
              Get Entry
            </Button>
            <Button className="my-4 btn-block" color="danger" onClick={() => removeCds(index)} disabled={cds.disabled}>
              Remove Entry
            </Button>
          </div>
        ))}

        <Button className="my-4 btn-block" color="secondary" onClick={addNewCds}>
          Add New CDS Id
        </Button>
      </FormGroup>
    </Form>
  );
}
EditCdsWidgetComponent.propTypes = { props: PropTypes.object.isRequired };

export default EditCdsWidgetComponent;
