/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Form, FormGroup, Input, Label } from 'reactstrap';
import { fetchArticles, onChangeComponent } from 'redux/actions';
import Select from 'react-select';
import CustomSelectInput from 'components/common/CustomSelectInput';

function EditArticlesLinkWidgetComponent({ props }) {
  const dispatch = useDispatch();
  const { newsletter } = useSelector((state) => state.newsletter);
  const { feeds } = useSelector((state) => state.feed);
  const { components, editedComponent } = useSelector((state) => state.designer);
  const { articles } = useSelector((state) => state.article);
  const [feedNID, setFeedNID] = useState();

  const onChangeForm = (e) => dispatch(onChangeComponent(editedComponent, e.target.name, e.target.value, true));
  const handleChangeSelect = (values) =>
    dispatch(onChangeComponent(editedComponent, 'articles', values ? values.map(({ value }) => value) : [], true));

  const {
    props: { articles: selectedArticles }
  } = components[editedComponent];

  useEffect(() => {
    if (feedNID) {
      const newsletterNID = newsletter && newsletter.attributes.drupal_internal__nid;
      const feed_nid = feedNID;
      const query = newsletterNID ? `?filter[field_source]=${newsletterNID}&filter[field_feed_nid]=${feed_nid}` : '';

      dispatch(fetchArticles(query));
    }
    return () => setFeedNID(undefined);
  }, [feedNID]);

  const onSelectAll = () => {
    dispatch(
      onChangeComponent(
        editedComponent,
        'articles',
        articles.map(({ id }) => id),
        true
      )
    );
    dispatch(onChangeComponent(editedComponent, 'articlesList', articles, true));
  };

  const newsletterFeedIds = newsletter ? newsletter.relationships.field_feed.data.map((f) => f.id) : [];
  const filteredFeeds = feeds && newsletter ? feeds.filter((f) => newsletterFeedIds.includes(f.id)) : [];
  const feedOptions = filteredFeeds
    ? filteredFeeds.map(({ id, attributes: { title, drupal_internal__nid } }) => ({ label: title, value: drupal_internal__nid, key: id }))
    : [];

  const articleOptions = articles.map(({ id, attributes: { title } }) => ({
    label: title,
    value: id,
    key: id
  }));

  return (
    <Form>
      <FormGroup>
        <FormGroup>
          <Label for="articles-number">Feed</Label>
          <Select
            components={{ Input: CustomSelectInput }}
            className="react-select"
            name="feeds"
            options={feedOptions}
            value={feedOptions.find((item) => feedNID === item.value)}
            onChange={({ value }) => setFeedNID(value)}
          />
        </FormGroup>

        <Label for="title">Widget Title</Label>
        <Input name="title" id="title" value={props.title} onChange={onChangeForm} />

        <Label className="mt-4 d-flex justify-content-between align-items-center">
          <span>Select Articles</span>
        </Label>
        <Button className="btn-block text-center btn-sm" color="primary" onClick={onSelectAll}>
            Initialize & Select All
          </Button>
        <Select
          isMulti
          components={{ Input: CustomSelectInput }}
          className="react-select"
          options={articleOptions}
          value={articleOptions.filter((item) => selectedArticles && selectedArticles.includes(item.value))}
          onChange={handleChangeSelect}
        />
      </FormGroup>
    </Form>
  );
}
EditArticlesLinkWidgetComponent.propTypes = { props: PropTypes.object.isRequired };

export default EditArticlesLinkWidgetComponent;
