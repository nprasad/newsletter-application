import PropTypes from 'prop-types';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Form, FormGroup, Input, Label } from 'reactstrap';
import { onChangeComponent } from 'redux/actions';

export function EditSocialWidgetComponent({ props }) {
  const dispatch = useDispatch();
  const { editedComponent } = useSelector((state) => state.designer);
  const onChangeForm = (e) => dispatch(onChangeComponent(editedComponent, e.target.name, e.target.value, true));
  return (
    <Form>
      {['facebook', 'twitter', 'youtube', 'instagram', 'linkedin'].map((social) => (
        <FormGroup key={social}>
          <Label for={social}>{social}</Label>
          <Input name={social} id={social} value={props[social]} onChange={onChangeForm} />
        </FormGroup>
      ))}
    </Form>
  );
}
EditSocialWidgetComponent.propTypes = { props: PropTypes.object.isRequired };
