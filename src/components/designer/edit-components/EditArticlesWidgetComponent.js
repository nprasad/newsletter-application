/* eslint-disable react/prop-types */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Button, Form, FormGroup, Label, Input, Collapse } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { onChangeComponent, fetchArticles, setFilter } from 'redux/actions';
import Select from 'react-select';
import CustomSelectInput from 'components/common/CustomSelectInput';
import Filters from 'components/filter/Filters';
import { formatArticle } from 'helpers/Utils';

export function EditArticlesWidgetComponent({ onChange, props }) {
  const dispatch = useDispatch();
  const { newsletter } = useSelector((state) => state.newsletter);
  const { articlesByFeed } = useSelector((state) => state.article);
  const { editedComponent } = useSelector((state) => state.designer);
  const { component, dateRange } = useSelector((state) => state.filter);
  const [showFilters, setShowFilters] = useState(false);
  const [selectedArticle, setSelectedArticle] = useState(null);

  const articles = (props.feed_nid && articlesByFeed[props.feed_nid]) || [];

  const toggleFilters = () => setShowFilters(!showFilters);
  const handleChangeSelect = ({ value }, { name }, index) => {
    const copiedArticles = [...props.articles];
    if (value && name === 'original_article') {
      const {
        id: articleId,
        attributes: { title = '', field_url, field_strap, field_image_thumb, field_img_thumb, drupal_internal__nid }
      } = value;

      const excerpt = field_strap ? field_strap.value : '';
      copiedArticles[index] = {
        ...copiedArticles[index],
        articleId,
        nodeId: drupal_internal__nid,
        title,
        link: field_url,
        excerpt,
        featuredImage: field_image_thumb || '',
        entryImage: field_img_thumb || '',
        isFeatured: !!field_image_thumb,
        withImage: !field_image_thumb && !!field_img_thumb
      };
      dispatch(onChangeComponent(editedComponent, 'props', { ...props, articles: copiedArticles }));
    }
    if (name === 'article_layout') {
      copiedArticles[index] = {
        ...copiedArticles[index],
        isFeatured: value === 'layout-0',
        withImage: value === 'layout-1',
        layout: value
      };
      dispatch(onChangeComponent(editedComponent, 'props', { ...props, articles: copiedArticles }));
    }
  };

  const onChangeArticleField = (e, index) => {
    const copiedArticles = [...props.articles];
    copiedArticles[index] = { ...copiedArticles[index], [e.target.name]: e.target.value };
    dispatch(onChangeComponent(editedComponent, 'props', { ...props, articles: copiedArticles }));
  };

  const toggleArticles = (index) => setSelectedArticle(selectedArticle !== index ? index : null);

  const addNewArticle = () => {
    const copiedArticles = [...props.articles];
    const newArticle = formatArticle(articles[props.articles.length]);
    dispatch(onChangeComponent(editedComponent, 'props', { ...props, articles: [...copiedArticles, newArticle] }));
  };
  const removeArticle = (id) => {
    const newArticles = props.articles.filter((article) => article.articleId !== id);
    dispatch(onChangeComponent(editedComponent, 'props', { ...props, articles: newArticles }));
  };

  useEffect(() => {
    const { feed_nid } = props;
    const newsletterNID = newsletter.attributes.drupal_internal__nid;
    const query = newsletterNID ? `?filter[field_source]=${newsletterNID}&filter[field_feed_nid]=${feed_nid}` : '';
    const options = { feed_nid };
    dispatch(fetchArticles(query, options));
    dispatch(setFilter({ key: 'component', value: 'article' }));
  }, []);

  const filteredArticles =
    component === 'article' && dateRange.start && dateRange.end
      ? articles.filter(
          (a) => new Date(a.attributes.changed) >= new Date(dateRange.start) && new Date(a.attributes.changed) <= new Date(dateRange.end)
        )
      : articles;
  const articleOptions = filteredArticles.map((article) => ({ label: article.attributes.title, value: article, key: article.id }));
  const layoutOptions = ['Featured', 'Image Preview', 'Simple'].map((option, index) => ({
    label: option,
    value: `layout-${index}`,
    key: `layout-${index}`
  }));
  return (
    <>
      <div className="d-flex justify-content-end mb-3">
        <Button className="pr-0" color="link" size="lg" onClick={toggleFilters}>
          <i className="iconsminds-filter-2" />
          filters
        </Button>
      </div>
      <Collapse isOpen={showFilters}>
        <Filters />
      </Collapse>
      <Label for="title">Article Title</Label>
      <Input name="title" id="title" value={props.title} onChange={onChange} />
      {props.articles.length < articles.length && (
        <Button className="pl-0" color="link" size="lg" onClick={addNewArticle}>
          Add New Article
        </Button>
      )}
      {props.articles.map((article, index) => (
        <div key={`${article.articleId}-${index}`}>
          <div className="d-flex justify-content-between align-items-center">
            <Button
              className="d-block pl-0"
              color="link"
              size="xl"
              onClick={() => toggleArticles(index)}
              aria-expanded={selectedArticle === index}
            >
              Article no.{index + 1}
            </Button>

            <div className="pr-0 text-danger" style={{ cursor: 'pointer' }} onClick={() => removeArticle(article.articleId)}>
              <i className="iconsminds-close m1-2" />
              remove
            </div>
          </div>

          <Collapse key={`collapsed-article-${index}`} isOpen={selectedArticle === index}>
            <Form>
              <FormGroup>
                <Label for="issue">Article</Label>
                <Select
                  components={{ Input: CustomSelectInput }}
                  className="react-select"
                  name="original_article"
                  options={articleOptions}
                  value={articleOptions.filter((item) => article.articleId === item.key)}
                  onChange={(a, b) => handleChangeSelect(a, b, index)}
                />
              </FormGroup>

              <FormGroup>
                <Label for={`title-${index}`}>Article Title</Label>
                <Input name="title" id={`title-${index}`} value={article.title} onChange={(e) => onChangeArticleField(e, index)} />
              </FormGroup>
              <FormGroup>
                <Label for={`excerpt-${index}`}>Article Excerpt</Label>
                <Input name="excerpt" id={`excerpt-${index}`} value={article.excerpt} onChange={(e) => onChangeArticleField(e, index)} />
              </FormGroup>
              <FormGroup>
                <Label for={`link-${index}`}>Article Link</Label>
                <Input name="link" id={`link-${index}`} value={article.link} onChange={(e) => onChangeArticleField(e, index)} />
              </FormGroup>
              <FormGroup>
                <Label for={`featuredImage-${index}`}>Featured Image</Label>
                <Input
                  name="featuredImage"
                  id={`featuredImage-${index}`}
                  value={article.featuredImage}
                  onChange={(e) => onChangeArticleField(e, index)}
                />
              </FormGroup>
              <FormGroup>
                <Label for={`entryImage-${index}`}>Entry Image</Label>
                <Input
                  name="entryImage"
                  id={`entryImage-${index}`}
                  value={article.entryImage}
                  onChange={(e) => onChangeArticleField(e, index)}
                />
              </FormGroup>
              <FormGroup>
                <Label>Article Layout</Label>
                <Select
                  components={{ Input: CustomSelectInput }}
                  className="react-select"
                  name="article_layout"
                  options={layoutOptions}
                  value={layoutOptions.filter((item) => article.layout === item.key)}
                  onChange={(a, b) => handleChangeSelect(a, b, index)}
                />
              </FormGroup>
            </Form>
          </Collapse>
        </div>
      ))}
    </>
  );
}
EditArticlesWidgetComponent.propTypes = { onChange: PropTypes.func.isRequired, props: PropTypes.object.isRequired };
