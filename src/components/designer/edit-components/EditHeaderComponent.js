import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Colxx } from 'components/common/CustomBootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Form, FormGroup, Input, Label } from 'reactstrap';
import { onChangeComponent } from 'redux/actions';

export function EditHeaderComponent({ onChange, props }) {
  const dispatch = useDispatch();
  const [links, setLinks] = useState(() => props.links || []);
  const { editedComponent } = useSelector((state) => state.designer);
  const onChangeLinks = (e, i) => {
    const cloneLinks = links.slice();
    cloneLinks[i] = { ...cloneLinks[i], [e.target.name]: e.target.value };
    setLinks(cloneLinks);
    dispatch(onChangeComponent(editedComponent, 'links', cloneLinks, true));
  };
  const addNewLink = () => setLinks([...links, { title: '', href: '' }]);
  return (
    <Form>
      <FormGroup>
        <Label for="cernLink">Cern Title Link</Label>
        <Input name="cernLink" id="cernLink" value={props.cernLink} onChange={onChange} />
      </FormGroup>
      <FormGroup>
        <Label for="issue">Issue number</Label>
        <Input name="issue" id="issue" value={props.issue} onChange={onChange} />
      </FormGroup>
      <FormGroup>
        <Label for="date">Date</Label>
        <Input name="date" id="date" value={props.date} onChange={onChange} />
      </FormGroup>
      <FormGroup>
        <Label for="printableLink">Printable version link</Label>
        <Input name="printableLink" id="printableLink" value={props.printableLink} onChange={onChange} />
      </FormGroup>
      <FormGroup>
        <Label for="bannerImage">Banner Image</Label>
        <Input name="bannerImage" id="bannerImage" value={props.bannerImage} onChange={onChange} />
      </FormGroup>
      {links &&
        links.map((link, index) => (
          <FormGroup row key={`links-${index}`}>
            <Colxx sm={6}>
              <FormGroup>
                <Label for="title">Title {index + 1}</Label>
                <Input name="title" id="title" value={link.title} onChange={(event) => onChangeLinks(event, index)} />
              </FormGroup>
            </Colxx>
            <Colxx sm={6}>
              <FormGroup>
                <Label for="href">Link {index + 1}</Label>
                <Input name="href" id="href" value={link.href} onChange={(event) => onChangeLinks(event, index)} />
              </FormGroup>
            </Colxx>
          </FormGroup>
        ))}
      {links && links.length < 6 && (
        <Button className="text-center" color="primary" onClick={addNewLink}>
          Add New Link
        </Button>
      )}
    </Form>
  );
}

EditHeaderComponent.propTypes = { onChange: PropTypes.func.isRequired, props: PropTypes.object.isRequired };
