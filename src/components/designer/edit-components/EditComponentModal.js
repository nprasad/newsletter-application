import React from 'react';
import PropTypes from 'prop-types';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { onChangeComponent } from 'redux/actions';
import { EditHeaderComponent } from './EditHeaderComponent';
import EditArticleComponent from './EditArticleComponent';
import { EditLinkWidgetComponent } from './EditLinkWidgetComponent';
import { EditFooterComponent } from './EditFooterComponent';
import { EditTitleWidgetComponent } from './EditTitleWidgetComponent';
import { EditArticlesWidgetComponent } from './EditArticlesWidgetComponent';
import EditArticlesLinkWidgetComponent from './EditArticlesLinkWidgetComponent';
import EditCdsWidgetComponent from './EditCdsWidgetComponent';
import { EditSocialWidgetComponent } from './EditSocialWidgetComponent';
import EditArticleSidebarComponent from './EditArticleSidebarComponent';

const componentsByType = {
  header: EditHeaderComponent,
  article: EditArticleComponent,
  emptyArticle: EditArticleComponent,
  emptyArticleSidebar: EditArticleSidebarComponent,
  articlesWidget: EditArticlesWidgetComponent,
  titleWidget: EditTitleWidgetComponent,
  linksWidget: EditLinkWidgetComponent,
  socialWidget: EditSocialWidgetComponent,
  articlesLinksWidget: EditArticlesLinkWidgetComponent,
  cdsWidget: EditCdsWidgetComponent,
  footer: EditFooterComponent
};

function EditComponentModal({ openModal, toggle }) {
  const dispatch = useDispatch();
  const { components, editedComponent } = useSelector((state) => state.designer);
  const onChangeForm = (e) => dispatch(onChangeComponent(editedComponent, e.target.name, e.target.value, true));

  const currentComponent = components && components[editedComponent];
  const componentProps = currentComponent ? currentComponent.props : {};
  const EditedComponent = currentComponent ? componentsByType[currentComponent.type] : React.Fragment;
  return currentComponent ? (
    <Modal size="lg" isOpen={openModal} toggle={toggle} wrapClassName="modal-right">
      <ModalHeader style={{ textTransform: 'capitalize' }}>{currentComponent.type} Settings</ModalHeader>
      <ModalBody>{EditedComponent && <EditedComponent onChange={onChangeForm} props={componentProps} />}</ModalBody>
      <ModalFooter>
        <Button color="secondary" onClick={toggle}>
          Close
        </Button>
      </ModalFooter>
    </Modal>
  ) : null;
}

EditComponentModal.propTypes = { openModal: PropTypes.bool, toggle: PropTypes.func };

export default EditComponentModal;
