import React from 'react';
import PropTypes from 'prop-types';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { onChangeComponent } from 'redux/actions';

export function EditTitleWidgetComponent({ props }) {
  const dispatch = useDispatch();
  const { editedComponent } = useSelector((state) => state.designer);
  const onChangeForm = (e) => dispatch(onChangeComponent(editedComponent, e.target.name, e.target.value, true));
  return (
    <Form>
      <FormGroup>
        <Label for="title">Widget Title</Label>
        <Input name="title" id="title" value={props.title} onChange={onChangeForm} />
      </FormGroup>
    </Form>
  );
}
EditTitleWidgetComponent.propTypes = { props: PropTypes.object.isRequired };
