/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { fetchArticles, onChangeComponent } from 'redux/actions';
import Select from 'react-select';
import CustomSelectInput from 'components/common/CustomSelectInput';

function EditArticleSidebarComponent({ props }) {
  const dispatch = useDispatch();
  const { newsletter } = useSelector((state) => state.newsletter);
  const { feeds } = useSelector((state) => state.feed);
  const { articles } = useSelector((state) => state.article);
  const { editedComponent } = useSelector((state) => state.designer);
  const [feedNID, setFeedNID] = useState(props.feedNID);
  const [article, setArticle] = useState();

  const onChangeForm = (e) => dispatch(onChangeComponent(editedComponent, e.target.name, e.target.value, true));
  const onSelectArticle = ({ value }) => setArticle(value);

  useEffect(() => {
    if (feedNID) {
      const newsletterNID = newsletter && newsletter.attributes.drupal_internal__nid;
      const feed_nid = feedNID;
      const query = newsletterNID ? `?filter[field_source]=${newsletterNID}&filter[field_feed_nid]=${feed_nid}` : '';
      dispatch(fetchArticles(query));
      dispatch(onChangeComponent(editedComponent, 'feedNID', feedNID, true));
    }
  }, [feedNID]);

  useEffect(() => {
    if (article) {
      const {
        attributes: { title, field_strap }
      } = article;
      const htmlContent = `<h4>${title}</h4>${field_strap ? field_strap.processed : '<p>...</p>'}`;
      dispatch(onChangeComponent(editedComponent, 'article', article, true));
      dispatch(onChangeComponent(editedComponent, 'content', htmlContent, true));
    }
  }, [article]);

  const newsletterFeedIds = newsletter ? newsletter.relationships.field_feed.data.map((f) => f.id) : [];
  const filteredFeeds = feeds && newsletter ? feeds.filter((f) => newsletterFeedIds.includes(f.id)) : [];
  const feedOptions = filteredFeeds
    ? filteredFeeds.map(({ id, attributes: { title, drupal_internal__nid } }) => ({ label: title, value: drupal_internal__nid, key: id }))
    : [];

  const articleOptions = articles.map((article) => ({
    label: article.attributes.title,
    value: article,
    key: article.id
  }));

  return (
    <Form>
      <FormGroup>
        <Label for="title">Widget Title</Label>
        <Input name="title" id="title" value={props.title} onChange={onChangeForm} />
      </FormGroup>
      <FormGroup>
        <Label for="content">Html Content</Label>
        <Input type="textarea" name="content" id="content" value={props.content} onChange={onChangeForm} />
      </FormGroup>

      <FormGroup>
        <Label>Select Feed*</Label>
        <Select
          components={{ Input: CustomSelectInput }}
          className="react-select"
          name="feeds"
          options={feedOptions}
          value={feedOptions.find((item) => props.feedNID === item.value)}
          onChange={({ value }) => {
            setFeedNID(value);
          }}
        />
      </FormGroup>

      {feedNID && (
        <FormGroup>
          <Label>Select Article*</Label>
          <Select
            components={{ Input: CustomSelectInput }}
            className="react-select"
            options={articleOptions}
            value={articleOptions.find((item) => props.article.id === item.value.id)}
            onChange={onSelectArticle}
          />
        </FormGroup>
      )}
    </Form>
  );
}
EditArticleSidebarComponent.propTypes = { onChange: PropTypes.func.isRequired, props: PropTypes.object.isRequired };
export default EditArticleSidebarComponent;
