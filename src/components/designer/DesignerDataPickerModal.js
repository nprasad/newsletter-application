import React, { useState } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import ReactDatePicker from 'react-datepicker';

function DesignerDataPickerModal({ openModal, toggle, initialDate }) {
  const [startDateTime, setStartDateTime] = useState(() => (initialDate ? new Date(initialDate) : new Date()));
  const onSave = (e) => toggle(e, moment(startDateTime).format());

  return (
    <Modal size="md" isOpen={openModal} toggle={toggle}>
      <ModalHeader style={{ textTransform: 'capitalize' }}>Schedule Issue</ModalHeader>
      <ModalBody className="p-5">
        <label>Select Date</label>
        <ReactDatePicker
          calendarClassName="embedded d-flex"
          inline
          showTimeSelect
          timeIntervals={10}
          selected={startDateTime}
          onChange={setStartDateTime}
        />
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" onClick={onSave}>
          Save
        </Button>
      </ModalFooter>
    </Modal>
  );
}

DesignerDataPickerModal.propTypes = { openModal: PropTypes.bool, toggle: PropTypes.func };

export default DesignerDataPickerModal;
