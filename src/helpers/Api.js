import Axios from 'axios';
import { renderFormParams, formatBasicAuth } from './serviceUtils';

const PREFIX_URL = 'anNvbmFwaWJhc2V1cmw';
const API_URL = `${process.env.REACT_APP_API_URL}/${PREFIX_URL}/`;

const getHeaders = () => {
  const headers = new Headers();
  headers.append('Accept', 'application/vnd.api+json');
  headers.append('Authorization', formatBasicAuth(process.env.REACT_APP_DRUPAL_USER, process.env.REACT_APP_DRUPAL_PASSWORD));
  headers.append('Content-Type', 'application/vnd.api+json');

  return headers;
};

const _fetch = (url, method = 'GET', data) =>
  fetch(url, {
    method, // *GET, POST, PUT, DELETE, etc.
    headers: getHeaders(),
    body: data ? JSON.stringify(data) : undefined
  });

const get = (url, queryParams) => {
  const query = queryParams && Object.keys(queryParams).length > 0 ? '?' + renderFormParams(queryParams) : '';
  return fetch(`${API_URL + url}${query}`);
};
const post = (url, data) => _fetch(API_URL + url, 'POST', data);
const update = (url, data) => _fetch(API_URL + url, 'PATCH', data);
const upload = (url, file) =>
  Axios({
    method: 'post',
    url: API_URL + url,
    headers: {
      Accept: 'application/vnd.api+json',
      'Content-Type': 'application/octet-stream',
      'Content-Disposition': `file; filename="bulletin_cern_${new Date().toLocaleDateString()}.pdf"`
    },
    data: file
  });
const _delete = (url) => _fetch(API_URL + url, 'DELETE');

export default { get, post, update, upload, delete: _delete };
