const isIterable = (obj) => obj != null && typeof obj[Symbol.iterator] === 'function';

const renderFormParams = (params) => {
  let formBody = new URLSearchParams();
  Object.keys(params).forEach((key) => {
    if (isIterable(params[key]) && typeof params[key] !== 'string') {
      params[key].forEach((param) => {
        formBody.append(key, param);
      });
    } else {
      formBody.append(key, params[key]);
    }
  });
  return formBody.toString();
};

const getKeycloakExchangeFormBody = (keycloak, cfg, audience) => {
  let formBody = new URLSearchParams();
  const params = {
    grant_type: cfg.grantType,
    audience: audience || 'authorization-service-api',
    subject_token: keycloak.token,
    subject_token_type: cfg.subjectTokenType,
    requested_token_type: cfg.subjectTokenType
  };
  Object.keys(params).forEach((key) => formBody.append(key, params[key]));
  return formBody.toString();
};

const getKeycloakExchangeUrlencoded = (cfg, audience) => {
  let urlencoded = new URLSearchParams();
  const params = {
    grant_type: cfg.grantType,
    client_id: cfg.authClientId,
    client_secret: cfg.authClientSecret,
    audience: audience || 'authorization-service-api'
  };
  Object.keys(params).forEach((key) => urlencoded.append(key, params[key]));
  return urlencoded;
};

const formatBasicAuth = (userName, password) => {
  var basicAuthCredential = userName + ':' + password;
  var bace64 = btoa(basicAuthCredential);
  return 'Basic ' + bace64;
};

export { renderFormParams, getKeycloakExchangeFormBody, getKeycloakExchangeUrlencoded, formatBasicAuth };
