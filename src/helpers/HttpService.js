import { renderFormParams, getKeycloakExchangeUrlencoded } from './serviceUtils';

const getHeaders = (token) => {
  const headers = new Headers();
  if (token) headers.append('Authorization', `Bearer ${token}`);
  headers.append('Content-Type', 'application/json; charset=utf-8');
  return headers;
};

const _fetch = (url, token, method = 'GET', data) => {
  const body = {
    method,
    headers: getHeaders(token),
    body: data ? JSON.stringify(data) : undefined
  };
  return fetch(url, body);
};

const get = (url, token, queryParams) => {
  const query = queryParams && Object.keys(queryParams).length > 0 ? '?' + renderFormParams(queryParams) : '';
  return _fetch(`${url}${query}`, token);
};
const post = (url, token, data) => _fetch(url, token, 'POST', data);
const put = (url, token, data) => _fetch(url, token, 'PUT', data);
const _delete = (url, token, data) => _fetch(url, token, 'DELETE', data);

const _getAuthApiToken = async (keycloak, cfg, audience) => {
  let expiry = Date.parse(sessionStorage.getItem(`shared_access_token_timeout`));
  if (!expiry || expiry < Date.now()) {
    try {
      const headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');

      const url =
        process.env.REACT_APP_STATUS === 'development'
          ? 'https://auth.cern.ch/auth/realms/cern/api-access/token'
          : keycloak.authServerUrl + cfg.tokenExchangeEndpoint;
      const response = await fetch(url, {
        method: 'POST',
        headers,
        body: getKeycloakExchangeUrlencoded(cfg, audience)
      });
      const result = await response.json();
      if (response.ok) {
        sessionStorage.setItem('shared_access_token', result.access_token);
        let expiry = new Date();
        expiry.setTime(expiry.getTime() + result.expires_in * 900);
        sessionStorage.setItem(`shared_access_token_timeout`, expiry);
        return result.access_token;
      }
      return undefined;
    } catch (error) {
      console.error(`Failed to exchange token =\n`, error.message);
      throw error;
    }
  } else {
    return sessionStorage.getItem('shared_access_token');
  }
};
const getAuthApiToken = async () => {
  let expiry = Date.parse(sessionStorage.getItem(`shared_access_token_timeout`));
  if (!expiry || expiry < Date.now()) {
    try {
      const url = process.env.PUBLIC_URL + '/api/token';
      const response = await fetch(url);
      const result = await response.json();
      if (response.ok) {
        sessionStorage.setItem('shared_access_token', result.access_token);
        let expiry = new Date();
        expiry.setTime(expiry.getTime() + result.expires_in * 900);
        sessionStorage.setItem(`shared_access_token_timeout`, expiry);
        return result.access_token;
      }
      return undefined;
    } catch (error) {
      console.error(`Failed to exchange token =\n`, error.message);
      throw error;
    }
  } else {
    return sessionStorage.getItem('shared_access_token');
  }
};

export default { get, post, put, delete: _delete, _getAuthApiToken, getAuthApiToken };
