import { defaultDirection } from '../constants/defaultValues';
import moment from 'moment';
import 'moment/locale/fr';

export const getDirection = () => {
  let direction = defaultDirection;
  if (localStorage.getItem('direction')) {
    const localValue = localStorage.getItem('direction');
    if (localValue === 'rtl' || localValue === 'ltr') {
      direction = localValue;
    }
  }
  return {
    direction,
    isRtl: direction === 'rtl'
  };
};

export const setDirection = (localValue) => {
  let direction = 'ltr';
  if (localValue === 'rtl' || localValue === 'ltr') {
    direction = localValue;
  }
  localStorage.setItem('direction', direction);
};

export const compare = (a, b, isAsc, isDate) => {
  if (isDate) return new Date(b) - new Date(a);
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
};

export const search_match = (term, array, key, isNested) => {
  const reg = new RegExp(
    term
      .replaceAll('(', '')
      .replaceAll(')', '')
      .replaceAll('[', '')
      .replaceAll(']', '')
      .replaceAll('+', '')
      .replaceAll('*', '')
      .split('')
      .join('.*'),
    'i'
  );
  return isNested
    ? array.filter((item) => {
        const value = getNestedObject(item, key);
        return value && value.match(reg);
      })
    : array.filter((item) => item[key] && item[key].match(reg));
};

export const getNestedObject = (nestedObj, pathArr) => {
  return pathArr.reduce((obj, key) => (obj && obj[key] !== 'undefined' ? obj[key] : undefined), nestedObj);
};
export const formatNumber = (num) => (!isNaN(num) ? num.toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false }) : '00');

export const calculateIssue = (issue) => {
  const currentYear = new Date().getFullYear();
  let result = `Issue No. 01-02/${currentYear}`;
  if (issue.includes('Issue No.')) {
    const [prefix, date] = issue.split('.');
    const [periods, year] = date.split('/');
    const [, second] = periods.split('-');
    const secondNumber = +second;
    const finalYear = +currentYear < +year ? year : currentYear;
    result = `${prefix}. ${formatNumber(secondNumber + 1)}-${formatNumber(secondNumber + 2)}/${finalYear}`;
  }
  return result;
};

export const formatArticle = ({
  id: articleId,
  attributes: { title = '', field_url, field_strap, field_image_thumb, field_img_thumb, drupal_internal__nid }
}) => ({
  articleId,
  nodeId: drupal_internal__nid,
  title,
  link: field_url,
  excerpt: field_strap ? field_strap.value : '',
  featuredImage: field_image_thumb || '',
  entryImage: field_img_thumb || '',
  isFeatured: !!field_image_thumb,
  withImage: !field_image_thumb && !!field_img_thumb,
  layout: field_image_thumb ? 'layout-0' : !field_image_thumb && !!field_img_thumb ? 'layout-1' : 'layout-2'
});

export const formatArticles = (articles) => articles.map(formatArticle);

export const decodeHtml = (html) => {
  const txt = document.createElement('textarea');
  txt.innerHTML = html;
  return txt.value;
};

export const getFontsFamily = async () => {
  const defaultFonts = [
    'Arial, sans-serif',
    'Verdana, sans-serif',
    'Helvetica, sans-serif',
    'Tahoma, sans-serif',
    "'Trebuchet MS', sans-serif",
    "'Times New Roman', serif",
    'Georgia, serif',
    'Garamond, serif',
    "'Courier New', monospace",
    "'Brush Script MT', cursive",
    'sans-serif'
  ];

  const fonts = new Set(
    [
      // Windows 10
      'Arial',
      'Arial Black',
      'Bahnschrift',
      'Calibri',
      'Cambria',
      'Cambria Math',
      'Candara',
      'Comic Sans MS',
      'Consolas',
      'Constantia',
      'Corbel',
      'Courier New',
      'Ebrima',
      'Franklin Gothic Medium',
      'Gabriola',
      'Gadugi',
      'Georgia',
      'HoloLens MDL2 Assets',
      'Impact',
      'Ink Free',
      'Javanese Text',
      'Leelawadee UI',
      'Lucida Console',
      'Lucida Sans Unicode',
      'Malgun Gothic',
      'Marlett',
      'Microsoft Himalaya',
      'Microsoft JhengHei',
      'Microsoft New Tai Lue',
      'Microsoft PhagsPa',
      'Microsoft Sans Serif',
      'Microsoft Tai Le',
      'Microsoft YaHei',
      'Microsoft Yi Baiti',
      'MingLiU-ExtB',
      'Mongolian Baiti',
      'MS Gothic',
      'MV Boli',
      'Myanmar Text',
      'Nirmala UI',
      'Palatino Linotype',
      'Segoe MDL2 Assets',
      'Segoe Print',
      'Segoe Script',
      'Segoe UI',
      'Segoe UI Historic',
      'Segoe UI Emoji',
      'Segoe UI Symbol',
      'SimSun',
      'Sitka',
      'Sylfaen',
      'Symbol',
      'Tahoma',
      'Times New Roman',
      'Trebuchet MS',
      'Verdana',
      'Webdings',
      'Wingdings',
      'Yu Gothic',
      // macOS
      'American Typewriter',
      'Andale Mono',
      'Arial',
      'Arial Black',
      'Arial Narrow',
      'Arial Rounded MT Bold',
      'Arial Unicode MS',
      'Avenir',
      'Avenir Next',
      'Avenir Next Condensed',
      'Baskerville',
      'Big Caslon',
      'Bodoni 72',
      'Bodoni 72 Oldstyle',
      'Bodoni 72 Smallcaps',
      'Bradley Hand',
      'Brush Script MT',
      'Chalkboard',
      'Chalkboard SE',
      'Chalkduster',
      'Charter',
      'Cochin',
      'Comic Sans MS',
      'Copperplate',
      'Courier',
      'Courier New',
      'Didot',
      'DIN Alternate',
      'DIN Condensed',
      'Futura',
      'Geneva',
      'Georgia',
      'Gill Sans',
      'Helvetica',
      'Helvetica Neue',
      'Herculanum',
      'Hoefler Text',
      'Impact',
      'Lucida Grande',
      'Luminari',
      'Marker Felt',
      'Menlo',
      'Microsoft Sans Serif',
      'Monaco',
      'Noteworthy',
      'Optima',
      'Palatino',
      'Papyrus',
      'Phosphate',
      'Rockwell',
      'Savoye LET',
      'SignPainter',
      'Skia',
      'Snell Roundhand',
      'Tahoma',
      'Times',
      'Times New Roman',
      'Trattatello',
      'Trebuchet MS',
      'Verdana',
      'Zapfino'
    ].sort()
  );

  await document.fonts.ready;

  const fontAvailable = new Set();

  for (const font of fonts.values()) {
    if (document.fonts.check(`12px "${font}"`)) {
      fontAvailable.add(font);
    }
  }

  return [...defaultFonts, ...fontAvailable.values()];
};

export const addIdToAnchorTag = (str) => str?.replace(
  /<a href=["'](https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s"']{2,}|(?!ftp:\/\/)[^\s]+\.[^\s'"]{2,})/g,
  (match) => !match.includes('https://cds.cern')
    ? match.split(' ')
      .map((substring, index) => index === 0
        ? `${substring} class="hrefClass"`
        : substring)
      .join(' ')
    : match
);

const replaceNumericalHTMLToChar = (str) =>
  str?.replace(/&#(\d+);/g, (match, match2) => String.fromCharCode(+match2));

export const parseHTMLArticlesStr = (articles) => {
  const parsedArticles = [];
  articles.forEach(article => {
    const parsedAttributes = {};
    Object.keys(article.attributes).forEach(key => {
      if (key === 'field_strap' || key === 'field_body') {
        parsedAttributes[key] = {
          value: replaceNumericalHTMLToChar(article.attributes[key]?.value)
        };
      } else if (typeof article.attributes[key] === 'string') {
        parsedAttributes[key] = replaceNumericalHTMLToChar(article.attributes[key]);
      } else {
        parsedAttributes[key] = article.attributes[key];
      }
    });
    parsedArticles.push({ ...article, attributes: parsedAttributes });
  });
  return parsedArticles;
}

export const parseHTMLArticlesComponentsStr = (articleComponents) => {
  const parsedArticleComponents = [];
  articleComponents.forEach(articleComponent => {
    parsedArticleComponents.push({
      ...articleComponent,
      props: {
        ...articleComponent.props,
        articles: articleComponent.props.articles?.map(article => ({
          ...article,
          excerpt: replaceNumericalHTMLToChar(article?.excerpt),
          featuredImage: replaceNumericalHTMLToChar(article?.featuredImage),
          title: replaceNumericalHTMLToChar(article?.title),
        })),
      },
    });
  });
  return parsedArticleComponents;
}

export const formatDate = (date, lang) => {
  moment.locale(lang);
  return  moment(date).format('D MMMM YYYY');
}
