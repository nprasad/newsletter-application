const express = require('express');
const path = require('path');
const cors = require('cors');
const fetch = require('node-fetch');
const puppeteer = require('puppeteer');
require('dotenv').config();
const app = express();
const PORT = process.env.PORT || 8080;

app.use(cors());
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ extended: true, limit: '50mb' }));

// API calls
app.get('/api/token', (req, res) => {
  const url = 'https://auth.cern.ch/auth/realms/cern/api-access/token';
  const headers = { 'Content-Type': 'application/x-www-form-urlencoded' };
  const body = `grant_type=client_credentials&client_id=${process.env.REACT_APP_AUTH_CLIENT_ID}&client_secret=${process.env.REACT_APP_AUTH_CLIENT_SECRET}&audience=authorization-service-api`;
  fetch(url, { method: 'POST', headers: headers, body })
    .then((response) => {
      if (response.status >= 200 && response.status < 300) {
        return response;
      } else {
        let err = new Error(response.statusText);
        err.response = response;
        throw err;
      }
    })
    .then((result) => result.json())
    .then((json) => res.send(json))
    .catch((error) =>
      res.status(error.response.status).send({
        message: error.response.statusText
      })
    );
});

app.get('/api/create-pdf', (req, res) => {
  res.send({ message: 'Create Pdf API' });
});

app.post('/api/create-pdf', async (req, res) => {
  const { html } = req.body;
  //  https://github.com/puppeteer/puppeteer/blob/main/docs/troubleshooting.md#setting-up-chrome-linux-sandbox
  const browser = await puppeteer.launch({ args: ['--no-sandbox', '--disable-setuid-sandbox'] });
  const page = await browser.newPage();
  await page.setContent(html, { waitUntil: 'networkidle0' });
  // await page.screenchot();
  const buffer = await page.pdf({
    format: 'A4',
    margin: {
      left: '0px',
      top: '30px',
      right: '0px',
      bottom: '70px'
    },
    displayHeaderFooter: true,
    footerTemplate: `
    <div style="width: 100%; font-size: 8px; color: #bbb; text-align: center;">
        <div><span class="pageNumber"></span>/<span class="totalPages"></span></div>
    </div>
  `
  });
  await browser.close();
  res.send(buffer);
});

app.use(express.static(path.join(__dirname, './build')));

app.get('*', (req, res) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.sendFile(path.join(__dirname, './build', 'index.html'));
});

app.listen(PORT, () => console.log(`Express server is running on port ${PORT}`));
